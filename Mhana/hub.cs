﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Mhana.Models;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Newtonsoft.Json;

namespace Mhana
{
    [HubName("MhanaHub")]
    public class hub : Hub
    {
        public readonly static ConnectionMapping<string> _connections = new ConnectionMapping<string>();
        public void Hello()
        {
            Clients.All.hello();
        }
        public override Task OnConnected()
        {
            //Context.QueryString["UserID"];
            string Id = Context.QueryString["UserID"]; //Context.User.Identity.GetUserId(); // واحد من هدول فاضي 
            _connections.Add(Id, Context.ConnectionId);
            using (var db = new MhanaDbEntities())
            {
                if(!string.IsNullOrEmpty(Id))
                {
                    var item = db.AspNetUsers.FirstOrDefault(w => w.Id == Id);
                    item.online = true;
                    db.Entry(item).State = EntityState.Modified;
                    db.SaveChanges();
                }
                 
            }
            Clients.All.hello(new ResponseModel() { status = "success", result = new { id= Id,online=true }, message = Id });
            //Clients.a
            return base.OnConnected();
        }
        public override Task OnDisconnected(bool stopCalled)
        {
            string Id = Context.QueryString["UserID"];
            _connections.Remove(Id, Context.ConnectionId);
            using (var db = new MhanaDbEntities())
            {
                if (!string.IsNullOrEmpty(Id))
                {
                    var item = db.AspNetUsers.FirstOrDefault(w => w.Id == Id);
                    item.online = false;
                    db.Entry(item).State = EntityState.Modified;
                    db.SaveChanges();
                }

            }
            Clients.All.hello(new ResponseModel() { status = "success", result = new { id = Id, online = false }, message = Id });
            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            string Id = Context.QueryString["UserID"];
            if (!_connections.GetConnections(Id).Contains(Context.ConnectionId))
            {
                _connections.Add(Id, Context.ConnectionId);
                using (var db = new MhanaDbEntities())
                {
                    if (!string.IsNullOrEmpty(Id))
                    {
                        var item = db.AspNetUsers.FirstOrDefault(w => w.Id == Id);
                        item.online = true;
                        db.Entry(item).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                }
            }
            Clients.All.hello(new ResponseModel() { status = "success", result = new { id = Id, online = true }, message = Id });
            return base.OnReconnected();
        }

        public void Online()
        {
            var RId = Context.QueryString["UserID"];           
            var ReturnUser = _connections.GetConnections(RId).FirstOrDefault();            
            Clients.Client(Context.ConnectionId).online(new ResponseModel() { status = "success", result = !string.IsNullOrEmpty(ReturnUser) });
        }
    }
    public class ResponseModel
    {
        public string status { set; get; }
        public object result { set; get; }
        public string message { set; get; }
    }
    public class ConnectionMapping<T>
    {


        private readonly Dictionary<T, HashSet<string>> _connections =
          new Dictionary<T, HashSet<string>>();



        public int Count
        {
            get
            {
                return _connections.Count;
            }
        }

        public void Add(T key, string connectionId)
        {
            lock (_connections)
            {
                HashSet<string> connections;
                if (!_connections.TryGetValue(key, out connections))
                {
                    connections = new HashSet<string>();
                    _connections.Add(key, connections);
                }

                lock (connections)
                {
                    connections.Add(connectionId);
                }
            }
        }

        public IEnumerable<string> GetConnections(T key)
        {
            HashSet<string> connections;
            if (_connections.TryGetValue(key, out connections))
            {
                return connections;
            }

            return Enumerable.Empty<string>();
        }
        public void Remove(T key, string connectionId)
        {
            lock (_connections)
            {
                HashSet<string> connections;
                if (!_connections.TryGetValue(key, out connections))
                {
                    return;
                }

                lock (connections)
                {
                    connections.Remove(connectionId);

                    if (connections.Count == 0)
                    {
                        _connections.Remove(key);
                    }
                }
            }
        }
    }

    public class TeacherStatus
    {
        [JsonProperty("status")]
        public int? status { get; set; }      
    }   
}