﻿using Mhana.Helper;
using Mhana.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using static Mhana.Models.Enums;

namespace Mhana.Areas.Teacher.Controllers
{
    public class AccountController : BaseController
    {
        // GET: Teacher/Account
        public ActionResult Index()
        {
            string user_id = Functions.GetUser().id;
            //TeacherRegisterResponseModel
            List<TeacherTimes> TT = new List<TeacherTimes>();
            List<string> el = new List<string>();
            using (var db = new MhanaDbEntities())
            {
                TT = db.Teacher_Times.Where(w=>w.user_id== user_id).Select(s => new TeacherTimes() { day_number = s.day_number, from = s.from_time, to = s.to_time }).ToList();
                ViewBag.specializations = db.specializations.Select(s => new keyValue() { id = s.Id, value = s.name }).ToList();
                ViewBag.branch_specialization = db.branch_specialization.Select(s => new keyValue() { id = s.Id, value = s.name }).ToList();
                ViewBag.education_level = db.education_level_sub.Select(s => new keyValue() { id = s.id, value = s.name }).ToList();
           
              
                var user = db.AspNetUsers.FirstOrDefault(f=>f.Id == user_id);

                if (user != null)
                {
                    TeacherRegisterResponseModel response = new TeacherRegisterResponseModel()
                    {
                        branch_specialization = user.branch_specialization,
                        certificate = user.certificate,
                        specialization = user.specialization,
                        mobile = user.PhoneNumber,
                        first_name = user.first_name,
                        last_name = user.last_name,
                        id = user.Id,
                        certificate_photo = user.certificate_photo,
                        city = user.city,
                        region = user.region,
                        country = user.country,
                        details = user.details,
                        dob = string.Format("{0:MM/dd/yyyy}", user.dob),
                        email = user.Email,
                        expired = 14,
                        fcm = user.fcm,
                        fullname = user.fullname,
                        gender = user.gender,
                        photo = user.photo,
                        online_cost =string.IsNullOrEmpty(user.online_cost)?0:Convert.ToDouble(user.online_cost),
                        site_cost = string.IsNullOrEmpty(user.site_cost) ? 0 : Convert.ToDouble(user.site_cost),
                        status = user.status,
                        teaching_mechanism = user.teaching_mechanism,
                        university = user.university,
                        teacher_times = TT,
                        education_level = !string.IsNullOrEmpty(user.education_level_text) ? user.education_level_text.Split(',').ToList() : el,
                        is_complete = user.is_complete == 1 ? true : false,
                        nid = user.absher_no,                                              
                    };
                    return View(response);
                }
                return RedirectToAction("Index", "Dashboard", new { area = "Teacher" });
            }
        }

        [HttpPost]
        public ActionResult Index(AdminTeacher model,HttpPostedFileBase photo_file, HttpPostedFileBase certificate_photo)
        {
            string user_id = Functions.GetUser().id;
            //if (!string.IsNullOrEmpty(model.photo))
            //{
            //    var ph = Functions.UploadB64File(model.photo, "~/resources/users");
            //    model.photo = ph;
            //}
            //if (!string.IsNullOrEmpty(model.certificate_photo))
            //{

            //    var ph2 = Functions.UploadB64File(model.certificate_photo, "~/resources/files");
            //    model.certificate_photo = ph2;
            //}
            using (var db = new MhanaDbEntities())
            {               
                var user = db.AspNetUsers.FirstOrDefault(f => f.Id == user_id);
                if (user != null)
                {
                    if (photo_file != null)
                    {
                        Tuple<bool, string> imgValidation = Functions.ValidateImage(photo_file);
                        if (!imgValidation.Item1)
                        {
                            TempData["Message"] = imgValidation.Item2;
                            TempData["Status"] = "danger";
                            return View();
                        }
                        string res = Functions.SaveTempFile(photo_file, "~/resources/users");
                        model.photo = res;
                    }


                    if (certificate_photo != null)
                    {
                        Tuple<bool, string> imgValidation = Functions.ValidateImage(certificate_photo);
                        if (!imgValidation.Item1)
                        {
                            TempData["Message"] = imgValidation.Item2;
                            TempData["Status"] = "danger";
                            return View();
                        }
                        string res = Functions.SaveTempFile(certificate_photo, "~/resources/files");
                        model.certificate_photo = res;
                    }
                    user.fullname = user.first_name + " " + user.last_name;
                    user.first_name = string.IsNullOrEmpty(model.first_name) ? user.first_name : model.first_name;
                    user.last_name = string.IsNullOrEmpty(model.last_name) ? user.last_name : model.last_name;
                    user.gender = string.IsNullOrEmpty(model.gender) ? user.gender : model.gender;
                    user.PhoneNumber = string.IsNullOrEmpty(model.mobile) ? user.PhoneNumber : model.mobile;
                    user.city = string.IsNullOrEmpty(model.city) ? user.city : model.city;
                    user.region = string.IsNullOrEmpty(model.region) ? user.region : model.region;
                    user.certificate= string.IsNullOrEmpty(model.certificate) ? user.certificate : model.certificate;
                    user.certificate_photo = string.IsNullOrEmpty(model.certificate_photo) ? user.certificate_photo : model.certificate_photo;
                    user.photo =  model.photo;
                    user.absher_no = model.nid;
                    user.dob = !model.dob.HasValue ? user.dob : model.dob;
                    user.education_level_text =( model.education_level !=null && model.education_level.Count() > 0) ? string.Join(",", model.education_level) : user.education_level_text;
                    user.details = string.IsNullOrEmpty(model.details) ? user.details : model.details;
                    user.university = string.IsNullOrEmpty(model.university) ? user.university : model.university;
                    user.teaching_mechanism = string.IsNullOrEmpty(model.teaching_mechanism) ? user.teaching_mechanism : model.teaching_mechanism;
                    user.specialization = !model.specialization.HasValue ? user.specialization : model.specialization;
                    user.branch_specialization = !model.branch_specialization.HasValue ? user.branch_specialization : model.branch_specialization;
                    user.is_complete = 1;
                    user.online_cost = model.online_cost.HasValue ? model.online_cost + "" : "0";
                    user.site_cost = model.site_cost.HasValue ? model.site_cost + "" : "0";
                    db.Entry(user).State = EntityState.Modified;
                    var Updated = db.SaveChanges();
                    List<Teacher_Times> TT = new List<Teacher_Times>();
                    List<string> el = new List<string>();
                    if (Updated!=0)
                    {
                        if (model.teacher_times.Count() > 0)
                        {

                            //using (var db = new MhanaDbEntities())
                            //{
                                var removed = db.Teacher_Times.Where(w => w.user_id == user_id).ToList();
                                foreach (var item in removed)
                                {
                                    db.Teacher_Times.Remove(item);
                                }
                                foreach (var item in model.teacher_times)
                                {
                                    if (item.day_number.HasValue) { 
                                        var _ct = new Teacher_Times() { day_number = item.day_number, from_time = item.from, to_time = item.to, user_id = user_id };
                                        db.Teacher_Times.Add(_ct);                                        
                                    }
                                }
                                db.SaveChanges();
                            //}
                        }
                       // Session["User"] = user;
                    }
                }
            }
            return getMessage(Models.Enums.MStatus.check, "", "Index", "Account");
        }

        public ActionResult Contact()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Contact(ContactModelCompany model)
        {
            return getMessage(MStatus.check, "تمت العملية بنجاح", "Contact", "Account");
        }


        public ActionResult Change_password()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Change_password(ResetPasswordModel model)
        {
            var u = Functions.GetUser();
            using (var context = new ApplicationDbContext())
            {
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var user = await UserManager.FindByNameAsync(u.email);
                if (user != null && UserManager.CheckPassword(user, model.CurrentPassword))
                {
                    var result = await UserManager.ChangePasswordAsync(user.Id, model.CurrentPassword, model.Password);
                    return RedirectToAction("login", "Home", new { area = "" });
                }
            }
            return getMessage(Models.Enums.MStatus.remove, "هنالك خطأ في كلمة المرور الحالية", "Change_password", "Account");
        }

        public ActionResult FAQs()
        {

            return View();
        }
    }

    public class AdminTeacher : GeneralUser
    {
        public string details { get; set; }
        public string certificate { get; set; }
        public string certificate_photo { get; set; }
        public int? specialization { get; set; }
        public int? branch_specialization { get; set; }
        /// <summary>
        /// online -- site -- all
        /// </summary>
        public string teaching_mechanism { get; set; }
        public string university { get; set; }
        public List<string> education_level { get; set; }
        public double? online_cost { get; set; }
        public double? site_cost { get; set; }
        public List<TeacherTimes> teacher_times { get; set; }

    }

}