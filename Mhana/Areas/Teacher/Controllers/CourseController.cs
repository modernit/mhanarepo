﻿using Mhana.Helper;
using Mhana.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Mhana.Areas.Teacher.Controllers
{
    public class CourseController : BaseController
    {
        // GET: Teacher/Course
        public ActionResult Index()
        {
            var user_id = Functions.GetUser().id;
            using (var db = new MhanaDbEntities())
            {
                var items = db.Courses.AsEnumerable().Where(w => w.teacher_id == user_id).Select(s => new CourseListAdmin()
                {
                    cdate = s.cdate + "",
                    id = s.Id,
                    lectures_count = s.lectures_count,
                    period = s.period,
                    photo = s.photo,
                    start_date = s.start_date,
                    start_time = s.start_date.HasValue ? s.start_date.Value.TimeOfDay : DateTime.Now.TimeOfDay,
                    status = s.status,
                    title = s.title,
                    teacher_name = s.AspNetUser.fullname,
                    total_std = s.total_std
                }).OrderByDescending(o => o.id).ToList();
                return View(items);
            }
        }
        public ActionResult Create(int? req_id=null)
        {
            var user_id = Functions.GetUser().id;
            using (var db = new MhanaDbEntities())
            {
                ViewBag.Contracts = db.StdNewRequests.Where(w => w.teacher_id == user_id && (w.status!=0|| w.status != -1)).Select(s => new keyValue() { id = s.Id, value = s.AspNetUser.first_name + " "+ s.AspNetUser.last_name }).OrderByDescending(o=>o.id).ToList();
                ViewBag.request = db.StdNewRequests.Where(w => w.Id == req_id).FirstOrDefault();
                string material = Functions.getOption("material");
                ViewBag.material = !string.IsNullOrEmpty(material) ? material.Split(',').ToList() : new List<string>();
                return View();
            }
        }
        [HttpPost]
        public ActionResult Create(CreateCourse model)
        {
            var user_id = Functions.GetUser().id;

            using (var db = new MhanaDbEntities())
            {
                List<Course_Times> ct = new List<Course_Times>();
                var request_id = db.StdNewRequests.Where(w => w.Id == model.request_id).FirstOrDefault();
                if (model.schedual != null)
                {
                    foreach (var item in model.schedual)
                    {
                        if(item!=null)
                        {
                            Course_Times _ct = new Course_Times()
                            {
                                sch_date = item,
                                sch_time = item.TimeOfDay,
                                teacher_id = user_id,
                                status = 0,

                            };
                            ct.Add(_ct);
                        }
                       
                    }
                }

                Course c = new Course()
                {
                    title = model.title,
                    allow_sound = (model.live_type == "video" || model.live_type == "audio") ? true : false,
                    allow_video = model.live_type == "video" ? true : false,
                    cdate = DateTime.Now,
                    cost = model.cost,
                    details = model.details,
                    lectures_count = model.lectures_count,
                    period = model.period,
                    start_date = model.start_date,
                    start_time = model.start_date.HasValue ? model.start_date.Value.TimeOfDay : DateTime.Now.TimeOfDay,
                    teacher_id = user_id,
                    status = 1,
                    teaching_mechanism = model.teaching_mechanism,
                    total_std = model.total_std,
                    Course_Times = ct,
                    request_id = model.request_id,
                    is_test = model.is_test,
                    material = model.material                       
                };
                var elemnt =   db.Courses.Add(c);
                db.SaveChanges();
                if(request_id !=null && elemnt !=null)
                {
                    Std_Course sc = new Std_Course()
                    {
                        cdate = DateTime.Now,
                        course_id = elemnt.Id,
                        status = 1,
                        std_id = request_id != null ? request_id.std_id:"",                         
                   };

                    db.Std_Course.Add(sc);
                    db.SaveChanges();

                    try
                    {
                        var fcm = db.AspNetUsers.Where(w => w.Id == request_id.std_id).AsEnumerable().Select(s => s.fcm).ToList();
                        if (fcm.Count() > 0)
                        {
                            string msg =  "لقد قام المعلم "+elemnt.AspNetUser.fullname + " باضافتك الى درس جديد يرجى مراجعة قائمة دروسي في التطبيق";
                            Functions.SendNotification(fcm, msg, "token", request_id.Id, 2, request_id.std_id);
                        }
                    }
                    catch (Exception ex)
                    {

                    }

                }
                
                return getMessage(Models.Enums.MStatus.check, "", "Index", "Course");

            }
        }

        public ActionResult SetTime(int? id,DateTime? start_date,TimeSpan? start_time)
        {
            var user_id = Functions.GetUser().id;
            using (var db = new MhanaDbEntities())
            {
                var _course = db.Courses.FirstOrDefault(f => f.Id == id);
                DateTime _d = new DateTime(start_date.Value.Year, start_date.Value.Month, start_date.Value.Day, start_time.Value.Hours, start_time.Value.Minutes,0);
                Course_Times ct = new Course_Times()
                {
                    course_id = id,
                    sch_date = _d,
                    sch_time = start_time,
                };
                db.Course_Times.Add(ct);
                db.SaveChanges();
                db.Entry(ct).State = EntityState.Detached;
                db.Entry(_course).State = EntityState.Detached;
                string atten_url = "";
                string wiziqId = Functions.CreateWiziq(_course, ct);
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/debug.txt"), true))
                    file.WriteLine("wiziqId : " + wiziqId);
                if (!string.IsNullOrEmpty(wiziqId))
                {
                    atten_url = Functions.AddStudentToWiziq(_course, wiziqId);
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/debug.txt"), true))
                        file.WriteLine("atten_url : " + atten_url);
                }
                else
                {
                    return getMessage(Models.Enums.MStatus.remove, "هنالك خطأ في البيانات", "Edit", "Course", id);
                }
                return getMessage(Models.Enums.MStatus.check, "", "Edit", "Course", id);
            }
        }
        public ActionResult Edit(int? id)
        {
            var user_id = Functions.GetUser().id;
            using (var db = new MhanaDbEntities())
            {
                Course c = db.Courses.Include(i => i.Course_Times).Where(w => w.Id == id).FirstOrDefault();
                ViewBag.stds = c.Std_Course.Select(s => new courseStdModel { fullname =  s.AspNetUser.fullname, cdate= s.cdate, phonenumber=  s.AspNetUser.PhoneNumber, email=  s.AspNetUser.Email ,id = s.AspNetUser.Id, first_name = s.AspNetUser.first_name,last_name = s.AspNetUser.last_name }).ToList();
                ViewBag.Contracts = db.StdNewRequests.Where(w => w.teacher_id == user_id && (w.status != 0 || w.status != -1)).Select(s => new keyValue() { id = s.Id, value = s.AspNetUser.fullname }).OrderByDescending(o => o.id).ToList();
                string material = Functions.getOption("material");
                ViewBag.material = !string.IsNullOrEmpty(material) ? material.Split(',').ToList() : new List<string>();
                //  ViewBag.request = db.StdNewRequests.Where(w => w.Id == c.request_id).FirstOrDefault();
                return View(c);
            }
        }
        public ActionResult EditReport(int? cid,string std)
        {
            var user_id = Functions.GetUser().id;
            using (var db = new MhanaDbEntities())
            {
                var c = db.MyRates.Where(w => w.course_id == cid && w.std_id == std).FirstOrDefault();
                var stdrate = db.AspNetUsers.Where(w => w.Id == std).FirstOrDefault();
                ViewBag.fullname = stdrate.fullname;
                ViewBag.email = stdrate.Email;
                ViewBag.phonenumber = stdrate.PhoneNumber;
                return View(c);
            }
        }

        [HttpPost]
        public ActionResult EditReport(MyRate model)
        {
            var user_id = Functions.GetUser().id;
            using (var db = new MhanaDbEntities())
            {
                var _rate = new MyRate()
                {
                    cdate = DateTime.Now,
                    comment = model.comment,
                    course_id = model.course_id,
                    rate = model.rate,
                    std_id = model.std_id,
                    teacher_id = user_id,
                    report = model.report,
                    rate_type = 1
                };
                db.MyRates.Add(_rate);
                db.SaveChanges();
                return getMessage(Models.Enums.MStatus.check, "", "Edit", "Course", model.course_id);
            }
        }


        [HttpPost]
        public ActionResult Edit(CreateCourse model)
        {
            var user_id = Functions.GetUser().id;
            using (var db = new MhanaDbEntities())
            {

                Course c = db.Courses.Where(w => w.Id == model.id).FirstOrDefault();

                c.title = model.title;
                c.allow_sound = model.both.HasValue ? true : model.allow_sound.HasValue;
                c.allow_video = model.both.HasValue ? true : model.allow_video.HasValue;
                c.material = model.material;
                c.cost = model.cost;
                c.details = model.details;
                c.lectures_count = model.lectures_count;
                c.period = model.period;
                c.start_date = model.start_date;
                c.start_time = model.start_date.HasValue ? model.start_date.Value.TimeOfDay : DateTime.Now.TimeOfDay;
                //c.teacher_id = model.teacher_id;
                //c.status = 1;
                c.teaching_mechanism = model.teaching_mechanism;
                c.total_std = model.total_std;
                //c.is_test = model.is_test;
                //c.request_id = model.request_id;

                List<Course_Times> ct = new List<Course_Times>();
                if (model.schedual != null)
                {
                    foreach (var item in model.schedual)
                    {
                        if (item > DateTime.Now)
                        {
                            Course_Times _ct = new Course_Times()
                            {
                                sch_date = item,
                                sch_time = item.TimeOfDay,
                                teacher_id = user_id,
                                 course_id = c.Id,
                                  status = 0,
                                   
                            };
                            db.Course_Times.Add(_ct);
                        }
                    }
                }

                db.Entry(c).State = EntityState.Modified;
                db.SaveChanges();
                return getMessage(Models.Enums.MStatus.check, "", "Index", "Course");

            }
        }

        public ActionResult Calendar()
        {
            var user_id = Functions.GetUser().id;
            using (var db = new MhanaDbEntities())
            {

                var today = db.Course_Times.Where(w => w.Course.teacher_id == user_id && w.sch_date.HasValue && w.sch_date.Value.Day == DbFunctions.AddHours(DateTime.Now,3).Value.Day && w.sch_date.Value.Month == DbFunctions.AddHours(DateTime.Now, 3).Value.Month && w.sch_date.Value.Year == DbFunctions.AddHours(DateTime.Now, 3).Value.Year).Select(s => new todayLesson()
                {
                    id=s.id,
                    course_id = s.course_id,
                    course_name = s.Course.title,
                    lesson_number = "5",
                    sch_date = s.sch_date,
                    sch_time = s.sch_time,
                    total_std = s.Course.total_std,
                    link = s.teacher_link,
                    status = s.status

                }).OrderBy(o => o.sch_date).ToList();
                var dd = DateTime.Now.AddMonths(-1);
                ViewBag.course = db.Course_Times.Where(w => w.Course.teacher_id == user_id && w.sch_date.HasValue && w.sch_date>= dd).Select(s => new todayLesson()
                {
                    id = s.id,
                    course_id = s.course_id,
                    course_name = s.Course.title,
                    lesson_number = "5",
                    sch_date = s.sch_date,
                    sch_time = s.sch_time,
                    total_std = s.Course.total_std,
                    link = s.teacher_link,
                    status = s.status

                }).OrderBy(o => o.sch_date).ToList();
                //Course c = db.Courses.Include(i => i.Course_Times).Where(w => w.Id == id).FirstOrDefault();
                return View(today);
            }
        }

        //public ActionResult ActiveToday(int? id)
        //{
        //    var user_id = Functions.GetUser().id;
        //    using (var db = new MhanaDbEntities())
        //    {
        //        var check_user = db.AspNetUsers.Where(w => w.Id == user_id).FirstOrDefault();

        //        //if(string.IsNullOrEmpty(check_user.wiziq_id))
        //        //{
        //        //    TeacherWiziQ Tw = new TeacherWiziQ()
        //        //    {
        //        //        about_the_teacher = check_user.details,
        //        //        can_schedule_class = "true",
        //        //        email = check_user.Email,
        //        //        is_active = "true",
        //        //        name = check_user.fullname,
        //        //        mobile_number = "",
        //        //        phone_number = "",
        //        //        password = "123456"
        //        //    };
        //        //    string result = WiziQ.WizIQClass.AddTeacher(Tw);
        //        //    using (System.IO.StreamWriter file =new System.IO.StreamWriter(Server.MapPath("~/debug.txt"), true))
        //        //        file.WriteLine(result);
        //        //    if (!string.IsNullOrEmpty(result)&& result.Substring(0,20).Contains("ok"))
        //        //    {
        //        //        rsp resultObject = Functions.DeserializeXML<rsp>(result);
        //        //        check_user.wiziq_id = resultObject.add_teacher.teacher_id + "";
        //        //        db.Entry(check_user).State = EntityState.Modified;
        //        //        db.SaveChanges();
        //        //    }                   
        //        //}
        //        var wiziq_id = db.WiziqIDs.Where(w => w.status == 0).FirstOrDefault();
        //        var course_time = db.Course_Times.Where(w => w.id == id).FirstOrDefault();
        //        var _course = course_time.Course;

        //        string atten_url = "";
        //        string wiziqId = Functions.CreateWiziq(_course, course_time);
        //        using (System.IO.StreamWriter file = new System.IO.StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/debug.txt"), true))
        //            file.WriteLine("wiziqId : " + wiziqId);
        //        if (!string.IsNullOrEmpty(wiziqId))
        //        {
        //            atten_url = Functions.AddStudentToWiziq(_course, wiziqId);
        //            using (System.IO.StreamWriter file = new System.IO.StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/debug.txt"), true))
        //                file.WriteLine("atten_url : " + atten_url);
        //        }
        //        else
        //        {
        //            return getMessage(Models.Enums.MStatus.remove, "هنالك خطأ في البيانات", "Calendar", "Course");                 
        //        }


        //       // if (string.IsNullOrEmpty(wiziq_id.wiziq_id))
        //       //     {

        //       //     CourseWiziQ Cw = new CourseWiziQ()
        //       //     {
        //       //         attendee_limit = 2 + "",
        //       //         create_recording = "true",
        //       //         duration = _course.is_test == 1 ? "10" : "60",
        //       //         start_time = course_time.sch_date.Value.Month + "/" + course_time.sch_date.Value.Day + "/" + course_time.sch_date.Value.Year + " " + course_time.sch_date.Value.AddHours(1).Hour + ":" + course_time.sch_date.Value.Minute,
        //       //         title = _course.title + course_time.id,
        //       //         //presenter_email = "mohanna111@gmail.com",
        //       //          presenter_id = wiziq_id.wiziq_id,
        //       //          presenter_name = wiziq_id.email,
        //       //          language_culture_name = "ar-sa",
        //       //             presenter_default_controls = "audio, video",
        //       //             time_zone = "Asia/Riyadh"
        //       //         };
        //       //         string c_response = WiziQ.WizIQClass.Create(Cw);
        //       //         using (System.IO.StreamWriter file = new System.IO.StreamWriter(Server.MapPath("~/debug.txt"), true))
        //       //             file.WriteLine("/n"+c_response + Cw.start_time);
        //       //         if (!string.IsNullOrEmpty(c_response) && c_response.Substring(0, 20).Contains("ok"))
        //       //         {
        //       //             rsp resultObject = Functions.DeserializeXML<rsp>(c_response);
        //       //             course_time.teacher_link = resultObject.create.class_details.presenter_list.presenter.presenter_url;
        //       //             course_time.wiziq_id = resultObject.create.class_details.class_id;
        //       //             course_time.status = 1;
        //       //             _course.course_live_url = resultObject.create.class_details.presenter_list.presenter.presenter_url;
        //       //             db.Entry(course_time).State = EntityState.Modified;
        //       //             db.Entry(_course).State = EntityState.Modified;
        //       //             db.SaveChanges();
        //       //         }
        //       //     }
        //       // var stds_reg = db.Std_Course.Where(w => w.course_id == _course.Id && w.status == 1).ToList();
        //       // var stds = stds_reg.Where(w => w.course_id == _course.Id && w.status == 1).Select(s => new atten_info()
        //       // {
        //       //     lang = "ar-sa",
        //       //     name = string.IsNullOrEmpty(s.AspNetUser.fullname)?(s.AspNetUser.Email): s.AspNetUser.fullname,
        //       //     user_id = s.std_id
        //       // }).ToList();
        //       // string xml_attendance = "";
        //       // foreach(var item in stds)
        //       // {
        //       //     xml_attendance += "<attendee><attendee_id>" + item.user_id + "</attendee_id><screen_name>" + item.name + "</screen_name><language_culture_name>" + item.lang + "</language_culture_name></attendee>";
        //       // }
        //       //var std_response =  WiziQ.WizIQClass.AddAttendees(course_time.wiziq_id, xml_attendance);
        //       // using (System.IO.StreamWriter file = new System.IO.StreamWriter(Server.MapPath("~/debug.txt"), true))
        //       //     file.WriteLine("/n" + std_response);
        //       // if (!string.IsNullOrEmpty(std_response) && std_response.Substring(0, 20).Contains("ok"))
        //       // {
        //       //     rsp resultObject = Functions.DeserializeXML<rsp>(std_response);
        //       //     foreach (var item in resultObject.add_attendees.attendee_list)
        //       //     {
        //       //         var std_time = stds_reg.Where(w => w.std_id == item.attendee_id).FirstOrDefault();
        //       //         if (std_time != null)
        //       //         {
        //       //             std_time.lesson_link = item.attendee_url;
        //       //             db.Entry(std_time).State = EntityState.Modified;
                           
        //       //         }
                          
        //       //     }
        //       //     db.SaveChanges();
        //       // }
        //        return getMessage(Models.Enums.MStatus.check, "", "Calendar", "Course");
        //    }
        //}

        public ActionResult CancelToday(int? id)
        {
            var user_id = Functions.GetUser().id;
            using (var db = new MhanaDbEntities())
            {
                var check_course = db.Course_Times.Where(w => w.id == id).FirstOrDefault();
                var cid = check_course.course_id;
                if (check_course!=null && check_course.Course.teacher_id== user_id)
                {

                    string result = WiziQ.WizIQClass.Cancel(check_course.wiziq_id);
                    //using (System.IO.StreamWriter file = new System.IO.StreamWriter(Server.MapPath("~/debug.txt"), true))
                    //    file.WriteLine(result);
                    //check_course.status = -1;
                    //check_course.teacher_link = "";
                    db.Course_Times.Remove(check_course);
                    db.SaveChanges();

                }               
                return getMessage(Models.Enums.MStatus.check, "", "Edit", "Course", cid);
            }
        }

    }

    public class courseStdModel
    {
        public string id { get; set; }
        public string fullname { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string phonenumber { get; set; }
        public DateTime? cdate { get; set; }
    }
}