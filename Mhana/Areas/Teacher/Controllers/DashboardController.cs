﻿using Mhana.Helper;
using Mhana.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mhana.Areas.Teacher.Controllers
{
    public class DashboardController : BaseController
    {
        // GET: Teacher/Dashboard
        public ActionResult Index()
        {
            using (var db = new MhanaDbEntities())
            {
                var user_id = Functions.GetUser().id;
                var courses = db.Courses.Where(w => w.teacher_id == user_id).ToList();
                ViewBag.courses = courses.Where(w => w.teacher_id == user_id).Count();
                ViewBag.requests = db.StdNewRequests.Where(w => w.teacher_id == user_id).Count();
                var income = db.AspNetUsers.Where(w => w.Id == user_id).FirstOrDefault()!=null? db.AspNetUsers.Where(w => w.Id == user_id).FirstOrDefault().total_income:0;
                ViewBag.payments = income.HasValue ? income : 0;
                ViewBag.online = courses.Where(w => w.teaching_mechanism == "online").Count();
                ViewBag.onsite = courses.Where(w => w.teaching_mechanism == "onsite").Count();
                ViewBag.both = courses.Where(w => w.teaching_mechanism == "both").Count();
                ViewBag.education_level_text = db.AspNetUsers.Where(w => w.AspNetRoles.FirstOrDefault() != null && w.AspNetRoles.FirstOrDefault().Id == "3" && !string.IsNullOrEmpty(w.education_level_text)).Select(s => s.education_level_text).ToList();
                var dd = DateTime.Now.AddMonths(-1);
                ViewBag.course = db.Course_Times.Where(w => w.Course.teacher_id == user_id && w.sch_date.HasValue && w.sch_date >= dd).Select(s => new todayLesson()
                {
                    id = s.id,
                    course_id = s.course_id,
                    course_name = s.Course.title,
                    lesson_number = "5",
                    sch_date = s.sch_date,
                    sch_time = s.sch_time,
                    total_std = s.Course.total_std,
                    link = s.teacher_link,
                    status = s.status

                }).OrderBy(o => o.sch_date).ToList();
                return View();
            }
        }

        public ActionResult HowWork()
        {
            return View();
        }
    }
}