﻿using Mhana.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mhana.Areas.Teacher.Controllers
{
    [AuthFilter(Roles = "2")]
    public class BaseController : Controller
    {
        public BaseController()
        {
            //try
            //{

            //    if (Session != null && Session["User"] != null)
            //    {
            //        Mhana.Models.ApplicationUser user = (Mhana.Models.ApplicationUser)Session["User"];
            //        if (user.is_complete != 1)
            //        {
            //            Response.Redirect("~/Teacher/Account/");
            //        }

            //    }
            //    else
            //    {
            //        Response.Redirect("~/Home/Login");
            //    }
            //}catch(Exception ex)
            //{
            //    Response.Redirect("~/Home/Login");

            //}
           
            
        }
        // GET: Teacher/Base
        public ActionResult getMessage(Enum status, string meesage, string action, string controller, int? id = null)
        {
            TempData["Message"] = string.IsNullOrEmpty(meesage) ? "تمت العملية بنجاح" : meesage;
            TempData["Status"] = status;
            return RedirectToAction(action, controller, new { id = id });
        }
        public ActionResult getMessage(Enum status, string meesage, string action, string controller, string id)
        {
            TempData["Message"] = string.IsNullOrEmpty(meesage) ? "تمت العملية بنجاح" : meesage;
            TempData["Status"] = status;
            return RedirectToAction(action, controller, new { id = id });
        }
        public ActionResult getMessage(Enum status, string meesage, string action, string controller)
        {
            TempData["Message"] = string.IsNullOrEmpty(meesage) ? "تمت العملية بنجاح" : meesage;
            TempData["Status"] = status;
            return RedirectToAction(action, controller);
        }
        public ActionResult getMessage(Enum status, string meesage, string returnUrl)
        {
            TempData["Message"] = string.IsNullOrEmpty(meesage) ? "تمت العملية بنجاح" : meesage;
            TempData["Status"] = status;
            return Redirect(returnUrl);
        }
    }
}