﻿using Mhana.Helper;
using Mhana.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Mhana.Areas.Admin.Controllers
{
    public class StudentsController : BaseController
    {
        // GET: Admin/Students
        public ActionResult Index(string name, int? course_id, string email, string mobile)
        {
            using (var db = new MhanaDbEntities())
            {
                var levels = db.education_level.AsEnumerable();
                var item = db.AspNetUsers.Where(w => w.AspNetRoles.FirstOrDefault() != null && w.AspNetRoles.FirstOrDefault().Id == "3").Select(s => new TeacherListAdmin()
                {
                    absher = s.absher,
                    cdate = s.cdate,
                    email = s.Email,
                    fullname = !string.IsNullOrEmpty(s.fullname) ? s.fullname : (s.first_name + " " + s.last_name),
                    id = s.Id,
                    mobile = s.PhoneNumber,
                    photo = s.photo,
                    status = s.status,
                    specialization = levels.FirstOrDefault(w => w.Id + "" == s.education_level_text) != null ? levels.FirstOrDefault(w => w.Id + "" == s.education_level_text).name : "---"

                });

                if (!string.IsNullOrEmpty(name))
                {
                    item = item.Where(w => !string.IsNullOrEmpty(w.fullname) && w.fullname.Contains(name));
                }
                if (!string.IsNullOrEmpty(email))
                {
                    item = item.Where(w => !string.IsNullOrEmpty(w.email) && w.email == email);
                }
                if (!string.IsNullOrEmpty(mobile))
                {
                    item = item.Where(w => !string.IsNullOrEmpty(w.mobile) && w.mobile == mobile);
                }
                if (course_id.HasValue)
                {
                    item = item.Where(w => w.course_ids != null && w.course_ids.Contains(course_id.Value));
                }
                return View(item.OrderByDescending(o => o.cdate).ToList());
            }
        }

        public ActionResult Edit(string id)
        {
            using (var db = new MhanaDbEntities())
            {
                var item = db.AspNetUsers.Where(w => w.Id == id).FirstOrDefault();
                if (item == null)
                    return getMessage(Models.Enums.MStatus.check, "", "Index", "Students");
                ViewBag.Courses = db.Std_Course.Where(w => w.std_id == item.Id).Select(s => new CourseList()
                {
                    id = s.course_id,
                    cdate = s.cdate + "",
                    status = s.status,
                    teacher_name = s.Course.AspNetUser.fullname,
                    title = s.Course.title,
                    teacher_id = s.Course.teacher_id,


                }).OrderByDescending(o => o.cdate).ToList();

                //ViewBag.levels = db.education_level.ToList();
                if (!string.IsNullOrEmpty(item.education_level_text))
                {
                    var levels = item.education_level_text.Split(',');
                    var string_levels = db.education_level_sub.Where(w => levels.Contains(w.id + "")).Select(s => s.name).ToList();
                    if (string_levels.Count() > 0)
                        ViewBag.eduLevels = string.Join(",", string_levels);
                    else
                        ViewBag.eduLevels = "---";
                }
                return View(item);
            }
        }
        public ActionResult Requests()
        {
            using (var db = new MhanaDbEntities())
            {
                var items = db.StdNewRequests.AsEnumerable().Select(s => new NewRequestList()
                {
                    cdate = s.cdate + "",
                    id = s.Id,
                    period = s.period,
                    start_date = s.start_date,
                    start_time = s.start_date.HasValue ? s.start_date.Value.TimeOfDay : DateTime.Now.TimeOfDay,
                    status = s.status,
                    student_name = s.AspNetUser != null ? string.IsNullOrEmpty(s.AspNetUser.fullname) ? (s.AspNetUser.first_name + " " + s.AspNetUser.last_name) : s.AspNetUser.fullname : "---",
                    details = s.details,
                    teaching_mechanism = s.teaching_mechanism,
                    material = s.material
                }).OrderByDescending(o => o.id).ToList();
                return View(items);
            }
        }
        public ActionResult EditRequest(int? id)
        {
            using (var db = new MhanaDbEntities())
            {
                var c = db.StdNewRequests.Include(i => i.AspNetUser).Where(w => w.Id == id).FirstOrDefault();
                return View(c);
            }
        }
        [HttpPost]
        public ActionResult EditRequest(EditCourse model)
        {
            using (var db = new MhanaDbEntities())
            {

                var _request = db.StdNewRequests.Where(w => w.Id == model.id).FirstOrDefault();

                _request.status = model.status;
                _request.teacher_notes = model.details;
                db.Entry(_request).State = EntityState.Modified;
                db.SaveChanges();
                return getMessage(Models.Enums.MStatus.check, "", "Requests", "Students");

            }
        }
        public ActionResult deactive(string id)
        {

            using (var context = new ApplicationDbContext())
            {
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var checkUser = UserManager.FindById(id);
                //if (checkUser != null)
                //    return getMessage(Enums.MStatus.danger, "البريد الالكتروني مستخدم مسبقاً", "Create", "Manager");
                checkUser.status = -1;
                UserManager.Update(checkUser);
                return getMessage(Models.Enums.MStatus.check, "", "Index", "Students");
            }
        }

        public ActionResult deactiveRequest(int? id)
        {

            using (var db = new MhanaDbEntities())
            {
                var item = db.StdNewRequests.FirstOrDefault(w => w.Id == id);
                if (item != null)
                {
                    item.status = -1;
                }
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                var fcm = db.AspNetUsers.Where(w => w.Id == item.std_id).AsEnumerable().Select(s => s.fcm).ToList();
                if (fcm.Count() > 0)
                {
                    string msg = "لقد تم رفض طلبك الذي قمت بارساله";
                    Functions.SendNotification(fcm, msg, "token", item.Id, 1, item.std_id);
                }
                return getMessage(Models.Enums.MStatus.check, "", "Index", "Students");
            }
        }

        public ActionResult DeleteRate(int? id)
        {

            using (var db = new MhanaDbEntities())
            {
                var rate = db.MyRates.FirstOrDefault(f => f.id == id);
                db.MyRates.Remove(rate);
                db.SaveChanges();
                return getMessage(Models.Enums.MStatus.check, "", "Rates", "Students");
            }
        }


        public ActionResult Active(string id)
        {

            using (var context = new ApplicationDbContext())
            {
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var checkUser = UserManager.FindById(id);
                //if (checkUser != null)
                //    return getMessage(Enums.MStatus.danger, "البريد الالكتروني مستخدم مسبقاً", "Create", "Manager");
                checkUser.status = 1;
                UserManager.Update(checkUser);
                return getMessage(Models.Enums.MStatus.check, "", "Index", "Students");
            }
        }
        public ActionResult RemoveFromCourse(int? id, string std)
        {

            using (var context = new MhanaDbEntities())
            {
                var _course = context.Std_Course.Where(w => w.course_id == id && w.std_id == std).FirstOrDefault();
                _course.status = -1;
                context.Entry(_course).State = EntityState.Modified;
                context.SaveChanges();
                return getMessage(Models.Enums.MStatus.check, "", "Edit", "Students", std);
            }
        }
        public ActionResult AcceptInCourse(int? id, string std)
        {

            using (var context = new MhanaDbEntities())
            {
                var _course = context.Std_Course.Where(w => w.course_id == id && w.std_id == std).FirstOrDefault();
                _course.status = 1;
                context.Entry(_course).State = EntityState.Modified;
                context.SaveChanges();
                return getMessage(Models.Enums.MStatus.check, "", "Edit", "Students", std);
            }
        }
        public ActionResult Delete(string id)
        {
            using (var db = new MhanaDbEntities())
            {
                var checkUser = db.AspNetUsers.Where(w => w.Id == id).FirstOrDefault();
                if (checkUser != null)
                {
                    var reqs = db.StdNewRequests.Where(w => w.std_id == id).ToList();
                    foreach (var item in reqs)
                    {
                        db.StdNewRequests.Remove(item);
                    }
                    var MyRates = db.MyRates.Where(w => w.std_id == id).ToList();
                    foreach (var item in MyRates)
                    {
                        db.MyRates.Remove(item);
                    }
                    var Std_Course = db.Std_Course.Where(w => w.std_id == id).ToList();
                    foreach (var item in Std_Course)
                    {
                        db.Std_Course.Remove(item);
                    }

                    var Pockets = db.Pockets.Where(w => w.user_id == id).ToList();
                    foreach (var item in Pockets)
                    {
                        db.Pockets.Remove(item);
                    }

                    var Invites = db.Invites.Where(w => w.send_by == id).ToList();
                    foreach (var item in Invites)
                    {
                        db.Invites.Remove(item);
                    }

                    db.SaveChanges();
                    db.AspNetUsers.Remove(checkUser);
                    db.SaveChanges();

                }

                return getMessage(Models.Enums.MStatus.check, "", "Index", "Students");
            }
        }
        public ActionResult Rates()
        {
            using (var db = new MhanaDbEntities())
            {

                var items = db.MyRates.Where(w => w.rate_type == 1).Select(s => new RateModel()
                {
                    id = s.id,
                    cdate = DateTime.Now,
                    comment = s.comment,
                    course_title = s.Course.title,
                    rate = s.rate,
                    course_id = s.course_id,
                    std_id = s.std_id,
                    student_name = s.AspNetUser.fullname,
                    teacher_name = s.AspNetUser1.fullname,
                    photo = "/resources/users/" + s.AspNetUser.photo

                }).OrderByDescending(o => o.id).ToList();
                return View(items);
            }
        }

        public ActionResult Invites(int? id)
        {
            var user_id = Functions.GetUser().id;
            using (var db = new MhanaDbEntities())
            {
                var c = db.Invites.OrderByDescending(o => o.id).ToList();
                return View(c);
            }
        }

        public ActionResult Invites_Std()
        {
            using (var db = new MhanaDbEntities())
            {
                ViewBag.Courses = db.Courses.Where(w => w.status == 1).Select(s => new keyValue() { id = s.Id, value = s.title }).ToList();
                return View();
            }

        }
        [HttpPost]
        public async Task<ActionResult> Invites_Std(Invite model, int? type)
        {
            var user_id = Functions.GetUser().id;
            using (var db = new MhanaDbEntities())
            {
                model.cdate = DateTime.Now;
                model.send_by = user_id;
                model.status = 0;
                model.invite_type = 0; //0 for student without reg 1 for student with reg
                db.Invites.Add(model);
                db.SaveChanges();
                if (type != 1)
                {
                    emailInviteS mail = new emailInviteS()
                    {
                        from = Functions.GetUser().fullname,
                        msg = "لقد تم دعوتك من قبل المعلم " + Functions.GetUser().fullname + "للمشاركة في درس في تطبيق درس خصوصي نتمن القيام بتحميل التطبيق الخاص بدرس خصوصي من الروابط التالية  ",
                        title = "دعوة طالب للمشاركة في درس خصوصي",
                        password = "",
                        username = "",
                        to = model.email,
                        cdate = string.Format("{0:MMM/dd/yyyy}", DateTime.Now)
                    };
                    new Mhana.Controllers.MailerController().InviteS(mail).Deliver();
                }
            }
            if (type == 1)
            {
                using (var context = new ApplicationDbContext())
                {
                    var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                    var checkUser = UserManager.FindByName(model.email);
                    if (checkUser != null)
                        return getMessage(Models.Enums.MStatus.check, "البريد الالكتروني مستخدم مسبقاً", "Invites_Std", "Students");
                    var user = new ApplicationUser
                    {
                        cdate = DateTime.Now,
                        Email = model.email,
                        fullname = model.std_name,
                        status = 0,
                        UserName = model.email,
                        is_complete = 0
                    };

                    string pass = System.Web.Security.Membership.GeneratePassword(8, 3);
                    var x = await UserManager.CreateAsync(user, pass);
                    if (model.invite_type == 1)
                        await UserManager.AddToRoleAsync(user.Id, "Student");
                    emailInviteS mail = new emailInviteS()
                    {
                        from = Functions.GetUser().fullname,
                        msg = "لقد تم دعوتك من قبل الادارة " + Functions.GetUser().fullname + "للمشاركة في درس في تطبيق درس خصوصي نتمن القيام بتحميل التطبيق الخاص بدرس خصوصي من الروابط التالية  ",
                        title = "دعوة طالب للمشاركة في درس خصوصي",
                        password = pass,
                        username = model.email,
                        to = model.email,
                        cdate = string.Format("{0:MMM/dd/yyyy}", DateTime.Now)
                    };
                    new Mhana.Controllers.MailerController().InviteS(mail).Deliver();

                }
            }

            return getMessage(Models.Enums.MStatus.check, "", "Invites", "Students");
        }
    }
}