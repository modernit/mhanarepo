﻿using Mhana.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mhana.Areas.Admin.Controllers
{
    public class CoursesController : BaseController
    {
        // GET: Admin/Courses
        public ActionResult Index()
        {
            using (var db = new MhanaDbEntities())
            {
                var levels = db.education_level.AsEnumerable();
                var item = db.Courses.Select(s => new CourseList()
                {
                    cdate = s.cdate+"",
                    id = s.Id,
                    start_date = s.start_date,
                    status = s.status,
                    std_count = s.total_std,
                    title = s.title,
                    teacher_name = (s.AspNetUser != null) ? s.AspNetUser.fullname : "---",
                    teacher_id = (s.AspNetUser != null) ? s.AspNetUser.Id : "---",
                    student_name = (s.AspNetUser != null) ? s.AspNetUser.fullname : "---"
                }).OrderByDescending(o => o.id).ToList();
                return View(item);
            }
        }
        public ActionResult Calendar()
        {            
            using (var db = new MhanaDbEntities())
            {

                var today = db.Course_Times.Where(w => w.sch_date.HasValue && w.sch_date.Value.Day == DateTime.Now.Day && w.sch_date.Value.Month == DateTime.Now.Month && w.sch_date.Value.Year == DateTime.Now.Year).Select(s => new todayLesson()
                {
                    id = s.id,
                    course_id = s.course_id,
                    course_name = s.Course.title,
                    lesson_number = "5",
                    sch_date = s.sch_date,
                    sch_time = s.sch_time,
                    total_std = s.Course.total_std,
                    link = s.teacher_link,
                    status = s.status

                }).OrderBy(o => o.sch_date).ToList();
                var dd = DateTime.Now.AddMonths(-1);
                ViewBag.course = db.Course_Times.Where(w => w.sch_date.HasValue && w.sch_date >= dd).Select(s => new todayLesson()
                {
                    id = s.id,
                    course_id = s.course_id,
                    course_name = s.Course.title,
                    lesson_number = "5",
                    sch_date = s.sch_date,
                    sch_time = s.sch_time,
                    total_std = s.Course.total_std,
                    link = s.teacher_link,
                    status = s.status

                }).OrderBy(o => o.sch_date).ToList();
                //Course c = db.Courses.Include(i => i.Course_Times).Where(w => w.Id == id).FirstOrDefault();
                return View(today);
            }
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(CreateCourse model,int? request_id)
        {
           
            using (var db = new MhanaDbEntities())
            {
                List<Course_Times> ct = new List<Course_Times>();
                if (model.schedual != null)
                {
                    foreach (var item in model.schedual)
                    {
                        Course_Times _ct = new Course_Times()
                        {
                            sch_date = item,
                            sch_time = item.TimeOfDay,
                            teacher_id = model.teacher_id,
                            status = 0,

                        };
                        ct.Add(_ct);
                    }
                }

                Course c = new Course()
                {
                    title = model.title,
                    allow_sound = (model.live_type == "both" || model.live_type == "audio") ? true : false,
                    allow_video = (model.live_type == "both" || model.live_type == "video") ? true : false,
                    cdate = DateTime.Now,
                    cost = model.cost,
                    details = model.details,
                    lectures_count = model.lectures_count,
                    period = model.period,
                    start_date = model.start_date,
                    start_time = model.start_date.HasValue ? model.start_date.Value.TimeOfDay : DateTime.Now.TimeOfDay,
                    teacher_id = model.teacher_id,
                    status = 1,
                    teaching_mechanism = model.teaching_mechanism,
                    total_std = model.total_std,
                    Course_Times = ct
                     
                };
                db.Courses.Add(c);
                db.SaveChanges();
                return getMessage(Models.Enums.MStatus.check, "", "Index", "Course");

            }
        }
        public ActionResult Edit(int? id)
        {
            using (var db = new MhanaDbEntities())
            {
                Course c = db.Courses.Include(i => i.Course_Times).Where(w => w.Id == id).FirstOrDefault();
                return View(c);
            }
        }
        [HttpPost]
        public ActionResult Edit(int?id ,int?status)
        {           
            using (var db = new MhanaDbEntities())
            {
                Course c = db.Courses.Where(w => w.Id == id).FirstOrDefault();
                c.status = status;
                db.Entry(c).State = EntityState.Modified;
                db.SaveChanges();
                return getMessage(Models.Enums.MStatus.check, "", "Index", "Courses");

            }
        }

        public ActionResult deactive(int? id)
        {

            using (var db = new MhanaDbEntities())
            {
                Course c = db.Courses.Where(w => w.Id == id).FirstOrDefault();
                c.status = -1;
                db.Entry(c).State = EntityState.Modified;
                db.SaveChanges();
                return getMessage(Models.Enums.MStatus.check, "", "Index", "Courses");
            }
        }
        public ActionResult Active(int? id)
        {

            using (var db = new MhanaDbEntities())
            {
                Course c = db.Courses.Where(w => w.Id == id).FirstOrDefault();
                c.status = 1;
                db.Entry(c).State = EntityState.Modified;
                db.SaveChanges();
                return getMessage(Models.Enums.MStatus.check, "", "Index", "Courses");
            }
        }
    }
}