﻿using Mhana.Helper;
using Mhana.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Mhana.Areas.Admin.Controllers
{
    public class UsersController : BaseController
    {
        // GET: Admin/Users
        public ActionResult Index()
        {
            using (var db = new MhanaDbEntities())
            {
                var items = db.AspNetUsers.Where(w => w.AspNetRoles.FirstOrDefault() != null && w.AspNetRoles.FirstOrDefault().Id == "1").ToList();
                return View(items.OrderByDescending(o => o.cdate));
            }
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Create(adminModel model, HttpPostedFileBase photo_file)
        {
            using (var context = new ApplicationDbContext())
            {
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var checkUser = UserManager.FindByName(model.email);
                if (checkUser != null)
                    return getMessage(Models.Enums.MStatus.check, "البريد الالكتروني مستخدم مسبقاً", "Create", "Users");
                if (photo_file != null)
                {
                    Tuple<bool, string> imgValidation = Functions.ValidateImage(photo_file);
                    if (!imgValidation.Item1)
                    {
                        TempData["Message"] = imgValidation.Item2;
                        TempData["Status"] = "danger";
                        return View();
                    }
                    string res = Functions.SaveTempFile(photo_file, "~/resources/users");
                    model.photo = res;
                }
                var user = new ApplicationUser
                {
                    cdate = DateTime.Now,
                    Email = model.email,
                    fullname = model.fullname,
                    PhoneNumber = model.phoneNumber,
                    status = (int)Models.Enums.Status.Visible,
                    UserName = model.email,
                    photo = model.photo,
                    absher_no = model.absher_no

                };

                string pass = "123456";// System.Web.Security.Membership.GeneratePassword(8, 3);
                var x = await UserManager.CreateAsync(user, pass);
                await UserManager.AddToRoleAsync(user.Id, "Admin");
                //AdminAddUser mail = new AdminAddUser()
                //{
                //    fullname = model.fullname,
                //    userName = model.UserName,
                //    Password = pass,
                //    to = model.Email,
                //    type = "كمدير نظام"
                //};
                //new Molen.Controllers.MailerController().NewAdmin(mail).Deliver();
                return getMessage(Models.Enums.MStatus.check, "", "Create", "Users");
            }
        }
        public ActionResult Edit(string id)
        {
            using (var db = new MhanaDbEntities())
            {
                var item = db.AspNetUsers.Where(w => w.Id == id).FirstOrDefault();
                return View(item);
            }
        }
        [HttpPost]
        public ActionResult Edit(adminModel model, HttpPostedFileBase photo_file)
        {
            using (var context = new ApplicationDbContext())
            {
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var checkUser = UserManager.FindById(model.id);
                if (photo_file != null)
                {
                    Tuple<bool, string> imgValidation = Functions.ValidateImage(photo_file);
                    if (!imgValidation.Item1)
                    {
                        TempData["Message"] = imgValidation.Item2;
                        TempData["Status"] = "danger";
                        return View();
                    }
                    string res = Functions.SaveTempFile(photo_file, "~/resources/users");
                    model.photo = res;
                }

                checkUser.PhoneNumber = model.phoneNumber;
                checkUser.fullname = model.fullname;
                checkUser.photo = model.photo;
                checkUser.absher_no = model.absher_no;
                UserManager.Update(checkUser);
                return getMessage(Models.Enums.MStatus.check, "", "Index", "Users");
            }
        }

        public ActionResult deactive(string id)
        {

            using (var context = new ApplicationDbContext())
            {
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var checkUser = UserManager.FindById(id);
                checkUser.status = -1;
                UserManager.Update(checkUser);
                return getMessage(Models.Enums.MStatus.check, "", "Index", "Users");
            }
        }
        public ActionResult Active(string id)
        {

            using (var context = new ApplicationDbContext())
            {
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var checkUser = UserManager.FindById(id);
                checkUser.status = 1;
                UserManager.Update(checkUser);
                return getMessage(Models.Enums.MStatus.check, "", "Index", "Users");
            }
        }


        public ActionResult Delete(string id)
        {
            using (var db = new MhanaDbEntities())
            {
                var checkUser = db.AspNetUsers.Where(w => w.Id == id).FirstOrDefault();
                if (checkUser != null)
                {
                    db.AspNetUsers.Remove(checkUser);
                    db.SaveChanges();

                }

                return getMessage(Models.Enums.MStatus.check, "", "Index", "Users");
            }
        }

        public ActionResult Change_password()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Change_password(ResetPasswordModel model)
        {
            var u = Functions.GetUser();
            using (var context = new ApplicationDbContext())
            {
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var user = await UserManager.FindByNameAsync(u.email);
                if (user != null && UserManager.CheckPassword(user, model.CurrentPassword))
                {
                    var result = await UserManager.ChangePasswordAsync(user.Id, model.CurrentPassword, model.Password);
                    return getMessage(Models.Enums.MStatus.check, "", "Change_password", "Users");
                }
            }
            return getMessage(Models.Enums.MStatus.remove, "", "Change_password", "Users");
        }
    }
}