﻿using Mhana.Areas.Teacher.Controllers;
using Mhana.Helper;
using Mhana.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Mhana.Areas.Admin.Controllers
{
    public class TeachersController : BaseController
    {
        // GET: Admin/Teachers
        public ActionResult Index()
        {
            using (var db = new MhanaDbEntities())
            {
                var item = db.AspNetUsers.Where(w => w.AspNetRoles.FirstOrDefault() != null && w.AspNetRoles.FirstOrDefault().Id == "2" && w.is_complete == 1).Select(s => new TeacherListAdmin()
                {
                    absher = s.absher,
                    cdate = s.cdate,
                    email = s.Email,
                    fullname = s.first_name + "" + s.last_name,
                    id = s.Id,
                    mobile = s.PhoneNumber,
                    photo = s.photo,
                    status = s.status,

                }).OrderByDescending(o => o.cdate).ToList();
                return View(item);
            }
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Edit(string id)
        {
            string user_id = id;
            //TeacherRegisterResponseModel
            List<TeacherTimes> TT = new List<TeacherTimes>();
            List<string> el = new List<string>();
            using (var db = new MhanaDbEntities())
            {
                TT = db.Teacher_Times.Where(w => w.user_id == user_id).Select(s => new TeacherTimes() { day_number = s.day_number, from = s.from_time, to = s.to_time }).ToList();
                ViewBag.specializations = db.specializations.Select(s => new keyValue() { id = s.Id, value = s.name }).ToList();
                ViewBag.branch_specialization = db.branch_specialization.Select(s => new keyValue() { id = s.Id, value = s.name }).ToList();
                ViewBag.education_level = db.education_level_sub.Select(s => new keyValue() { id = s.id, value = s.name }).ToList();
                ViewBag.banks = db.Banks_Account.Where(w => w.user_id == id).OrderByDescending(o => o.Id).ToList();
                ViewBag.absherData = db.AbsherDatas.Where(w => w.mhana_user_id == id).FirstOrDefault();
                ViewBag.Courses = db.Courses.Where(w => w.teacher_id == id).Select(s => new CourseList()
                {
                    id = s.Id,
                    cdate = s.cdate + "",
                    status = s.status,
                    title = s.title,
                    std_count = s.total_std
                }).OrderByDescending(o => o.cdate).ToList();
                ViewBag.tTimes = TT;
                var user = db.AspNetUsers.Include(i => i.Teacher_Times).FirstOrDefault(f => f.Id == id);
                if (user != null)
                {
                    if (!string.IsNullOrEmpty(user.education_level_text))
                    {
                        var levels = user.education_level_text.Split(',');
                        var string_levels = db.education_level_sub.Where(w => levels.Contains(w.id + "")).Select(s => s.name).ToList();
                        if (string_levels.Count() > 0)
                            ViewBag.eduLevels = string.Join(",", string_levels);
                        else
                            ViewBag.eduLevels = "---";
                    }

                    //TeacherRegisterResponseModel response = new TeacherRegisterResponseModel()
                    //{
                    //    branch_specialization = user.branch_specialization,
                    //    certificate = user.certificate,
                    //    specialization = user.specialization,
                    //    mobile = user.PhoneNumber,
                    //    first_name = user.first_name,
                    //    last_name = user.last_name,
                    //    id = user.Id,
                    //    certificate_photo = user.certificate,
                    //    city = user.city,
                    //    region = user.region,
                    //    country = user.country,
                    //    details = user.details,
                    //    dob = string.Format("{0:MM/dd/yyyy}", user.dob),
                    //    email = user.Email,
                    //    expired = 14,
                    //    fcm = user.fcm,
                    //    fullname = user.fullname,
                    //    gender = user.gender,
                    //    photo = user.photo,
                    //    online_cost = string.IsNullOrEmpty(user.online_cost) ? 0 : Convert.ToDouble(user.online_cost),
                    //    site_cost = string.IsNullOrEmpty(user.site_cost) ? 0 : Convert.ToDouble(user.site_cost),
                    //    status = user.status,
                    //    teaching_mechanism = user.teaching_mechanism,
                    //    university = user.university,
                    //    teacher_times = TT,
                    //    education_level = !string.IsNullOrEmpty(user.education_level_text) ? user.education_level_text.Split(',').ToList() : el,
                    //    is_complete = user.is_complete == 1 ? true : false,
                    //    absher = user.absher

                    //};
                    return View(user);
                }

                return RedirectToAction("Index", "Dashboard", new { area = "Admin" });
            }
        }
        //[HttpPost]
        //public async Task<ActionResult> Edit(AdminTeacher model)
        //{           
        //    if (!string.IsNullOrEmpty(model.photo))
        //    {
        //        var ph = Functions.UploadB64File(model.photo, "~/resources/users");
        //        model.photo = ph;
        //    }
        //    if (!string.IsNullOrEmpty(model.certificate_photo))
        //    {

        //        var ph2 = Functions.UploadB64File(model.certificate_photo, "~/resources/files");
        //        model.certificate_photo = ph2;
        //    }
        //    using (var context = new ApplicationDbContext())
        //    {
        //        var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
        //        var user = UserManager.FindByName(model.email);
        //        if (user != null)
        //        {
        //            if (!string.IsNullOrEmpty(model.photo))
        //            {
        //                var ph = Functions.UploadB64File(model.photo, "~/resources/users");
        //                model.photo = ph;
        //            }
        //            user.fullname = string.IsNullOrEmpty(model.fullname) ? user.fullname : model.fullname;
        //            user.first_name = string.IsNullOrEmpty(model.first_name) ? user.first_name : model.first_name;
        //            user.last_name = string.IsNullOrEmpty(model.last_name) ? user.last_name : model.last_name;
        //            user.gender = string.IsNullOrEmpty(model.gender) ? user.gender : model.gender;
        //            user.PhoneNumber = string.IsNullOrEmpty(model.mobile) ? user.PhoneNumber : model.mobile;
        //            user.city = string.IsNullOrEmpty(model.city) ? user.city : model.city;
        //            user.region = string.IsNullOrEmpty(model.region) ? user.region : model.region;
        //            user.photo = string.IsNullOrEmpty(model.photo) ? user.photo : model.photo;
        //            user.photo = string.IsNullOrEmpty(model.photo) ? user.photo : model.photo;
        //            user.dob = !model.dob.HasValue ? user.dob : model.dob;
        //            user.education_level_text = model.education_level.Count() > 0 ? string.Join(",", model.education_level) : user.education_level_text;
        //            user.details = string.IsNullOrEmpty(model.details) ? user.details : model.details;
        //            user.university = string.IsNullOrEmpty(model.university) ? user.university : model.university;
        //            user.teaching_mechanism = string.IsNullOrEmpty(model.teaching_mechanism) ? user.teaching_mechanism : model.teaching_mechanism;
        //            user.specialization = !model.specialization.HasValue ? user.specialization : model.specialization;
        //            user.branch_specialization = !model.branch_specialization.HasValue ? user.branch_specialization : model.branch_specialization;
        //            user.is_complete = 1;
        //            var Updated = UserManager.Update(user);
        //            List<Teacher_Times> TT = new List<Teacher_Times>();
        //            List<string> el = new List<string>();
        //            //if (Updated.Succeeded)
        //            //{
        //            //    if (model.teacher_times.Count() > 0)
        //            //    {

        //            //        using (var db = new MhanaDbEntities())
        //            //        {
        //            //            var removed = db.Teacher_Times.Where(w => w.user_id == user_id).ToList();
        //            //            foreach (var item in removed)
        //            //            {
        //            //                db.Teacher_Times.Remove(item);
        //            //            }
        //            //            foreach (var item in model.teacher_times)
        //            //            {
        //            //                if (item.day_number.HasValue)
        //            //                {
        //            //                    var _ct = new Teacher_Times() { day_number = item.day_number, from_time = item.from, to_time = item.to, user_id = user_id };
        //            //                    db.Teacher_Times.Add(_ct);
        //            //                }
        //            //            }
        //            //            db.SaveChanges();
        //            //        }
        //            //    }
        //            //}
        //        }
        //    }
        //    return getMessage(Models.Enums.MStatus.check, "", "Index", "Teachers");
        //}
        public ActionResult Requests()
        {
            using (var db = new MhanaDbEntities())
            {
                var items = db.StdNewRequests.AsEnumerable().Select(s => new NewRequestList()
                {
                    cdate = s.cdate + "",
                    id = s.Id,
                    period = s.period,
                    start_date = s.start_date,
                    start_time = s.start_date.HasValue ? s.start_date.Value.TimeOfDay : DateTime.Now.TimeOfDay,
                    status = s.status,
                    student_name = s.AspNetUser != null ? s.AspNetUser.fullname : "---",
                    details = s.details,
                    teaching_mechanism = s.teaching_mechanism,
                    teacher_name = s.AspNetUser1 != null ? s.AspNetUser1.fullname : "---",
                    material = s.material
                }).OrderByDescending(o => o.id).ToList();
                return View(items);
            }
        }

        public ActionResult EditRequest(int? id)
        {
            using (var db = new MhanaDbEntities())
            {
                var c = db.StdNewRequests.Include(i => i.AspNetUser).Where(w => w.Id == id).FirstOrDefault();
                ViewBag.banks = db.Banks_Account.Where(w => w.user_id == c.teacher_id).OrderByDescending(o => o.Id).ToList();
                return View(c);
            }
        }
        [HttpPost]
        public ActionResult EditRequest(EditCourse model)
        {
            using (var db = new MhanaDbEntities())
            {

                var _request = db.StdNewRequests.Where(w => w.Id == model.id).FirstOrDefault();

                _request.status = model.status;
                //_request.re
                db.Entry(_request).State = EntityState.Modified;
                db.SaveChanges();
                return getMessage(Models.Enums.MStatus.check, "", "Requests", "Teachers");

            }
        }

        public ActionResult Rates()
        {
            using (var db = new MhanaDbEntities())
            {

                var items = db.MyRates.Where(w => w.rate_type == 2).Select(s => new RateModel()
                {
                    id = s.id,
                    cdate = DateTime.Now,
                    comment = s.comment,
                    course_title = s.Course.title,
                    rate = s.rate,
                    course_id = s.course_id,
                    std_id = s.std_id,
                    student_name = s.AspNetUser.fullname,
                    teacher_name = s.AspNetUser1.fullname,
                    photo = "/resources/users/" + s.AspNetUser.photo

                }).OrderByDescending(o => o.id).ToList();
                return View(items);
            }
        }
        public ActionResult Invites_Teacher(int? id)
        {
            using (var db = new MhanaDbEntities())
            {
                ViewBag.Courses = db.Courses.Where(w => w.status == 1).Select(s => new keyValue() { id = s.Id, value = s.title }).ToList();
                return View();
            }


        }
        [HttpPost]
        public async Task<ActionResult> Invites_Teacher(Invite model, int? type)
        {
            var user_id = Functions.GetUser().id;
            using (var db = new MhanaDbEntities())
            {
                model.cdate = DateTime.Now;
                model.send_by = user_id;
                model.status = 0;
                model.invite_type = 1; //0 for student without reg 1 for student with reg
                db.Invites.Add(model);
                db.SaveChanges();
                if (type != 1)
                {
                    var mail = new emailInviteT()
                    {
                        cdate = DateTime.Now + "",
                        from = User.Identity.Name,
                        to = model.email,
                        title = "دعوة الانضمام الى تطبيق درس خصوصي",
                        msg = "لقد تمت دعوتك للانضمام الى تطبيق درس خصوصي من قبل المعلم " + model.email + "يمكنك الآن الانضمام الى التطبيق وتسجيل بياناتك من خلال الروابط المرفقة الخاصة بتطبيق الأيفون والأندرويد"
                    };
                    new Mhana.Controllers.MailerController().InviteT(mail).Deliver();
                }
            }
            if (type == 1)
            {
                using (var context = new ApplicationDbContext())
                {
                    var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                    var checkUser = UserManager.FindByName(model.email);
                    if (checkUser != null)
                        return getMessage(Models.Enums.MStatus.check, "البريد الالكتروني مستخدم مسبقاً", "Create", "Users");
                    var user = new ApplicationUser
                    {
                        cdate = DateTime.Now,
                        Email = model.email,
                        fullname = model.std_name,
                        status = 0,
                        UserName = model.email,
                        is_complete = 0
                    };

                    string pass = System.Web.Security.Membership.GeneratePassword(8, 3);
                    var x = await UserManager.CreateAsync(user, pass);
                    if (model.invite_type == 1)
                        await UserManager.AddToRoleAsync(user.Id, "Teacher");
                    var mail = new emailInviteT()
                    {
                        cdate = DateTime.Now + "",
                        from = User.Identity.Name,
                        to = model.email,
                        title = "دعوة الانضمام الى تطبيق درس خصوصي",
                        msg = "لقد تمت دعوتك للانضمام الى تطبيق درس خصوصي من قبل المعلم " + model.email + "يمكنك الآن الانضمام الى التطبيق وتسجيل بياناتك من خلال الروابط المرفقة الخاصة بتطبيق الأيفون والأندرويد",
                        username = model.email,
                        password = pass
                    };
                    new Mhana.Controllers.MailerController().InviteT(mail).Deliver();

                }
            }

            return getMessage(Models.Enums.MStatus.check, "", "Index", "Invites");
        }

        public ActionResult deactive(string id)
        {
            using (var db = new MhanaDbEntities())
            {
                var checkUser = db.AspNetUsers.Where(w => w.Id == id).FirstOrDefault();
                if (checkUser != null)
                {
                    checkUser.status = -1;

                    db.Entry(checkUser).State = EntityState.Modified;
                    db.SaveChanges();
                }

                return getMessage(Models.Enums.MStatus.check, "", "Index", "Teachers");
            }
        }
        public ActionResult Active(string id)
        {
            using (var db = new MhanaDbEntities())
            {
                var checkUser = db.AspNetUsers.Where(w => w.Id == id).FirstOrDefault();
                if (checkUser != null)
                {
                    checkUser.status = 1;
                    checkUser.absher = 1;
                    db.Entry(checkUser).State = EntityState.Modified;
                    db.SaveChanges();
                    TeacherWiziQ tw = new TeacherWiziQ()
                    {
                        email = checkUser.Email,
                        name = checkUser.fullname,
                        about_the_teacher = checkUser.details,
                        password = "123456"
                    };
                    var x = WiziQ.WizIQClass.AddTeacher(tw);
                    if (!string.IsNullOrEmpty(x) && x.Substring(0, 20).Contains("ok"))
                    {
                        rsp resultObject = Functions.DeserializeXML<rsp>(x);
                        checkUser.wiziq_id = resultObject.add_teacher.teacher_id + "";
                        db.Entry(checkUser).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(Server.MapPath("~/debug.txt"), true))
                        file.WriteLine("/n" + x);

                }

                return getMessage(Models.Enums.MStatus.check, "", "Index", "Teachers");
            }

            //using (var context = new ApplicationDbContext())
            //{
            //    var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            //    var checkUser = UserManager.FindById(id);
            //    //if (checkUser != null)
            //    //    return getMessage(Enums.MStatus.danger, "البريد الالكتروني مستخدم مسبقاً", "Create", "Manager");
            //    checkUser.status = 1;
            //    checkUser.absher = 1;
            //    UserManager.Update(checkUser);
            //    TeacherWiziQ tw = new TeacherWiziQ()
            //    {
            //        email = checkUser.Email,
            //        name = checkUser.fullname,
            //        about_the_teacher = checkUser.details,
            //        password = "123456"
            //    };
            //    var x = WiziQ.WizIQClass.AddTeacher(tw);
            //    using (System.IO.StreamWriter file = new System.IO.StreamWriter(Server.MapPath("~/debug.txt"), true))
            //        file.WriteLine("/n" + x);

            //    return getMessage(Models.Enums.MStatus.check, "", "Index", "Teachers");
            //}
        }

        public ActionResult Delete(string id)
        {
            using (var db = new MhanaDbEntities())
            {
                var checkUser = db.AspNetUsers.Where(w => w.Id == id).FirstOrDefault();
                if (checkUser != null)
                {
                    var Courses = db.Courses.Where(w => w.teacher_id == id).ToList();
                    var Courses_ids = Courses.Select(s => s.Id).ToList();
                    var reqs = db.StdNewRequests.Where(w => w.teacher_id == id).ToList();
                    foreach (var item in reqs)
                    {
                        db.StdNewRequests.Remove(item);
                    }
                    var MyRates = db.MyRates.Where(w => w.teacher_id == id || Courses_ids.Contains(w.course_id.Value)).ToList();
                    foreach (var item in MyRates)
                    {
                        db.MyRates.Remove(item);
                    }                  

                    var Pockets = db.Pockets.Where(w => w.user_id == id).ToList();
                    foreach (var item in Pockets)
                    {
                        db.Pockets.Remove(item);
                    }
                   
                    var Invites = db.Invites.Where(w => w.send_by == id || Courses_ids.Contains(w.course_id.Value)).ToList();
                    foreach (var item in Invites)
                    {
                        db.Invites.Remove(item);
                    }
                    var Course_Times = db.Course_Times.Where(w => w.teacher_id == id).ToList();
                    foreach (var item in Course_Times)
                    {
                        db.Course_Times.Remove(item);
                    }
                    var std_course = db.Std_Course.Where(w => Courses_ids.Contains(w.course_id.Value)).ToList();
                    foreach (var item in std_course)
                    {
                        db.Std_Course.Remove(item);
                    }
                    foreach (var item in Courses)
                    {
                        db.Courses.Remove(item);
                    }

                    var t_times = db.Teacher_Times.Where(w => w.user_id == id).ToList();
                    foreach (var item in t_times)
                    {
                        db.Teacher_Times.Remove(item);
                    }

                    db.SaveChanges();
                    db.AspNetUsers.Remove(checkUser);
                    db.SaveChanges();

                }

                return getMessage(Models.Enums.MStatus.check, "", "Index", "Teachers");
            }
        }

    }
}