﻿using Mhana.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mhana.Areas.Admin.Controllers
{
    public class SettingsController : BaseController
    {
        // GET: Admin/Settings
        public ActionResult Index()
        {
            using (var db = new MhanaDbEntities())
            {
                var items = db.Settings.ToList();
                ViewBag.item = items;
                return View();
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Index(FormCollection form)
        {
            using (var db = new MhanaDbEntities())
            {
                foreach (var key in form.AllKeys)
                {

                    var check = db.Settings.Where(w => w.option_key.Equals(key)).FirstOrDefault();
                    if (check != null)
                    {
                        check.option_value = Request.Form[key];
                        db.Entry(check).State = EntityState.Modified;

                    }
                    else
                    {
                        var con = new Setting()
                        {
                            option_key = key,
                            option_value = Request.Form[key]
                        };
                        db.Settings.Add(con);
                    }

                }
                db.SaveChanges();
                return getMessage(Enums.Status.Visible, "", "Index", "Settings");
            }
        }
        public ActionResult Specialization(int? id)
        {
            using (var db = new MhanaDbEntities())
            {
                var items = db.specializations.OrderByDescending(o => o.Id).ToList();
                ViewBag.item = items.Where(w => w.Id == id).FirstOrDefault();
                return View(items);
            }
        }

        public ActionResult RemoveSpecialization(int? id)
        {
            using (var db = new MhanaDbEntities())
            {
                var items = db.specializations.FirstOrDefault(f => f.Id == id);
                if (items != null) {                   
                   var _b =  db.branch_specialization.Where(w => w.specialization_id == id).ToList();
                    foreach(var x in _b)
                        db.branch_specialization.Remove(x);
                    db.specializations.Remove(items);
                    db.SaveChanges();
                }
                return getMessage(Models.Enums.MStatus.check, "", "Specialization", "Settings");
            }
        }

        [HttpPost]
        public ActionResult Specialization(specialization model,int? edit_id)
        {
            using (var db = new MhanaDbEntities())
            {
                if (edit_id.HasValue)
                {
                    var item = db.specializations.Where(w => w.Id == model.Id).FirstOrDefault();
                    if (item != null)
                    {                        
                        item.name = model.name;
                        db.Entry(item).State = EntityState.Modified;
                        db.SaveChanges();
                        return getMessage(Models.Enums.MStatus.check, "", "Specialization", "Settings");
                    }
                }
                var co = new specialization()
                {
                    name = model.name,                   
                };
                db.specializations.Add(co);
                db.SaveChanges();
                return getMessage(Models.Enums.MStatus.check, "", "Specialization", "Settings");
            }
        }

        public ActionResult mail_edu_level(int? id)
        {
            using (var db = new MhanaDbEntities())
            {
                var items = db.education_level.OrderByDescending(o => o.Id).ToList();
                ViewBag.item = items.Where(w => w.Id == id).FirstOrDefault();
                return View(items);
            }
        }
        [HttpPost]
        public ActionResult mail_edu_level(education_level model, int? edit_id)
        {
            using (var db = new MhanaDbEntities())
            {
                if (edit_id.HasValue)
                {
                    var item = db.education_level.Where(w => w.Id == model.Id).FirstOrDefault();
                    if (item != null)
                    {
                        item.name = model.name;
                        db.Entry(item).State = EntityState.Modified;
                        db.SaveChanges();
                        return getMessage(Models.Enums.MStatus.check, "", "mail_edu_level", "Settings");
                    }
                }
                var co = new education_level()
                {
                    name = model.name,
                };
                db.education_level.Add(co);
                db.SaveChanges();
                return getMessage(Models.Enums.MStatus.check, "", "education_level", "Settings");
            }
        }


        public ActionResult Material(int? id)
        {
            using (var db = new MhanaDbEntities())
            {
                var items = db.specializations.OrderByDescending(o => o.Id).ToList();
                ViewBag.item = items.Where(w => w.Id == id).FirstOrDefault();
                return View(items);
            }
        }
        [HttpPost]
        public ActionResult Material(specialization model, int? edit_id)
        {
            using (var db = new MhanaDbEntities())
            {
                if (edit_id.HasValue)
                {
                    var item = db.specializations.Where(w => w.Id == model.Id).FirstOrDefault();
                    if (item != null)
                    {
                        item.name = model.name;
                        db.Entry(item).State = EntityState.Modified;
                        db.SaveChanges();
                        return getMessage(Models.Enums.MStatus.check, "", "Specialization", "Settings");
                    }
                }
                var co = new specialization()
                {
                    name = model.name,
                };
                db.specializations.Add(co);
                db.SaveChanges();
                return getMessage(Models.Enums.MStatus.check, "", "Specialization", "Settings");
            }
        }


        public ActionResult branch(int? id)
        {
            using (var db = new MhanaDbEntities())
            {
                var items = db.branch_specialization.Include(x=>x.specialization).OrderByDescending(o => o.Id).ToList();
                ViewBag.item = items.Where(w => w.Id == id).FirstOrDefault();
                ViewBag.list = db.specializations.Select(s => new keyValue() { id = s.Id, value = s.name }).ToList();
                return View(items);
            }
        }

        public ActionResult RemoveBranch(int? id)
        {
            using (var db = new MhanaDbEntities())
            {
                var items = db.branch_specialization.FirstOrDefault(f => f.Id == id);
                if (items != null)
                {
                    db.branch_specialization.Remove(items);
                    db.SaveChanges();
                }
                return getMessage(Models.Enums.MStatus.check, "", "branch", "Settings");
            }
        }
        [HttpPost]
        public ActionResult branch(branch_specialization model, int? edit_id)
        {
            using (var db = new MhanaDbEntities())
            {
                if (edit_id.HasValue)
                {
                    var item = db.branch_specialization.Where(w => w.Id == model.Id).FirstOrDefault();
                    if (item != null)
                    {
                        item.name = model.name;
                        db.Entry(item).State = EntityState.Modified;
                        db.SaveChanges();
                        return getMessage(Models.Enums.MStatus.check, "", "branch", "Settings");
                    }
                }
                var co = new branch_specialization()
                {
                    name = model.name,
                    specialization_id = model.specialization_id
                };
                db.branch_specialization.Add(co);
                db.SaveChanges();
                return getMessage(Models.Enums.MStatus.check, "", "branch", "Settings");
            }
        }


        public ActionResult edu_sub_level(int? id)
        {
            using (var db = new MhanaDbEntities())
            {
                var items = db.education_level_sub.Include(x => x.education_level).OrderByDescending(o => o.id).ToList();
                ViewBag.item = items.Where(w => w.id == id).FirstOrDefault();
                ViewBag.list = db.specializations.Select(s => new keyValue() { id = s.Id, value = s.name }).ToList();
                return View(items);
            }
        }
        [HttpPost]
        public ActionResult edu_sub_level(education_level_sub model, int? edit_id)
        {
            using (var db = new MhanaDbEntities())
            {
                if (edit_id.HasValue)
                {
                    var item = db.education_level_sub.Where(w => w.id == model.id).FirstOrDefault();
                    if (item != null)
                    {
                        item.name = model.name;
                        db.Entry(item).State = EntityState.Modified;
                        db.SaveChanges();
                        return getMessage(Models.Enums.MStatus.check, "", "edu_sub_level", "Settings");
                    }
                }
                var co = new education_level_sub()
                {
                    name = model.name,                    
                     main_level = model.main_level
                      
                };
                db.education_level_sub.Add(co);
                db.SaveChanges();
                return getMessage(Models.Enums.MStatus.check, "", "edu_sub_level", "Settings");
            }
        }


        public ActionResult levels(int? id)
        {
            using (var db = new MhanaDbEntities())
            {
                var items = db.education_level.OrderByDescending(o => o.Id).ToList();
                ViewBag.item = items.Where(w => w.Id == id).FirstOrDefault();
                return View(items);
            }
        }
        [HttpPost]
        public ActionResult levels(education_level model, int? edit_id)
        {
            using (var db = new MhanaDbEntities())
            {
                if (edit_id.HasValue)
                {
                    var item = db.education_level.Where(w => w.Id == model.Id).FirstOrDefault();
                    if (item != null)
                    {
                        item.name = model.name;
                        db.Entry(item).State = EntityState.Modified;
                        db.SaveChanges();
                        return getMessage(Models.Enums.MStatus.check, "", "levels", "Settings");
                    }
                }
                var co = new education_level()
                {
                    name = model.name                     
                };
                db.education_level.Add(co);
                db.SaveChanges();
                return getMessage(Models.Enums.MStatus.check, "", "levels", "Settings");
            }
        }
    }
}