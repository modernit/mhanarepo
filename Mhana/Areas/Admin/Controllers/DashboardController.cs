﻿using Mhana.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mhana.Areas.Admin.Controllers
{
    public class DashboardController : BaseController
    {
        // GET: Admin/Dashboard
        public ActionResult Index()
        {
            using (var db = new MhanaDbEntities())
            {
                ViewBag.courses_all = db.Courses.Count();
                ViewBag.courses = db.Courses.Where(w => w.status != -1).Count();
                ViewBag.std = db.AspNetUsers.Where(w => w.AspNetRoles.FirstOrDefault() != null && w.AspNetRoles.FirstOrDefault().Id == "3").Count();
                ViewBag.teacher = db.AspNetUsers.Where(w => w.AspNetRoles.FirstOrDefault() != null && w.AspNetRoles.FirstOrDefault().Id == "2").Count();
                ViewBag.std_active = db.AspNetUsers.Where(w => w.AspNetRoles.FirstOrDefault() != null && w.AspNetRoles.FirstOrDefault().Id == "3" && w.status == 1).Count();
                ViewBag.teacher_active = db.AspNetUsers.Where(w => w.AspNetRoles.FirstOrDefault() != null && w.AspNetRoles.FirstOrDefault().Id == "2" && w.status == 1).Count();
                ViewBag.requests = db.StdNewRequests.Where(w => w.status != -1).Count();
                ViewBag.NewT = db.AspNetUsers.Where(w => w.status == 0 && w.is_complete == 1).Select(s => new TeacherList()
                {
                    id = s.Id,
                    fullname = s.fullname,
                    photo = "/resources/users/" + (string.IsNullOrEmpty(s.photo) ? "default.png" : ""),
                    specialization = s.specialization1 != null ? s.specialization1.name : "---",
                    branch_specialization = s.branch_specialization1 != null ? s.branch_specialization1.name : "--"
                }).OrderByDescending(o => o.id).ToList();
                ViewBag.RequestsN = db.StdNewRequests.Where(w => w.status == 0).Select(s => new NewRequestList()
                {
                    id = s.Id,
                    teacher_name = s.AspNetUser1.fullname,
                    student_name = s.AspNetUser.fullname,
                    material = s.material,
                    teaching_mechanism = s.teaching_mechanism
                }).OrderByDescending(o => o.id).ToList();
                return View();
            }
        }
    }
}