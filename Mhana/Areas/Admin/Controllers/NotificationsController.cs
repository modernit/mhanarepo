﻿using Mhana.Helper;
using Mhana.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mhana.Areas.Admin.Controllers
{
    public class NotificationsController : BaseController
    {
        // GET: Admin/Notifications
        public ActionResult Index()
        {

            using (var db = new MhanaDbEntities())
            {
                var noti = db.Notifications.Select(s => new NotificationModel()
                {
                    id = s.id,
                    cdate = s.cdate,
                    title = s.title,
                    details = s.details,
                    is_read = s.is_read,
                    type = s.type,
                    photo = "",
                    rel_id = s.rel_id
                }).ToList();
                return View(noti);
            }
        }

        public ActionResult Create()
        {            
                return View();
        }
        [HttpPost]
        public ActionResult Create(Notification model)
        {
            using (var db = new MhanaDbEntities())
            {
                List<string> x = new List<string>();
                Functions.SendNotification(x, model.details, "topic", null);
                return getMessage(Models.Enums.MStatus.check, "", "Index", "Notifications");
            }
        }
          
    }
}