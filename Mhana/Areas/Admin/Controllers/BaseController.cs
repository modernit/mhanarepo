﻿using Mhana.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mhana.Areas.Admin.Controllers
{
     [AuthFilter(Roles = "1")]
    public class BaseController : Controller
    {
        // GET: Admin/Base
        public ActionResult getMessage(Enum status, string meesage, string action, string controller, int? id = null)
        {
            TempData["Message"] = string.IsNullOrEmpty(meesage) ? "تمت العملية بنجاح" : meesage;
            TempData["Status"] = status;
            return RedirectToAction(action, controller, new { id = id });
        }
        public ActionResult getMessage(Enum status, string meesage, string action, string controller, string id)
        {
            TempData["Message"] = string.IsNullOrEmpty(meesage) ? "تمت العملية بنجاح" : meesage;
            TempData["Status"] = status;
            return RedirectToAction(action, controller, new { id = id });
        }
        public ActionResult getMessage(Enum status, string meesage, string action, string controller)
        {
            TempData["Message"] = string.IsNullOrEmpty(meesage) ? "تمت العملية بنجاح" : meesage;
            TempData["Status"] = status;
            return RedirectToAction(action, controller);
        }
        public ActionResult getMessage(Enum status, string meesage, string returnUrl)
        {
            TempData["Message"] = string.IsNullOrEmpty(meesage) ? "تمت العملية بنجاح" : meesage;
            TempData["Status"] = status;
            return Redirect(returnUrl);
        }
    }
}