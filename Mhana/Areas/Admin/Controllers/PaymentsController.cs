﻿using Mhana.Helper;
using Mhana.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mhana.Areas.Admin.Controllers
{
    public class PaymentsController : BaseController
    {
        // GET: Admin/Payments
        public ActionResult Index()
        {

         ViewBag.rate =    Functions.getOption("site_rate");
            using (var db = new MhanaDbEntities())
            {
                var items = db.Courses.AsEnumerable().Select(s => new CourseListAdmin()
                {
                    cdate = s.cdate + "",
                    id = s.Id,
                    lectures_count = s.lectures_count,
                    period = s.period,
                    photo = s.photo,
                    start_date = s.start_date,
                    start_time = s.start_date.HasValue ? s.start_date.Value.TimeOfDay : DateTime.Now.TimeOfDay,
                    status = s.status,
                    title = s.title,
                    teacher_name = s.AspNetUser.fullname,
                    total_std = s.total_std,
                    cost = s.cost
                }).OrderByDescending(o => o.id).ToList();
                return View(items);
            }
        }
    }
}