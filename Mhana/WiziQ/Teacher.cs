﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Configuration;
using System.Collections.Specialized;
using System.Net;
using System.IO;
using Mhana.Models;

/// <summary>
/// Summary description for Teacher
/// </summary>
namespace Mhana.WiziQ
{
    public partial class WizIQClass
    {
        public static string AddTeacher(TeacherWiziQ model)
        {
            string ServiceRootUrl = ConfigurationManager.AppSettings["ServiceRootUrl"];
            //private secret Key
            var secretAcessKey = ConfigurationManager.AppSettings["SeceretKey"];
            // Build the Query Parameters to Generate Signature           
            NameValueCollection requestParameters = new NameValueCollection();
            requestParameters["access_key"] = ConfigurationManager.AppSettings["AccessKey"];

            requestParameters["timestamp"] = AuthBase.GenerateTimeStamp();
            requestParameters.Add("method", "add_teacher");
            // Step 1 Generate Signature using secretAcessKey
            AuthBase authBase = new AuthBase();          
            string signature = authBase.GenerateSignature(requestParameters["access_key"], secretAcessKey, requestParameters["timestamp"], requestParameters["method"]);
            
            // Step 2 Add additional Query Params for the WiZiQ REST Service
            // Construct requestParameters and call WiZiQ endpoint to add a teacher
            // Required parameters                     
            requestParameters["signature"] = signature;
            requestParameters.Add("name", model.name);
            //This is the unique email of the teacher that will identify the teacher in your WizIQ account.
            //For more information visit at: (http://developer.wiziq.com/faqs)
            requestParameters.Add("email", model.email);
            requestParameters.Add("password", model.password);
            //Optional parameters
            requestParameters.Add("phone_number", model.phone_number);
            requestParameters.Add("mobile_number", model.mobile_number);
            requestParameters.Add("time_zone", "Asia/Riyadh");
            requestParameters.Add("about_the_teacher", model.about_the_teacher);
            //0-Cannot Schedule(deafult)
            //1-Can Schedule
            requestParameters.Add("can_schedule_class", "1");
            //0-Inactive
            //1-Active(default)
            requestParameters.Add("is_active", "1");
            //image_url can have following extensions
            //{'.gif','.jpeg','.jpg','.png'}
            //if no image file is there pass this as empty string.
            string postFilePath ="";
             HttpRequest oRequest = new  HttpRequest();
            return oRequest.WiZiQWebRequest(ServiceRootUrl + "method=add_teacher", requestParameters, postFilePath);
        }
        public static string EditTeacher()
        {
            string ServiceRootUrl = ConfigurationManager.AppSettings["ServiceRootUrl"];
            //private secret Key
            var secretAcessKey = ConfigurationManager.AppSettings["SeceretKey"];
            // Build the Query Parameters to Generate Signature           
            NameValueCollection requestParameters = new NameValueCollection();
            requestParameters["access_key"] = ConfigurationManager.AppSettings["AccessKey"];
            requestParameters["timestamp"] = AuthBase.GenerateTimeStamp();
            requestParameters.Add("method", "edit_teacher");
            // Step 1 Generate Signature using secretAcessKey
            AuthBase authBase = new AuthBase();          
            string signature = authBase.GenerateSignature(requestParameters["access_key"], secretAcessKey, requestParameters["timestamp"], requestParameters["method"]);
           
            // Step 2 Add additional Query Params for the WiZiQ REST Service
            // Construct requestParameters and call WiZiQ endpoint to edit a teacher
            // Required parameters                     
            requestParameters["signature"] = signature;
            requestParameters.Add("name", "teacher name");
            //This is the unique email of the teacher that will identify the teacher in your WizIQ account.
            //For more information visit at: (http://developer.wiziq.com/faqs)
            requestParameters.Add("email", "teacher@domain.com");
            requestParameters.Add("password", "xxxxxxx");
            requestParameters.Add("teacher_id", "123");
            requestParameters.Add("phone_number", "+1 123 456 7890");
            requestParameters.Add("mobile_number", "+1 123 456 7890");
            requestParameters.Add("time_zone", "America/Goose_Bay");
            requestParameters.Add("about_the_teacher", "Masters in physics and 6 years of experience in teaching.");           
            //0-Cannot Schedule Class(deafult)
            //1-Can Schedule Class
            requestParameters.Add("can_schedule_class", "0");
            //0-Inactive Teacher
            //1-Active Teacher(default)
            requestParameters.Add("is_active", "1");
            //image can have following extensions
            //{'.gif','.jpeg','.jpg','.png'}
            //if no image file is there pass this as empty string.
            string postFilePath = "x://folder/img.jpeg";
           HttpRequest oRequest = new HttpRequest();
            return oRequest.WiZiQWebRequest(ServiceRootUrl + "method=edit_teacher", requestParameters, postFilePath);
        }

        public static string GetTeacherDetails()
        {
            string ServiceRootUrl = ConfigurationManager.AppSettings["ServiceRootUrl"];
            //private secret Key
            var secretAcessKey = ConfigurationManager.AppSettings["SeceretKey"];
            // Build the Query Parameters to Generate Signature
            var requestParameters = new Dictionary<string, string>();
            requestParameters["access_key"] = ConfigurationManager.AppSettings["AccessKey"];
            requestParameters["timestamp"] = AuthBase.GenerateTimeStamp();
            requestParameters["method"] = "get_teacher_details";

            // Step 1 Generate Signature using secretAcessKey
            AuthBase authBase = new AuthBase();
            var signature = authBase.GenerateSignature(secretAcessKey, requestParameters);
            // Step 2 Add additional Query Params for the WiZiQ REST Service
            // Construct requestParameters and call WiZiQ endpoint to get teacher details
            // Required parameters
            requestParameters["signature"] = signature;
            //Optional Parameter
           // requestParameters["teacher_id"] = "123456";
            HttpRequest oRequest = new HttpRequest();
            //Once you call method response xml will shows related teacher details.             
            return oRequest.WiZiQWebRequest(ServiceRootUrl + "method=get_teacher_details", requestParameters);
        }
    }
}