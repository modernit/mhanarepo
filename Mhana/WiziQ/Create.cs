﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Configuration;
using Mhana.Models;

namespace Mhana.WiziQ
{
    /// <summary>
    /// Summary description for create
    /// </summary>
    public partial class WizIQClass
    {
        public static string Create(CourseWiziQ model)
        {
            string ServiceRootUrl = ConfigurationManager.AppSettings["ServiceRootUrl"];
            //private secret Key
            var secretAcessKey = ConfigurationManager.AppSettings["SeceretKey"];
            // Build the Query Parameters to Generate Signature
            var requestParameters = new Dictionary<string, string>();
            requestParameters["access_key"] = ConfigurationManager.AppSettings["AccessKey"];

            requestParameters["timestamp"] = AuthBase.GenerateTimeStamp();
            requestParameters["method"] = "create";
          
            // Step 1 Generate Signature using secretAcessKey
            AuthBase authBase = new AuthBase();
            var signature = authBase.GenerateSignature(secretAcessKey, requestParameters);

            // Step 2 Add additional Query Params for the WiZiQ REST Service
            // Construct requestParameters and call WiZiQ endpoint to create a class
            // Required parameters
            requestParameters["signature"] = signature;
            requestParameters["start_time"] = model.start_time;// "10/23/2019 11:00";// "07/20/2012 20:12";

            //for teacher account pass parameter 'presenter_email'

            //This is the unique email of the presenter that will identify the presenter in WizIQ. Make sure to add 
            //this presenter email to your organization’s teacher account.
            //For more information visit at: (http://developer.wiziq.com/faqs)
            // requestParameters["presenter_email"] = model.presenter_email;// ConfigurationManager.AppSettings["PresenterEmail"];

            //presenter_id and presenter_name used for room based account
            requestParameters["presenter_id"] = model.presenter_id;// "123";
            requestParameters["presenter_name"] = model.presenter_name;// "veenu george";
            
            requestParameters["title"] = model.title;

            // Optional parameters
            requestParameters["time_zone"] = "Asia/Riyadh";
            requestParameters["attendee_limit"] = model.attendee_limit;// "10";
            requestParameters["duration"] = model.duration;// "30";//in mins        
            requestParameters["presenter_default_controls"] = model.presenter_default_controls;// "audio, video";
            requestParameters["attendee_default_controls"] = "audio";
            requestParameters["create_recording"] = "false";
            requestParameters["return_url"] = "";
            requestParameters["status_ping_url"] = "";
            requestParameters["language_culture_name"] = model.language_culture_name;// "en-us";

            HttpRequest oRequest = new HttpRequest();
            return oRequest.WiZiQWebRequest(ServiceRootUrl + "method=create", requestParameters);

        }

        public static string CreatePerma()
        {
            string ServiceRootUrl = ConfigurationManager.AppSettings["ServiceRootUrl"];
            //private secret Key
            var secretAcessKey = ConfigurationManager.AppSettings["SeceretKey"];
            // Build the Query Parameters to Generate Signature
            var requestParameters = new Dictionary<string, string>();
            requestParameters["access_key"] = ConfigurationManager.AppSettings["AccessKey"];

            requestParameters["timestamp"] = AuthBase.GenerateTimeStamp();
            requestParameters["method"] = "create_perma_class";

            // Step 1 Generate Signature using secretAcessKey
            AuthBase authBase = new AuthBase();
            var signature = authBase.GenerateSignature(secretAcessKey, requestParameters);

            // Step 2 Add additional Query Params for the WiZiQ REST Service
            // Construct requestParameters and call WiZiQ endpoint to create a class
            // Required parameters
            requestParameters["signature"] = signature;

            //for teacher account pass parameter 'presenter_email'

            //This is the unique email of the presenter that will identify the presenter in WizIQ. Make sure to add 
            //this presenter email to your organization’s teacher account.
            //For more information visit at: (http://developer.wiziq.com/faqs)
            requestParameters["presenter_email"] = ConfigurationManager.AppSettings["PresenterEmail"];

            //presenter_id and presenter_name used for room based account
            //requestParameters["presenter_id"] = "123";
            //requestParameters["presenter_name"] = "veenu george";

            requestParameters["title"] = "Mathematics";

            // Optional parameters
            requestParameters["attendee_limit"] = "10";      
            requestParameters["presenter_default_controls"] = "audio, video";
            requestParameters["attendee_default_controls"] = "audio";
            requestParameters["create_recording"] = "true";
            requestParameters["return_url"] = "";
            requestParameters["status_ping_url"] = "";
            requestParameters["language_culture_name"] = "en-us";

            HttpRequest oRequest = new HttpRequest();
            return oRequest.WiZiQWebRequest(ServiceRootUrl + "method=create_perma_class", requestParameters);

        }
    }
}
