﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Configuration;
/// <summary>
/// Summary description for GetData
/// </summary>
namespace Mhana.WiziQ
{
    public partial class WizIQClass
    {
        public static string GetData()
        {
            string ServiceRootUrl = ConfigurationManager.AppSettings["ServiceRootUrl"];
            //private secret Key
            var secretAcessKey = ConfigurationManager.AppSettings["SeceretKey"];
            // Build the Query Parameters to Generate Signature
            var requestParameters = new Dictionary<string, string>();
            requestParameters["access_key"] = ConfigurationManager.AppSettings["AccessKey"];

            requestParameters["timestamp"] = AuthBase.GenerateTimeStamp();
            requestParameters["method"] = "get_data";

            // Step 1 Generate Signature using secretAcessKey
            AuthBase authBase = new AuthBase();
            var signature = authBase.GenerateSignature(secretAcessKey, requestParameters);

            // Step 2 Add additional Query Params for the WiZiQ REST Service
            // Construct requestParameters and call WiZiQ endpoint to grt class/classes information
            // Required parameters
            requestParameters["signature"] = signature;
            //requestParameters["class_id"] = "24393";          

            requestParameters["multiple_class_id"] = "24392,24393";
           
            HttpRequest oRequest = new HttpRequest();
            //Once you call method response xml will shows related class/classes information.             
            return oRequest.WiZiQWebRequest(ServiceRootUrl + "method=get_data", requestParameters);
            
        }
    }
}