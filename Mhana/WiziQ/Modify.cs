﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Configuration;

namespace Mhana.WiziQ
{

    /// <summary>
    /// Summary description for Modify
    /// </summary>
    public partial class WizIQClass
    {
        public static string Modify()
        {
            string ServiceRootUrl = ConfigurationManager.AppSettings["ServiceRootUrl"];
            //private secret Key
            var secretAcessKey = ConfigurationManager.AppSettings["SeceretKey"];
            // Build the Query Parameters to Generate Signature
            var requestParameters = new Dictionary<string, string>();
            requestParameters["access_key"] = ConfigurationManager.AppSettings["AccessKey"];

            requestParameters["timestamp"] = AuthBase.GenerateTimeStamp();
            requestParameters["method"] = "modify";

            // Step 1 Generate Signature using secretAcessKey
            AuthBase authBase = new AuthBase();
            var signature = authBase.GenerateSignature(secretAcessKey, requestParameters);

            // Step 2 Add additional Query Params for the WiZiQ REST Service
            // Construct requestParameters and call WiZiQ endpoint to modify a class
            // Required parameters
            requestParameters["signature"] = signature;

            //Required for Time-based class
            requestParameters["class_id"] = "13816";
            //Required for permanent class
            //requestParameters["class_master_id"] = "29783";
            //requestParameters["perma_class"] = "true";

            // Optional parameters
            requestParameters["start_time"] = "9/21/2011 07:12";
            //presenter_id and presenter_name used for room based account
            //requestParameters["presenter_id"] = "123";
            //requestParameters["presenter_name"] = "veenu george";
            requestParameters["presenter_email"] = ConfigurationManager.AppSettings["PresenterEmail"];
            requestParameters["title"] = "Management";
            requestParameters["time_zone"] = "Asia/Kolkata";
            requestParameters["attendee_limit"] = "10";
            requestParameters["duration"] = "30";//in mins        
            requestParameters["presenter_default_controls"] = "audio, video";
            requestParameters["attendee_default_controls"] = "audio";
            requestParameters["create_recording"] = "true";
            requestParameters["return_url"] = "";
            requestParameters["status_ping_url"] = "";
            requestParameters["language_culture_name"] = "en-us";

            HttpRequest oRequest = new HttpRequest();
            return oRequest.WiZiQWebRequest(ServiceRootUrl + "method=modify", requestParameters);

        }
    }
}
