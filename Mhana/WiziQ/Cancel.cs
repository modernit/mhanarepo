﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace Mhana.WiziQ
{
    /// <summary>
    /// Summary description for Cancel
    /// </summary>
    public partial class WizIQClass
    {
        public static string Cancel(string class_id)
        {
            string ServiceRootUrl = ConfigurationSettings.AppSettings["ServiceRootUrl"];
            //private secret Key
            var secretAcessKey = ConfigurationManager.AppSettings["SeceretKey"];
            // Build the Query Parameters to Generate Signature
            var requestParameters = new Dictionary<string, string>();
            requestParameters["access_key"] = ConfigurationManager.AppSettings["AccessKey"];

            requestParameters["timestamp"] = AuthBase.GenerateTimeStamp();
            requestParameters["method"] = "cancel";

            // Step 1 Generate Signature using secretAcessKey
            AuthBase authBase = new AuthBase();
            var signature = authBase.GenerateSignature(secretAcessKey, requestParameters);

            // Step 2 Add additional Query Params for the WiZiQ REST Service
            // Construct requestParameters and call WiZiQ endpoint to cancel a class
            // Required parameters
            requestParameters["signature"] = signature;

            //Required for Time-based class
            requestParameters["class_id"] = class_id;// "13816";
            //Required for permanent class
            //requestParameters["class_master_id"] = "29783";
            //requestParameters["perma_class"] = "true";

            HttpRequest oRequest = new HttpRequest();
            return oRequest.WiZiQWebRequest(ServiceRootUrl + "method=cancel", requestParameters);

        }
    }
}
