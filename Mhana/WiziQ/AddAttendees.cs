﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Configuration;

namespace Mhana.WiziQ
{
    /// <summary>
    /// Summary description for AddAttendees
    /// </summary>
    public partial class WizIQClass
    {
        public static string AddAttendees(string course_id,string model)
        {
            string ServiceRootUrl = ConfigurationManager.AppSettings["ServiceRootUrl"];
            //private secret Key
            var secretAcessKey = ConfigurationManager.AppSettings["SeceretKey"];
            // Build the Query Parameters to Generate Signature
            var requestParameters = new Dictionary<string, string>();
            requestParameters["access_key"] = ConfigurationManager.AppSettings["AccessKey"];

            requestParameters["timestamp"] = AuthBase.GenerateTimeStamp();
            requestParameters["method"] = "add_attendees";

            // Step 1 Generate Signature using secretAcessKey
            AuthBase authBase = new AuthBase();
            var signature = authBase.GenerateSignature(secretAcessKey, requestParameters);

            // Step 2 Add additional Query Params for the WiZiQ REST Service
            // Construct requestParameters and call WiZiQ endpoint to add attendees to a class
            // Required parameters
            requestParameters["signature"] = signature;

            //Required for Time-based class
            requestParameters["class_id"] = course_id;// "13816";
            //Required for permanent class
            //requestParameters["class_master_id"] = "29783";
            //requestParameters["perma_class"] = "true";
            requestParameters["attendee_list"] ="<attendee_list>"+model+"</attendee_list>" ;//"<attendee_list><attendee><attendee_id><![CDATA[101]]></attendee_id><screen_name><![CDATA[Attendee1]]></screen_name><language_culture_name><![CDATA[en-us]]></language_culture_name></attendee><attendee><attendee_id><![CDATA[102]]></attendee_id><screen_name><![CDATA[Attendee2]]></screen_name><language_culture_name><![CDATA[en-us]]></language_culture_name></attendee></attendee_list>";

            HttpRequest oRequest = new HttpRequest();
            return oRequest.WiZiQWebRequest(ServiceRootUrl + "method=add_attendees", requestParameters);

        }
    }
}
