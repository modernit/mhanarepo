﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Configuration;

namespace Mhana.WiziQ
{
    /// <summary>
    /// Summary description for DownloadRecording
    /// </summary>
    public partial class WizIQClass
    {
        public static string DownloadRecording()
        {
            string ServiceRootUrl = ConfigurationManager.AppSettings["ServiceRootUrl"];
            //private secret Key
            var secretAcessKey = ConfigurationManager.AppSettings["SeceretKey"];
            // Build the Query Parameters to Generate Signature
            var requestParameters = new Dictionary<string, string>();
            requestParameters["access_key"] = ConfigurationManager.AppSettings["AccessKey"];

            requestParameters["timestamp"] = AuthBase.GenerateTimeStamp();
            requestParameters["method"] = "download_recording";

            // Step 1 Generate Signature using secretAcessKey
            AuthBase authBase = new AuthBase();
            var signature = authBase.GenerateSignature(secretAcessKey, requestParameters);

            // Step 2 Add additional Query Params for the WiZiQ REST Service
            // Construct requestParameters and call WiZiQ endpoint to cancel a class
            // Required parameters
            requestParameters["signature"] = signature;
            requestParameters["class_id"] = "16854";
			
            //recording_format value can be zip, exe or mp4
			//mp4 recording is available only for classes conducted on WizIQ desktop app
            requestParameters["recording_format"] = "zip";

            Mhana.WiziQ.HttpRequest oRequest = new Mhana.WiziQ.HttpRequest();
            //Once you call method, it creates recording package. <message> node in response xml will shows related messages. 
            //< status_xml_path> will contain http xml path which will have all the information of recording package.
            return oRequest.WiZiQWebRequest(ServiceRootUrl + "method=download_recording", requestParameters);
            
            

        }
    }
}