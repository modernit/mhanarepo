﻿using Mhana.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
namespace Mhana.ActionFilters
{
    public class AuthFilter : ActionFilterAttribute
    {

        public string Roles { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContextBase httpContext = filterContext.RequestContext.HttpContext;
            ApplicationUser model = (ApplicationUser)httpContext.Session["User"];

            if (model == null)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { area = "", controller = "Home", action = "Login" }));
                return;
            }
            if (Roles != null && !Roles.Contains(model.Roles.FirstOrDefault().RoleId))
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { area = "", controller = "Home", action = "Login" }));
                return;
            }
            if (Roles != null && Roles.Contains(model.Roles.FirstOrDefault().RoleId) && Roles.Contains("2"))
            {
                var rd = httpContext.Request.RequestContext.RouteData;
                string link =  rd.GetRequiredString("controller");
                if (model.is_complete != 1 && !string.IsNullOrEmpty(link) && link.ToLower()!="account")
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { area = "Teacher", controller = "Account", action = "Index" }));
                    return;
                }
            }
            //using (var db = new MolenEntities())
            //{
            //    var rd = httpContext.Request.RequestContext.RouteData;
            //    string link = rd.GetRequiredString("controller") + "/" + rd.GetRequiredString("action");
            //    string note = "";
            //    switch (link.ToLower())
            //    {
            //        case "advertiser/index":
            //            note = "فتح صفحة المعنين ";
            //            break;
            //        case "advertiser/create":
            //            note = "انشاء معلن جديد ";
            //            break;
            //        case "advertiser/edit":
            //            note = "تفاصيل بيانات معلن ";
            //            break;
            //        case "advertiser/deactive":
            //            note = "ايقاف معلن ";
            //            break;
            //        case "advertiser/active":
            //            note = "تفعيل معلن موقوف ";
            //            break;
            //        case "advs/index":
            //            note = "فتح صفحة الاعلانات ";
            //            break;
            //        case "advs/create":
            //            note = "انشاء اعلان جديد ";
            //            break;
            //        case "advs/edit":
            //            note = "تعديل بيانات اعلان ";
            //            break;
            //        case "advs/getadvertiser":
            //            note = "البحث عن محلن ";
            //            break;

            //        case "home/index":
            //            note = "الصفحة الرئيسية ";
            //            break;
            //        case "home/resetpassword":
            //            note = "تغيير كلمة المرور ";
            //            break;                 
            //        case "manager/index":
            //            note = "عرض المستخدمين ";
            //            break;
            //        case "manager/create":
            //            note = "انشاء مستخدم جديد ";
            //            break;
            //        case "manager/edit":
            //            note = "تعديل بيانات مستخدم ";
            //            break;
            //        case "manager/deactive":
            //            note = "ايقاف مستخدم ";
            //            break;
            //        case "manager/active":
            //            note = "تفعيل مستخدم ";
            //            break;

            //        case "notifications/index":
            //            note = "عرض الاشعارات ";
            //            break;
            //        case "notifications/create":
            //            note = "ارسال اشعار لمستقبلي اعلانات ";
            //            break;
            //        case "notifications/sendmolen":
            //            note = "ارسال شعار لمعلن ";
            //            break;
            //        case "notifications/sendsms":
            //            note = "ارسال sms ";
            //            break;

            //        case "payments/withdraw":
            //            note = "عرض طلبات السحب ";
            //            break;

            //        case "payments/charging":
            //            note = "عرض طلبات الشحن ";
            //            break;
            //        case "payments/accept":
            //            note = "قبل طلب دفع جديد ";
            //            break;

            //        case "payments/reject":
            //            note = "رفض طلب دفع ";
            //            break;

            //        case "recipient/index":
            //            note = "عرض مستقبلي الاعلانات ";
            //            break;
            //        case "recipient/create":
            //            note = "انشاء مستقبل اعلانات ";
            //            break;
            //        case "recipient/edit":
            //            note = "تعديل مستقبل اعلانات ";
            //            break;
            //        case "recipient/deactive":
            //            note = "ايقاف مستقبل اعلانات ";
            //            break;
            //        case "recipient/active":
            //            note = "تفعيل مستقبل اعلانات ";
            //            break;
            //        case "recipient/payment":
            //            note = "الدفع لمستقبل اعلانات ";
            //            break;

            //        case "settings/index":
            //            note = "عرض اعددات النظام ";
            //            break;
            //        case "settings/log":
            //            note = "عرض حركات النظام ";
            //            break;

            //    }
            //    if(!string.IsNullOrEmpty(note))
            //    {
            //        Log l = new Log()
            //        {
            //            user_id = model.Id,
            //            cdate = DateTime.Now,
            //            action = rd.GetRequiredString("action"),
            //            controller = rd.GetRequiredString("controller"),
            //            details = note
            //        };
            //        db.Logs.Add(l);
            //        db.SaveChanges();
            //    }

            //}
            //      throw new HttpException(401, "Unauthorized Access");
            //  /*if (Roles != null && !model.RoleName.ToLower().Equals(Roles.ToLower()))
            //  {
            //      throw new HttpException(401,"Unauthorized Access");
            //  }*/
        }

    }
}