﻿using Mhana.Helper;
using Mhana.Models;
using Mhana.WiziQ;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using static Mhana.Models.Enums;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using System.Security.Cryptography;
using System.Data.Entity;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;
using System.Web.Script.Serialization;
using System.Configuration;

namespace Mhana.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            string result = @"<rsp status='ok' call_id='72fe7bbe8df0'><method>add_teacher</method><add_teacher status='true'><teacher_id>1482</teacher_id><teacher_email>mike@example.com</teacher_email></add_teacher></rsp>";            
            rsp resultObject = Functions.DeserializeXML<rsp>(result);
            Response.Write(resultObject.add_teacher.teacher_id);
          
            return View();
        }
        public async Task<ActionResult> CheckIAM(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                    return View();

               // Session["user_id"] = id;
                //var cers = StateCertificate.getAllCertificate();
                //foreach(var vv in cers)
                //{
                //    Response.Write(vv+"</br>----------------------</br>");
                //}

                //var cers2 = StateCertificate.getAllCertificate2(StoreLocation.LocalMachine);
                //foreach (var vv in cers2)
                //{
                //    Response.Write(vv + "</br>----------222------------</br>");
                //}


                //X509Certificate2 myCert =
                //StateCertificate.LoadCertificate(StoreLocation.LocalMachine,
                //"CN=mhana.azurewebsites.net");
                //string myText = "This is the text I wish to encrypt";
                //Response.Write("UNENCRYPTED: " + myText + "\n");
                //Console.WriteLine("UNENCRYPTED: " + myText);
                //string encrypted = StateCertificate.Encrypt(myCert, myText);
                //Response.Write("ENCRYPTED: " + encrypted + "\n");
                //string decrypted = StateCertificate.Decrypt(myCert, encrypted);
                //Response.Write("DECRYPTED: " + decrypted + "\n");


                // var xxx = StateCertificate.Encrypt();

                var item = StateCertificate.getCer(id);
                foreach (var x in item)
                {
                    ViewBag.txt = item[0];
                }
                HttpClient client = new HttpClient();
                var values = new Dictionary<string, string>
                {                   
                };
                var content = new FormUrlEncodedContent(values);

                //var xxx = StateCertificate.Encrypt2();                              
                var response = await client.PostAsync(item[0], content);
                var responseString = await response.Content.ReadAsStringAsync();  
                  Response.Write(responseString);
                //Response.Write("Cer 2 :" + xxx);
            }
            catch (Exception e)
            {
                Response.Write("EXCEPTION: "+ e.Message + "\n");


            }
            //String timeStamp = GetTimestamp(DateTime.Now);
            //Response.Redirect("https://iambeta.elm.sa/authservice/authorize?scope=openid&response_type=id_token&response_mode=form_post&client_id=16352727&redirect_uri=http://privatelessonforyou.com/Home/callback&nonce" + Guid.NewGuid() + "&ui_locales=ar&prompt=login&max_age=" + timeStamp);
            return View();
        }
        public async Task<ActionResult> CheckIAMTest(string id)
        {
            return RedirectToAction("CallBack",new {id = id });
        }
            public static String GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmss");
        }
        public ActionResult Login()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Login(LoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                TempData["Message"] = " هنالك خطأ في اسم المستخدم أو كلمة المرور ";
                TempData["Status"] = "danger";
                return View(model);
            }
            using (var context = new ApplicationDbContext())
            {

                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var result = await UserManager.FindAsync(model.username, model.password);
                
                if (result != null)
                {
                    if (result.status == -1)
                    {
                        TempData["Message"] = "حسابك موقوف يرجى مراحعة الادارة ";
                        TempData["Status"] = "danger";
                        return RedirectToAction("Login", "Home", new { area = "" });
                    }

                    if (result.Roles.FirstOrDefault() != null && result.Roles.FirstOrDefault().RoleId == "1")
                    {
                        Session["User"] = result;
                        return RedirectToAction("Index", "Dashboard", new { area = "Admin" });
                    }

                    else if (result.Roles.FirstOrDefault() != null && result.Roles.FirstOrDefault().RoleId == "2")
                    {
                        Session["User"] = result;
                        return RedirectToAction("Index", "Dashboard", new { area = "Teacher" });
                    }
                    //else if (result.Roles.FirstOrDefault() != null && result.Roles.FirstOrDefault().RoleId == "3")
                    //{
                    //    Session["User"] = result;
                    //    return RedirectToAction("Index", "Dashboard", new { area = "Teacher" });
                    //}
                    else
                    {
                        //  TempData["Error"] = " ليس لديك صلاحية المرور ";
                        TempData["Message"] = " ليس لديك صلاحية المرور ";
                        TempData["Status"] = "danger";
                        return RedirectToAction("Login", "Home", new { area = "" });
                    }


                }
                // TempData["Error"] = " هنالك خطأ في اسم المستخدم أو كلمة المرور";
                TempData["Message"] = " هنالك خطأ في اسم المستخدم أو كلمة المرور ";
                TempData["Status"] = "danger";
                return RedirectToAction("Login", "Home", new { area = "" });
            }
        }
        public ActionResult ResetPassword()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {

            if (!ModelState.IsValid)
            {
                return View(model);
            }
            using (var context = new ApplicationDbContext())
            {
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var user = await UserManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    TempData["Message"] = "حسابك موقوف يرجى مراحعة الادارة ";
                    TempData["Status"] = "danger";
                    return RedirectToAction("ResetPassword", "Home", new { area = "" });
                }

                // var result = await UserManager.ResetPasswordAsync(user.Id,model.Code, model.Password);
                if (!string.IsNullOrEmpty(model.Code))
                {
                    UserManager.RemovePassword(user.Id);
                    UserManager.AddPassword(user.Id, model.Password);
                    TempData["Message"] = "لقد تم تغيير كلمة المرور بنجاح ";
                    TempData["Status"] = "success";
                    return RedirectToAction("ResetPassword", "Home", new { area = "" });
                }
                TempData["Message"] = "انتهت صلاحية الرابط المرسل على بريدك يرجى طلب رابط جديد";
                TempData["Status"] = "danger";
                return RedirectToAction("ResetPassword", "Home", new { area = "" });
            }

        }

        public ActionResult PaymentNotify(string id = null)
        {
            try
            {
                
                using (StreamWriter writetext = new StreamWriter(HttpContext.Server.MapPath("~/write.txt"), true))
                {
                    writetext.WriteLine("----------------------------------- Response" + DateTime.Now + " -----------------");
                    writetext.WriteLine(" Response : " + Request.Url.PathAndQuery);
                    writetext.WriteLine("----------------------------------- Response" + DateTime.Now + "  -----------------");
                }
                var source = Request.QueryString["id"];
             
                using (StreamWriter writetext = new StreamWriter(HttpContext.Server.MapPath("~/write.txt"), true))
                {
                    writetext.WriteLine("----------------------------------- source " + DateTime.Now + "  -----------------");
                    writetext.WriteLine("source : " + source);
                    writetext.WriteLine("----------------------------------- source " + DateTime.Now + "  -----------------");
                }

                string qs = source;        
                using (var db = new MhanaDbEntities())
                {

                    var adv = db.StdNewRequests.Where(w => w.txt_id == source).FirstOrDefault();
                    if (!string.IsNullOrEmpty(qs))
                    {
                        var responseData = StatusRequest(qs);
                        PaymentModelResult model = new PaymentModelResult();
                        if (responseData != null)
                        {
                            foreach (var group in responseData)
                            {
                                if (group.Key == "ndc")
                                {
                                    model.ndc = group.Value;
                                    if (adv != null)
                                    {
                                        adv.status = (int)Enums.Status.Visible;
                                        db.Entry(adv).State = EntityState.Modified;
                                        db.SaveChanges();
                                    }

                                }
                                using (StreamWriter writetext = new StreamWriter(HttpContext.Server.MapPath("~/write.txt"), true))
                                {
                                    writetext.WriteLine("----------------------------------- key value " + DateTime.Now + "  -----------------");
                                    writetext.WriteLine("key : " + group.Key);
                                    writetext.WriteLine("value : " + group.Value);
                                    writetext.WriteLine("----------------------------------- key value " + DateTime.Now + "  -----------------");
                                }
                                if (group.Key == "result")
                                {
                                    foreach (var group2 in responseData["result"])
                                    {
                                        if (group2.Key == "description")
                                        {
                                            model.description = group2.Value;

                                        }
                                        if (group.Key == "code")
                                        {
                                            model.code = group2.Value;
                                            Regex successPattern = new Regex(@"(000\.000\.|000\.100\.1|000\.[36])");
                                            Regex successManuelPattern = new Regex(@"(000\.400\.0[^3]|000\.400\.100)");
                                            Match match1 = successPattern.Match(group2.Value);
                                            Match match2 = successManuelPattern.Match(group2.Value);

                                            if (match1.Success || match2.Success)
                                            {
                                                model.status = 1;
                                                //ViewBag.regId = response["id"];
                                            }
                                        }
                                        using (StreamWriter writetext = new StreamWriter(HttpContext.Server.MapPath("~/write.txt"), true))
                                        {
                                            writetext.WriteLine("----------------------------------- key value " + DateTime.Now + "  -----------------");
                                            writetext.WriteLine("key : " + group2.Key);
                                            writetext.WriteLine("value : " + group2.Value);
                                            writetext.WriteLine("----------------------------------- key value " + DateTime.Now + "  -----------------");
                                        }

                                    }
                                }

                            }
                        }
                    }
                    return View();
                }
            }
            catch (Exception ex)
            {
                using (StreamWriter writetext = new StreamWriter(HttpContext.Server.MapPath("~/write.txt"), true))
                {
                    writetext.WriteLine("----------------------------------- EX value" + DateTime.Now + "  -----------------");
                    writetext.WriteLine("EX : " + ex.Message);
                    writetext.WriteLine("----------------------------------- EX value" + DateTime.Now + " -----------------");
                }

                return View();
            }
        }
        public Dictionary<string, dynamic> StatusRequest(string resourcePath)
        {

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            Dictionary<string, dynamic> responseData;
            string data = ConfigurationManager.AppSettings["HyperPayEntityId"].ToString();// "entityId=8acda4cc77b5a3a30177ce2752532d43";
            string url = "https://oppwa.com/v1/checkouts/" + resourcePath + "/payment?" + data;
            //string url = "https://test.oppwa.com/v1/checkouts/3EA66E05222AE381DBF8B6DB6698C312.uat01-vm-tx02/payment?entityId=8ac7a4c7706822c8017077724f7c0a63";
            //string url = "https://test.oppwa.com"+ resourcePath + "?" + data;            
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.Headers["Authorization"] = ConfigurationManager.AppSettings["HyperPayAuthorization"].ToString();// "Bearer OGFjZGE0Y2M3N2I1YTNhMzAxNzdjZTI2ZTU1NzJkM2J8WGpicVRxWjRCaw==";
            request.ContentType = "application/json;charset=UTF-8";
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                var s = new JavaScriptSerializer();
                responseData = s.Deserialize<Dictionary<string, dynamic>>(reader.ReadToEnd());
                reader.Close();
                dataStream.Close();
            }
            return responseData;
        }
        public ActionResult WiziQ()
        {
           
            ViewBag.Title = "Home Page";

            return View();
        }
        [HttpPost]
        public ActionResult WiziQ(string t_id = "44ac2c0c-d123-4679-bcee-70b43d00dce8")
        {
            //304082
            using (var db = new MhanaDbEntities())
            {
                var teacher = db.AspNetUsers.Where(w => w.Id == t_id).FirstOrDefault();
                TeacherWiziQ tw = new TeacherWiziQ()
                {
                    about_the_teacher = teacher.details,
                    can_schedule_class = "1",
                    email = teacher.Email,
                    name = teacher.fullname,
                    mobile_number = "",
                    phone_number = teacher.photo,
                    password = "123456"

                };
             var result = Mhana.WiziQ.WizIQClass.AddTeacher(tw);
                Response.Write(result);
                ViewBag.result = result;
                return View(ViewBag.result);
            }
        }


        public ActionResult WiziQT()
        {

            ViewBag.Title = "Home Page";

            return View();
        }
        public ActionResult getMessage(Enum status, string meesage, string action, string controller, int? id = null)
        {
            TempData["Message"] = string.IsNullOrEmpty(meesage) ? "تمت العملية بنجاح" : meesage;
            TempData["Status"] = status;
            return RedirectToAction(action, controller, new { id = id });
        }
        [HttpPost]
        public async Task<ActionResult> ForgotPassword(string email)
        {
            using (var context = new ApplicationDbContext())
            {
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var user = await UserManager.FindByEmailAsync(email);
                if (user == null)
                {
                    return getMessage(MStatus.remove, "هذا البريد غير مسجل لدينا", "Login", "Home");
                    // Don't reveal that the user does not exist
                    //  return RedirectToAction("login", "Account");
                }
                ChangeUserModel mail = new ChangeUserModel()
                {
                    name = user.fullname,
                    to = user.Email,
                    url = Request.Url.Scheme + "://" + Request.Url.Host + Url.Action("ResetPassword", "Home", new { code = user.Id })
                };
                new Mhana.Controllers.MailerController().foregetPassword(mail).Deliver();
                return getMessage(MStatus.check, "يرجى مراجعة بريدك الالكتروني لاكمال عملية تغيير كلمة المرور", "Login", "Home");
            }
        }
        [HttpPost]
        public ActionResult WiziQT(int? t_id = 1)
        {
            //304082
            using (var db = new MhanaDbEntities())
            {
                var course = db.Courses.Where(w => w.Id == t_id).FirstOrDefault();
                CourseWiziQ tw = new CourseWiziQ()
                {
                     title = course.title,
                      attendee_limit = "2",
                       create_recording = "false",
                        duration = "60",
                         start_time = "10/16/2019 12:00",
                          time_zone = "Asia/Riyadh",
                           presenter_email = "abed2@gmail.com"


                };
                var result = Mhana.WiziQ.WizIQClass.Create(tw);
                Response.Write(result);
                ViewBag.result = result;
                return View(ViewBag.result);
            }
        }
        [HttpPost]
        public ActionResult LogOff()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            Response.Cache.SetNoStore();
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            return RedirectToAction("Login", "Home", new { area = "" });
        }
        //<class_master_id>1456513</class_master_id><class_id>1133437</class_id>
        public ActionResult WiziQS()
        {

            ViewBag.Title = "Home Page";

            return View();
        }
        [HttpPost]
        public ActionResult WiziQS(int? t_id = 1)
        {
            //304082
            using (var db = new MhanaDbEntities())
            {
                var course = db.Courses.Where(w => w.Id == t_id).FirstOrDefault();
                CourseWiziQ tw = new CourseWiziQ()
                {
                    title = course.title,
                    attendee_limit = "10",
                    create_recording = "false",
                    duration = "60",
                    start_time = "10/16/2019 12:00",
                    time_zone = "Asia/Riyadh",
                    presenter_email = "abed2@gmail.com"


                };
                var result = Mhana.WiziQ.WizIQClass.Create(tw);
                Response.Write(result);
                ViewBag.result = result;
                return View(ViewBag.result);
            }
        }

        [HttpPost]
        public ActionResult CallBack(FormCollection form,string id)
        {
            try
            {
                Response.Write("<p style='padding:15px;'> id : " + id + "</p>");
                //var re = Request;
                //var headers = Request.Headers;
                //foreach (var key in Request.Headers.AllKeys)
                //{
                //    Response.Write("<p style='padding:15px;'>"+key + ": " + Request.Headers[key] + "</p>");
                //}
                //string[] keys = Request.Form.AllKeys;
                //for (int i = 0; i < keys.Length; i++)
                //{
                //    Response.Write(keys[i] + ": " + Request.Form[keys[i]] + "<br>");
                //}
                foreach (var key in form.AllKeys)
                {
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/debug.txt"), true))
                        file.WriteLine(key + ": " + Request.Form[key]);

                    if (key == "id_token")
                    {
                        var decoded = ConvertFromBase64String(Request.Form[key]);
                        if (decoded !=null)
                        {
                            var data = Encoding.UTF8.GetString(decoded);
                            using (System.IO.StreamWriter file = new System.IO.StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/debug.txt"), true))
                                file.WriteLine("Data : " + data);
                        }
                    }

                }

                //using (var db = new MhanaDbEntities())
                //{
                //    var current_user = db.AspNetUsers.Where(w => w.Id == id).FirstOrDefault();
                //    if (current_user != null)
                //    {
                //        current_user.absher = 1;
                //        current_user.absher_no = "";
                //        db.Entry(current_user).State = EntityState.Modified;
                //        db.SaveChanges();
                //    }
                //}


                foreach (var key in form.AllKeys)
                {
                    Response.Write("<p style='padding:15px;'>" + key + ": " + Request.Form[key] + "</p>");

                    if (key == "id_token")
                    {


                        var decoded = ConvertFromBase64String(Request.Form[key]);
                        var data = Encoding.UTF8.GetString(decoded);
                        Response.Write("<p style='padding:15px;'> My Data : " + data + "</p>");
                        if (data.Contains("englishName"))
                        {
                            // var data = Encoding.ASCII.GetString(decoded);

                         //   var items = data.Split('}');

                          //  if (items.Count() > 1)
                          //  {
                                //data = "[" + items[0] + "}," + items[1] + "}" + "]";
                                //var myData = items[1] + "}";
                                var myDataObject = JsonConvert.DeserializeObject<AbsherData>(data);
                                dynamic parsedJson = JsonConvert.DeserializeObject(data);
                                // Response.Write(myDataObject.arabicName);
                                string user_id = id;

                                using (var db = new MhanaDbEntities())
                                {
                                    var current_user = db.AspNetUsers.Where(w => w.Id == id).FirstOrDefault();
                                    if (current_user != null)
                                    {
                                        current_user.absher = 1;
                                        current_user.absher_no = myDataObject.sub;
                                        current_user.fullname = myDataObject.arabicName;
                                        current_user.gender = myDataObject.gender == "male" ? "ذكر" : "أنثى";
                                        current_user.first_name = myDataObject.arabicFirstName;
                                        current_user.last_name = myDataObject.arabicFamilyName;
                                        //current_user.dob = DateTime.Parse(myDataObject.dob);
                                        db.Entry(current_user).State = EntityState.Modified;
                                        db.SaveChanges();
                                    }

                                    if (myDataObject != null)
                                    {
                                        myDataObject.cdate = DateTime.Now;
                                        myDataObject.mhana_user_id = user_id;
                                        db.AbsherDatas.Add(myDataObject);
                                        db.SaveChanges();
                                    }
                                }
                           // }
                        }

                    }
                    if (key == "state")
                    {

                        Response.Write("<p style='padding:15px;'> My State : " + Request.Form[key] + "</p>");
                        // var decoded = ConvertFromBase64String(Request.Form[key]);
                        //   var data = Encoding.UTF8.GetString(decoded);
                        //   Response.Write(data);
                    }

                }

                // Response.Redirect("mhanna://success");
                return View();
            }catch(Exception ex)
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/debug.txt"), true))
                    file.WriteLine(ex.ToString());

                return View();
            }
        }

        [HttpGet]
        public ActionResult CallBack(string id)
        {
            //string yy = "FlPRzW6vGPk+Mk371TXTTGWzf+S7CNrkKAXIZxiE3KQ=";

            //var ydecoded = ConvertFromBase64String(yy);
            //var ydata = Encoding.UTF8.GetString(ydecoded);
            //Response.Write(ydata);
            //using (System.IO.StreamWriter file = new System.IO.StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/debug.txt"), true))
            //    file.WriteLine("Session Get: " + Convert.ToString(Session["user_id"]));
            string xx = "eyJhbGciOiJSUzI1NiJ9.eyJlbmdsaXNoTmFtZSI6Ik1pc2hhcmkgTW9oYW1tZWQgTXVhdGggS2hhbGlkIiwiYXJhYmljRmF0aGVyTmFtZSI6ItmF2K3ZhdivIiwiZW5nbGlzaEZhdGhlck5hbWUiOiJNb2hhbW1lZCIsInN1YiI6IjEwMjAxOTEzNTciLCJnZW5kZXIiOiJNYWxlIiwiaXNzIjoiaHR0cHM6XC9cL3d3dy5pYW0uZ292LnNhXC91c2VyYXV0aCIsImNhcmRJc3N1ZURhdGVHcmVnb3JpYW4iOiJUaHUgTm92IDE2IDAwOjAwOjAwIEFTVCAyMDE3IiwiZW5nbGlzaEdyYW5kRmF0aGVyTmFtZSI6Ik11YXRoIiwidXNlcmlkIjoiMTAyMDE5MTM1NyIsImlkVmVyc2lvbk5vIjoiNCIsImFyYWJpY05hdGlvbmFsaXR5Ijoi2KfZhNi52LHYqNmK2Kkg2KfZhNiz2LnZiNiv2YrYqSIsImFyYWJpY05hbWUiOiLZhdi02KfYsdmKINmF2K3ZhdivINmF2LnYp9iwINiu2KfZhNivIiwiYXJhYmljRmlyc3ROYW1lIjoi2YXYtNin2LHZiiIsIm5hdGlvbmFsaXR5Q29kZSI6IjExMyIsImlxYW1hRXhwaXJ5RGF0ZUhpanJpIjoiMTQ0OFwvMDlcLzExIiwibGFuZyI6ImFyIiwiZXhwIjoxNTk3NjI3NzY2LCJpYXQiOjE1OTc2Mjc2MTYsImlxYW1hRXhwaXJ5RGF0ZUdyZWdvcmlhbiI6IlRodSBGZWIgMTggMDA6MDA6MDAgQVNUIDIwMjciLCJpZEV4cGlyeURhdGVHcmVnb3JpYW4iOiJUaHUgRmViIDE4IDAwOjAwOjAwIEFTVCAyMDI3IiwianRpIjoiaHR0cHM6XC9cL2lhbWJldGEuZWxtLnNhLDUyZTUxZjUwLWRkNDMtNDNhNS1hZmFlLWJkMWQ0ODNlOTZiNiIsImlzc3VlTG9jYXRpb25BciI6IiIsImRvYkhpanJpIjoiMTQwMlwvMDFcLzAxIiwiY2FyZElzc3VlRGF0ZUhpanJpIjoiMTQzOVwvMDJcLzI3IiwiZW5nbGlzaEZpcnN0TmFtZSI6Ik1pc2hhcmkiLCJpc3N1ZUxvY2F0aW9uRW4iOiIiLCJhcmFiaWNHcmFuZEZhdGhlck5hbWUiOiLZhdi52KfYsCIsImF1ZCI6Imh0dHA6XC9cL3ByaXZhdGVsZXNzb25mb3J5b3UuY29tXC8iLCJuYmYiOjE1OTc2Mjc0NjYsIm5hdGlvbmFsaXR5IjoiU2F1ZGkgQXJhYmlhIiwiZG9iIjoiV2VkIE9jdCAyOCAwMDowMDowMCBBU1QgMTk4MSIsImVuZ2xpc2hGYW1pbHlOYW1lIjoiS2hhbGlkIiwiaWRFeHBpcnlEYXRlSGlqcmkiOiIxNDQ4XC8wOVwvMTEiLCJhc3N1cmFuY2VfbGV2ZWwiOiIiLCJhcmFiaWNGYW1pbHlOYW1lIjoi2K7Yp9mE2K8ifQ.IAPT--2SeMCLTgmAg_hDAdR5_jGGMzgm1ug0-Xoiggnx3ZXJmMsZZJuIjwRt29Dn2kOKmXEtvOg9pDJjUiZCjTOdhMy7P-g4RzcLSZ8pUHcmgqMEH2NfSagOWCb3zkQRjokmCYIHRtcbdXmqgjI2LrOUnyUEpqvkooopV-Xr78egiFCOjE-Myv0QZAcp6wCzY3nUe41zLyBN8U_95knfPaCHrwwwpb20BpbgYIUW26R0Qd-jE-PqK4qz8tCN-4EZwCigWDArQQeaVHH_KVePrSgIXKW6qCssrpCcfNsjou64T9Z0PVV5gwwdkg5nGj-iqQZP59KStVJ73Jb8CXiriA";
            var decoded = ConvertFromBase64String(xx);
            var data = Encoding.UTF8.GetString(decoded);
            if (data.Contains("RS256"))
            {
                // var data = Encoding.ASCII.GetString(decoded);
                var items = data.Split('}');
                data = "[" + items[0] + "}," + items[1] + "}" + "]";
                var myData = items[1] + "}";
                var myDataObject = JsonConvert.DeserializeObject<AbsherData>(myData);
                dynamic parsedJson = JsonConvert.DeserializeObject(data);
                Response.Write(parsedJson);
               // string user_id = Convert.ToString(Session["user_id"]);
                using (var db = new MhanaDbEntities())
                {
                    var current_user = db.AspNetUsers.Where(w => w.Id == id).FirstOrDefault(); 
                    if(current_user !=null)
                    {
                        current_user.absher = 1;
                        current_user.absher_no = myDataObject.sub;
                        db.Entry(current_user).State = EntityState.Modified;
                    }                   
                    myDataObject.mhana_user_id = id;
                    db.AbsherDatas.Add(myDataObject);
                    db.SaveChanges();
                }
                //   Response.Write(JsonConvert.SerializeObject(parsedJson, Newtonsoft.Json.Formatting.Indented));
            }
            return View();
        }

        private static byte[] ConvertFromBase64String(string input)
        {

            var items = input.Split('.');
            if(items.Count()>0)
            {
                input = items[1];
            }

            //var l = input.Length;
            //input = l % 4 == 1 && input[l - 1] == '='? input.Substring(0, l - 1) : input;
                
                


            //string working = input.Replace("-", "+").Replace("_", "/").Replace(".","");
            //while (working.Length % 4 != 0)
            //{
            //    working += '=';
            //}
            //while (working.Length % 3 != 0)
            //{
            //    working += '=';
            //}
            try
            {
                return Convert.FromBase64String(input);
            }
            catch (Exception)
            {
                // .Net core 2.1 bug
                // https://github.com/dotnet/corefx/issues/30793
                try
                {

                    return Convert.FromBase64String(input.Replace('-', '+').Replace('_', '/'));
                }
                catch (Exception) { }
                try
                {
                    return Convert.FromBase64String(input.Replace('-', '+').Replace('_', '/') + "=");
                }
                catch (Exception) { }
                try
                {
                    return Convert.FromBase64String(input.Replace('-', '+').Replace('_', '/') + "==");
                }
                catch (Exception) { }

                try
                {
                    return Convert.FromBase64String(input.Replace("-", "+").Replace("_", "/").Replace(".", "") + "=");
                }
                catch (Exception) { }

                try
                {
                    return Convert.FromBase64String(input.Replace("-", "+").Replace("_", "/").Replace(".", "") + "==");
                }
                catch (Exception) { }


                try
                {
                    return Convert.FromBase64String(input.Replace("-", "+").Replace("_", "/").Replace(".", ""));
                }
                catch (Exception) { }



                return null;
            }
        }

    }
}
