﻿using Mhana.Helper;
using Mhana.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.AspNet.Identity;
using System.Web;
using Newtonsoft.Json;
using System.Data.Entity;

namespace Mhana.Controllers
{
    [RoutePrefix("api/v1/General")]
    public class GeneralController : ApiController
    {
        /// <summary>
        ///    تواصل معنا      .
        /// </summary>
        [Route("Contact")]
        [ResponseType(typeof(string))]
        [HttpPost]
        public IHttpActionResult Contact(ContactModel model)
        {
            var user_id = User.Identity.GetUserId();
            using (var db = new MhanaDbEntities())
            {
                var user = db.AspNetUsers.Where(w => w.Id == user_id).FirstOrDefault();               
                if (string.IsNullOrEmpty(model.message))
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "خطأ في البيانات المرسلة", false, null));
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "شكرا لك ! تم ارسال رسالتك بنجاح", true, null));

            }

        }


        /// <summary>
        ///    ارسال شكوى      .
        /// </summary>
        [Route("Complaint")]
        [ResponseType(typeof(string))]
        [HttpPost]
        public IHttpActionResult Complaint(ContactModel model)
        {
            var user_id = User.Identity.GetUserId();
            using (var db = new MhanaDbEntities())
            {
                // var user = db.AspNetUsers.Where(w => w.Id == user_id).FirstOrDefault();   
                
                if (!model.course_id.HasValue)
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "خطأ في البيانات المرسلة", false, null));
                var _cc = db.Courses.Where(w => w.Id == model.course_id).FirstOrDefault();
                MyRate mr = new MyRate()
                {
                    cdate = DateTime.Now,
                    comment = model.subject,
                    course_id = model.course_id,
                    rate = model.rate,
                    rate_type = 2,
                    std_id = user_id,  
                     teacher_id = _cc!=null?_cc.teacher_id:null
                };
                db.MyRates.Add(mr);
                db.SaveChanges();                
               
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "شكرا لك ! تم ارسال تقييمك بنجاح", true, null));

            }
        }


        /// <summary>
        ///    استعراض كافة المدن السعودية.
        ///    يتم من خلالها طلب مدن السعودية 
        ///    
        /// </summary>

        [Route("GetAllCity")]
        [ResponseType(typeof(List<string>))]
        [HttpGet]
        // [CacheFilterAttribute(Duration = 60)]
        public IHttpActionResult GetAllCity()
        {
            string file = HttpContext.Current.Server.MapPath("~/saudi_regions.json");
            //deserialize JSON from file  
            string Json = System.IO.File.ReadAllText(file);
            // JavaScriptSerializer ser = new JavaScriptSerializer();
            List<CityListModel> list = JsonConvert.DeserializeObject<List<CityListModel>>(Json);
            //List<CityList> list = JsonConvert.DeserializeObject<List<CityList>>(Json);
            var items = list.OrderBy(o => o.city_name).Select(s => s.city_name).Distinct().ToList();
            return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, null, true, items));
        }
        /// <summary>
        ///    طلب الأحياء لمدينة محددة.
        ///    من خلال تمرير المدينة تطلب احيا السعودية 
        /// </summary>

        [Route("GetDistricts")]
        [ResponseType(typeof(List<string>))]
        [HttpGet]
        // [CacheFilterAttribute(Duration = 60)]
        public IHttpActionResult GetDistricts(string city)
        {
            string file = HttpContext.Current.Server.MapPath("~/saudi_regions.json");
            //deserialize JSON from file  
            string Json = System.IO.File.ReadAllText(file);
            // JavaScriptSerializer ser = new JavaScriptSerializer();
            List<CityListModel> list = JsonConvert.DeserializeObject<List<CityListModel>>(Json);
            //List<CityList> list = JsonConvert.DeserializeObject<List<CityList>>(Json);
            var items = list.Where(w => w.city_name == city).OrderBy(o => o.district_name).Select(s => s.district_name).ToList();
            return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, null, true, items));
        }


        /// <summary>
        ///    الشروط والأحكام.
        /// </summary>
        [Route("GetPages")]
        [ResponseType(typeof(term_policy))]
        [HttpGet]
        //[CacheFilterAttribute(Duration = 60)]
        public IHttpActionResult GetPages()
        {
            using (var db = new MhanaDbEntities())
            {
                var items = db.Settings.ToList();
                term_policy tp = new term_policy()
                {                  
                    about = items.Where(w => w.option_key == "about").FirstOrDefault().option_value,
                    term = items.Where(w => w.option_key == "term").FirstOrDefault().option_value,
                };
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, null, true, tp));
            }

        }


        /// <summary>
        ///    التخصصات الأساسية.
        /// </summary>
        [Route("GetSpecialization")]
        [ResponseType(typeof(List<keyValue>))]
        [HttpGet]
        //[CacheFilterAttribute(Duration = 60)]
        public IHttpActionResult GetSpecialization()
        {
            using (var db = new MhanaDbEntities())
            {
                var items = db.specializations.Select(s=>new keyValue() {  id = s.Id, value = s.name}).ToList();               
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, null, true, items));
            }

        }
        /// <summary>
        ///    التخصصات الفرعية.
        /// </summary>
        [Route("GetBranchSpecialization")]
        [ResponseType(typeof(List<keyValue>))]
        [HttpGet]
        //[CacheFilterAttribute(Duration = 60)]
        public IHttpActionResult GetBranchSpecialization(int?id)
        {
            using (var db = new MhanaDbEntities())
            {
                var items = db.branch_specialization.Where(w=>w.specialization_id==id).Select(s => new keyValue() { id = s.Id, value = s.name }).ToList();
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, null, true, items));
            }

        }

        /// <summary>
        ///    المراحل التعليمية.
        /// </summary>
        [Route("GetEducationLevels")]
        [ResponseType(typeof(List<keyValue>))]
        [HttpGet]
        //[CacheFilterAttribute(Duration = 60)]
        public IHttpActionResult GetEducationLevels()
        {
            using (var db = new MhanaDbEntities())
            {
                var items = db.education_level.Select(s => new keyValueEdu() { id = s.Id, value = s.name , sub_level = s.education_level_sub.Select(x=>new keyValue() { id = x.id, value = x.name }).ToList() }).ToList();
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, null, true, items));
            }

        }

        /// <summary>
        ///    نوع الشهادة.
        /// </summary>
        [Route("GetCertificateType")]
        [ResponseType(typeof(List<keyValue>))]
        [HttpGet]
        //[CacheFilterAttribute(Duration = 60)]
        public IHttpActionResult GetCertificateType()
        {
            using (var db = new MhanaDbEntities())
            {
                List<string> cer = new List<string>();
                cer.Add("دبلوم");
                cer.Add("بكالوريوس");
                cer.Add("ماجستير");
                cer.Add("دكتوراه");
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, null, true, cer));
            }

        }


        /// <summary>
        ///    أنواع الشكاوى.
        /// </summary>
        [Route("ComplaintTypes")]
        [ResponseType(typeof(List<string>))]
        [HttpGet]
        //[CacheFilterAttribute(Duration = 60)]
        public IHttpActionResult ComplaintTypes()
        {
            using (var db = new MhanaDbEntities())
            {
                List<string> cer = new List<string>();
                cer.Add("شكوى من الصوت ؟");
                cer.Add("شكوى من المعلم ؟");                
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, null, true, cer));
            }

        }

        /// <summary>
        ///    المواد العليمة .
        /// </summary>
        [Route("GetMaterial")]
        [ResponseType(typeof(List<string>))]
        [HttpGet]
        //[CacheFilterAttribute(Duration = 60)]
        public IHttpActionResult GetMaterial()
        {
            
                using (var db = new MhanaDbEntities())
                {
                    List<string> cer = new List<string>();
                    var item = db.Settings.Where(w => w.option_key == "material").FirstOrDefault();
                    if(item !=null)
                    {
                        if(!string.IsNullOrEmpty(item.option_value))
                        {
                            cer = item.option_value.Split(',').ToList();
                        }
                    }
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, null, true, cer));
                }                

        }



        /// <summary>
        ///    تغيير حالة الاشعار الى مقروء .
        /// </summary>
        [Route("MakeNotificationIsRead")]
        [ResponseType(typeof(string))]
        [HttpGet]
        //[CacheFilterAttribute(Duration = 60)]
        public IHttpActionResult MakeNotificationIsRead(int?id)
        {

            using (var db = new MhanaDbEntities())
            {              
                var item = db.Notifications.FirstOrDefault(w => w.id == id);
                if (item != null)
                {
                    item.is_read = 1;
                    db.Entry(item).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "تم قراءة الاشعار", true, null));
            }

        }
    }
}
