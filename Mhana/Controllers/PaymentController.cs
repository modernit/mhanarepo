﻿using Mhana.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Mhana.Helper;
using Microsoft.AspNet.Identity;
using System.Web;
using System.IO;
using System.Configuration;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Text;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;


namespace Mhana.Controllers
{
   // [Authorize]
    [RoutePrefix("api/v1/Payment")]
    public class PaymentController : ApiController
    {

        /// <summary>
        ///   الحصول على checkout_id.
        /// </summary>
        [Route("CheckPayment")]
        [HttpPost]
        [Authorize]
        [ResponseType(typeof(PaymentModelResult))]
        public IHttpActionResult CheckPayment(PaymentModelReq req)
        {
            //["result"]["description"];
            if (!req.request_id.HasValue || !req.amount.HasValue || req.amount <= 0)
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات المرسلة  ", false, null));
            using (StreamWriter writetext = new StreamWriter(HttpContext.Current.Server.MapPath("~/debug.txt"), true))
            {
                writetext.WriteLine("----------------------------------- Model " + DateTime.Now + " -----------------");
                writetext.WriteLine("Amount: {0} ---- Adv: {1}", req.amount, req.request_id);
            }
            var responseData = myrequest(req.amount, req.request_id);
            PaymentModelResult model = new PaymentModelResult();
            using (var db = new MhanaDbEntities())
            {
                var _request = db.StdNewRequests.Where(w => w.Id == req.request_id).FirstOrDefault();



                if (responseData != null)
                {
                    using (StreamWriter writetext = new StreamWriter(HttpContext.Current.Server.MapPath("~/write.txt"), true))
                    {
                        writetext.WriteLine("----------------------------------- Payment responseData " + DateTime.Now + " -----------------");
                        foreach (var group in responseData)
                        {
                            writetext.WriteLine("Key: {0} Value: {1}", group.Key, group.Value);
                        }
                        foreach (var group in responseData["result"])
                        {
                            writetext.WriteLine("Key: {0} Value: {1}", group.Key, group.Value);
                        }
                        writetext.WriteLine("responseData : " + responseData);
                        writetext.WriteLine("----------------------------------- Payment responseData" + DateTime.Now + " -----------------");
                    }

                    foreach (var group in responseData)
                    {
                        if (group.Key == "id")
                        {
                            model.checkout_id = group.Value;

                        }
                        if (group.Key == "ndc")
                        {
                            model.ndc = group.Value;
                            _request.txt_id = group.Value;
                            db.Entry(_request).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        if (group.Key == "result")
                        {
                            foreach (var group2 in responseData["result"])
                            {
                                if (group2.Key == "description")
                                {
                                    model.description = group2.Value;
                                    if (group2.Value == "successfully created checkout")
                                    {
                                        model.status = 1;
                                        //  db.SaveChanges();
                                    }
                                    else
                                    {
                                        model.status = -1;
                                    }

                                }
                                if (group.Key == "code")
                                {
                                    model.code = group2.Value;
                                }


                            }
                        }

                    }


                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "", true, model));
                }
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات المرسلة  ", false, null));
            }
            //using (StreamWriter writetext = new StreamWriter(HttpContext.Current.Server.MapPath("~/write.txt"), true))
            //{

            //    foreach (var group in responseData)
            //    {
            //        writetext.WriteLine("Key: {0} Value: {1}", group.Key, group.Value);
            //    }

            //    writetext.WriteLine("----------------------------------- Payment responseData -----------------");

            //    foreach (var group in responseData["result"])
            //    {
            //        writetext.WriteLine("Key: {0} Value: {1}", group.Key, group.Value);
            //    }
            //    writetext.WriteLine("responseData : " + responseData);
            //    writetext.WriteLine("----------------------------------- Payment responseData -----------------");
            //}

        }


        /// <summary>
        ///   التحقق من العميلة 
        /// </summary>
        [Route("PaymentStatus")]
        [HttpGet]
        //[AllowAnonymous]
        [ResponseType(typeof(PaymentModelResult))]
        public IHttpActionResult PaymentStatus(string checkout_id = null, string request_id = null)
        {
            //PaymentModelResult model = new PaymentModelResult();
            //model.status = 1;
            //model.checkout_id = checkout_id;
            //return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "", true, model));
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            ////["result"]["description"];
            //if (!request_id.HasValue)
            //    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات المرسلة  ", false, null));
            using (var db = new MhanaDbEntities())
            {
                var request = db.StdNewRequests.Where(w => w.txt_id == checkout_id).OrderByDescending(o=>o.Id).FirstOrDefault();
                if (request != null)
                {
                    string _user = User.Identity.GetUserId();
                    var responseData = statusrequest(request.txt_id);
                    double out_amount = 0;
                    PaymentModelResult model = new PaymentModelResult();
                    if (responseData != null)
                    {
                        foreach (var group in responseData)
                        {
                            if (group.Key == "ndc")
                            {
                                model.ndc = group.Value;
                                model.checkout_id = group.Value;
                            }
                            if (group.Key == "amount")
                            {
                                out_amount = Convert.ToDouble(group.Value);
                            }
                            if (group.Key == "result")
                            {
                                foreach (var group2 in responseData["result"])
                                {
                                    if (group2.Key == "description")
                                    {
                                        model.description = group2.Value;

                                    }
                                    if (group2.Key == "code")
                                    {
                                        model.code = group2.Value;
                                        Regex successPattern = new Regex(@"(000\.000\.|000\.100\.1|000\.[36])");
                                        Regex successManuelPattern = new Regex(@"(000\.400\.0[^3]|000\.400\.100)");
                                        Match match1 = successPattern.Match(group2.Value);
                                        Match match2 = successManuelPattern.Match(group2.Value);
                                        if (match1.Success || match2.Success)
                                        {
                                            model.status = 1;
                                            request.status = (int)Enums.Status.Visible;
                                            request.amount = (decimal)out_amount;
                                            request.payment_status = 1;
                                            db.Entry(request).State = EntityState.Modified;
                                            db.SaveChanges();
                                            Course c = new Course()
                                            {
                                                title = request.AspNetUser.fullname,
                                                allow_sound = true, // (request.live_type == "video" || request.live_type == "audio") ? true : false,
                                                allow_video = true, // request.live_type == "video" ? true : false,
                                                cdate = DateTime.Now,
                                                cost = request.amount.HasValue ? (double)request.amount : 0,
                                                details = request.details,
                                                lectures_count = request.period,
                                                period = request.period,
                                                start_date = request.start_date,
                                                start_time = request.start_date.HasValue ? request.start_date.Value.TimeOfDay : DateTime.Now.TimeOfDay,
                                                teacher_id = request.teacher_id,
                                                status = 1,
                                                teaching_mechanism = request.teaching_mechanism,
                                                total_std = 1,
                                                request_id = request.Id,
                                                is_test = request.is_test,
                                                material = request.material
                                            };
                                            var elemnt = db.Courses.Add(c);
                                            db.SaveChanges();
                                            Pocket _p = new Pocket()
                                            {
                                                action = 1,
                                                request_id = request.Id,
                                                cdate = DateTime.Now,
                                                status = 1,
                                                tr_id = model.ndc,
                                                txt_id = model.code,
                                                user_id = _user,
                                                amount = out_amount
                                            };
                                            db.Pockets.Add(_p);
                                            db.SaveChanges();






                                        }
                                        else
                                        {
                                            model.status = 0;
                                        }
                                    }


                                }
                            }

                        }
                        return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "", true, model));
                    }
                }
                else
                {
                    PaymentModelResult model1 = new PaymentModelResult();
                    model1.status = 1;
                    model1.checkout_id = checkout_id;
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "", true, model1));
                }
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات المرسلة  ", false, null));
            }

            //using (StreamWriter writetext = new StreamWriter(HttpContext.Current.Server.MapPath("~/write.txt"), true))
            //{

            //    foreach (var group in responseData)
            //    {
            //        writetext.WriteLine("Key: {0} Value: {1}", group.Key, group.Value);
            //    }

            //    writetext.WriteLine("----------------------------------- Payment responseData -----------------");

            //    foreach (var group in responseData["result"])
            //    {
            //        writetext.WriteLine("Key: {0} Value: {1}", group.Key, group.Value);
            //    }
            //    writetext.WriteLine("responseData : " + responseData);
            //    writetext.WriteLine("----------------------------------- Payment responseData -----------------");
            //}

        }
        public Dictionary<string, dynamic> myrequest(float? amount, int? request_id,int?gateway=0)
        {
            string email, firstname, surname, street1;
            using (var db = new MhanaDbEntities())
            {
                var _request = db.StdNewRequests.Where(w => w.Id == request_id).FirstOrDefault();
                email = _request.AspNetUser.Email;
                firstname = _request.AspNetUser.first_name?? _request.AspNetUser.fullname;
                surname = _request.AspNetUser.last_name ?? _request.AspNetUser.fullname;
                street1 = _request.AspNetUser.address ?? "unknown";
            }
            using (StreamWriter writetext = new StreamWriter(HttpContext.Current.Server.MapPath("~/write.txt"), true))
            {
                writetext.WriteLine("----------------------------------- Model2 " + DateTime.Now + " -----------------");
                writetext.WriteLine("Amount: {0}", String.Format("{0:0.00}", amount));
            }
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            Dictionary<string, dynamic> responseData;
            string data = ConfigurationManager.AppSettings["HyperPayEntityId"].ToString()+//  "entityId=8acda4cc77b5a3a30177ce2752532d43" +
                "&amount=" + String.Format("{0:0.00}", amount) +
                "&currency=SAR" +
                "&paymentType=DB" +
                "&notificationUrl=" + ConfigurationManager.AppSettings["HyperPayNotify"] + "/" + request_id +
                "&customer.email="  + email+
                "&billing.street1="+ street1+
                "&billing.city=" + street1 +
                "&billing.state=" + street1 +
                "&billing.country=SA" +
                "&billing.postcode=11543" +
                "&customer.givenName="+ firstname+
                "&customer.surname="+ surname+
                "&testMode=EXTERNAL" +
                "&merchantTransactionId=" + Guid.NewGuid();


            string url = " https://oppwa.com/v1/checkouts";//"https://test.oppwa.com/v1/checkouts";
            byte[] buffer = Encoding.ASCII.GetBytes(data);
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.Method = "POST";
            request.Headers["Authorization"] = ConfigurationManager.AppSettings["HyperPayAuthorization"];// "Bearer OGFjZGE0Y2M3N2I1YTNhMzAxNzdjZTI2ZTU1NzJkM2J8WGpicVRxWjRCaw==";
            request.ContentType = "application/x-www-form-urlencoded";
            Stream PostData = request.GetRequestStream();
            PostData.Write(buffer, 0, buffer.Length);
            PostData.Close();
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                var s = new System.Web.Script.Serialization.JavaScriptSerializer();
                responseData = s.Deserialize<Dictionary<string, dynamic>>(reader.ReadToEnd());
                reader.Close();
                dataStream.Close();
            }
            return responseData;
        }
        public Dictionary<string, dynamic> statusrequest(string checkout_id)
        {
            try
            {
                Dictionary<string, dynamic> responseData;
                string data = ConfigurationManager.AppSettings["HyperPayEntityId"].ToString();  //"entityId=8acda4cc77b5a3a30177ce2752532d43";
                string url = "https://oppwa.com/v1/checkouts" + checkout_id + "/payment?" + data;
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
                request.Method = "GET";
                request.Headers["Authorization"] = ConfigurationManager.AppSettings["HyperPayAuthorization"].ToString();//"Bearer OGFjZGE0Y2M3N2I1YTNhMzAxNzdjZTI2ZTU1NzJkM2J8WGpicVRxWjRCaw==";
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    Stream dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);
                    var s = new JavaScriptSerializer();
                    responseData = s.Deserialize<Dictionary<string, dynamic>>(reader.ReadToEnd());
                    reader.Close();
                    dataStream.Close();
                }
                return responseData;
            }catch(Exception ex)
            {
                Dictionary<string, dynamic> responseData2  = new Dictionary<string, dynamic>();
                return responseData2;
            }
        }
    }
}
