﻿using Mhana.Helper;
using Mhana.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.AspNet.Identity;
using System.Data.Entity;

namespace Mhana.Controllers
{
    [Authorize]
    [RoutePrefix("api/v1/Teacher")]
    public class TeacherController : ApiController
    {
        string URL = ConfigurationManager.AppSettings["URL"];
        string user_id = "";
        DateTime newD = DateTime.Now.AddHours(3);
        public TeacherController()
        {
            user_id = User.Identity.GetUserId();
        }
        /// <summary>
        /// عرض الشاشة الرئيسية عند المعلم
        /// </summary>
        /// <returns></returns>
        [Route("GetHome")]
        [ResponseType(typeof(HomeModel))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpGet]
        public IHttpActionResult GetHome()
        {
            var u_id = User.Identity.GetUserId();
            using (var db = new MhanaDbEntities())
            {
                var requsts = db.StdNewRequests.Where(w => w.teacher_id == u_id && w.status == 0).Select(s => new NewRequestListViewModel()
                {

                    id = s.Id,
                    cdate = DateTime.Now + "",
                    details = s.details,
                    material = s.material,
                    period = s.period,
                    start_date = s.start_date.HasValue ? s.start_date.Value.Year + "-" + s.start_date.Value.Month + "-" + s.start_date.Value.Day : "",
                    start_time = s.start_time.HasValue ? s.start_time.Value.Hours + ":" + s.start_time.Value.Minutes : "---",
                    status = s.status,
                    teacher_id = s.teacher_id,
                    student_name = (s.AspNetUser != null) ? (string.IsNullOrEmpty(s.AspNetUser.fullname) ? s.AspNetUser.first_name + " " + s.AspNetUser.last_name : s.AspNetUser.fullname) : "---",
                    student_id = s.AspNetUser != null ? s.AspNetUser.Id : "",
                    photo = URL + "/resources/users/" + (s.AspNetUser != null ? s.AspNetUser.photo : "default.png"),
                    teaching_mechanism = s.teaching_mechanism,
                    lat = s.lat,
                    lng = s.lng,
                    address_name = s.address_name,
                    address_vicinity = s.address_vicinity

                }).OrderByDescending(o => o.id).ToList();
                var times = db.Course_Times.Where(w => w.Course.teacher_id == u_id && w.sch_date.HasValue && w.sch_date.Value.Year == newD.Year && w.sch_date.Value.Month == newD.Month && w.sch_date.Value.Day == newD.Day).Select(s => new CTime() { course_id = s.course_id, id = s.id, status = s.status, sch_date = s.sch_date }).OrderBy(o => o.sch_date).ToList();
                var courses = db.Courses.Where(w => w.teacher_id == u_id).Select(s => new HomeCourses()
                {
                    id = s.Id,
                    title = s.title,
                    teaching_mechanism = s.teaching_mechanism,
                    education_level = "متوسط",
                    photo = URL + "/resources/users/" + (s.Std_Course.FirstOrDefault() != null ? s.Std_Course.FirstOrDefault().AspNetUser.photo : "default.png"),
                    start_time = s.start_date + "",
                    live_type = (s.allow_sound.HasValue && s.allow_video.HasValue && s.allow_sound.Value && s.allow_video.Value) ? "video" : "audio",
                    student_name = s.Std_Course.FirstOrDefault() != null ? string.IsNullOrEmpty(s.Std_Course.FirstOrDefault().AspNetUser.fullname) ? s.Std_Course.FirstOrDefault().AspNetUser.first_name + " " + s.Std_Course.FirstOrDefault().AspNetUser.last_name : s.Std_Course.FirstOrDefault().AspNetUser.fullname : "---",
                    live_url = s.course_live_url,
                    student_id = s.Std_Course.FirstOrDefault() != null ? s.Std_Course.FirstOrDefault().std_id : "",

                }).OrderByDescending(o => o.id).ToList();
                foreach (var item in courses)
                {
                    var sd = times.Where(w => w.course_id == item.id).FirstOrDefault();

                    item.start_date = sd != null ? string.Format("{0:g}", sd.sch_date) : "";
                }
                HomeModel Hm = new HomeModel() { upcoming_courses = courses, waiting_requsts = requsts };
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, null, true, Hm));
            }
        }

        /// <summary>
        /// عرض التقويم لدى المعلم
        /// </summary>
        /// <returns></returns>
        [Route("GetCalender")]
        [ResponseType(typeof(CalenderModel))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpGet]
        public IHttpActionResult GetCalender()
        {
            var u_id = User.Identity.GetUserId();
            using (var db = new MhanaDbEntities())
            {
                var times = db.Course_Times.Where(w => w.Course.teacher_id == u_id && w.sch_date.HasValue).Select(s => new CTime() { course_id = s.course_id, id = s.id, status = s.status, sch_date = s.sch_date }).OrderBy(o => o.sch_date).ToList();
                var courses = db.Courses.Where(w => w.teacher_id == u_id).Select(s => new CalenderModel()
                {
                    id = s.Id,
                    title = s.title,
                    live_type = s.teaching_mechanism,
                    education_level = "متوسط",
                    time = s.start_time + "",
                    start_date = s.start_date,
                    photo = URL + "/resources/users/" + (s.AspNetUser != null ? s.AspNetUser.photo : "default.png"),
                    student_name = s.Std_Course.FirstOrDefault() != null ? s.Std_Course.FirstOrDefault().AspNetUser.first_name : "---",
                    student_id = s.AspNetUser != null ? s.AspNetUser.Id : "",
                }).OrderByDescending(o => o.id).ToList();
                foreach (var item in courses)
                {
                    var sd = times.Where(w => w.course_id == item.id).FirstOrDefault();
                    item.start_date = sd != null ? sd.sch_date : null;
                }
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, null, true, courses));
            }
        }

        /// <summary>
        /// شاشة عرض طلباتي لدى المعلم
        /// status == 0  طلب جديد
        /// status == 1 طلباتي التي تم الموافقة عليها
        /// status == 2 لديه عقد 
        /// </summary>
        /// <param name="rows"></param>
        /// <returns></returns>
        [Route("MyRequests")]
        [ResponseType(typeof(List<NewRequestList>))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpGet]
        public IHttpActionResult MyRequests(int? rows = 10)
        {
            var u_id = User.Identity.GetUserId();
            using (var db = new MhanaDbEntities())
            {

                var items = db.StdNewRequests.Where(w => w.teacher_id == u_id).Select(s => new NewRequestListViewModel()
                {
                    id = s.Id,
                    cdate = DateTime.Now + "",
                    details = s.details,
                    material = s.material,
                    period = s.period,
                    start_date = s.start_date.HasValue ? s.start_date.Value.Year + "-" + s.start_date.Value.Month + "-" + s.start_date.Value.Day : "",
                    start_time = s.start_time.HasValue ? s.start_time.Value.Hours + ":" + s.start_time.Value.Minutes : "---",
                    status = s.status,
                    teacher_id = s.teacher_id,
                    student_name = (s.AspNetUser != null) ? (string.IsNullOrEmpty(s.AspNetUser.fullname) ? s.AspNetUser.first_name + " " + s.AspNetUser.last_name : s.AspNetUser.fullname) : "",
                    student_id = s.AspNetUser != null ? s.AspNetUser.Id : "",
                    photo = URL + "/resources/users/" + (s.AspNetUser != null ? s.AspNetUser.photo : "default.png"),
                    teaching_mechanism = s.teaching_mechanism,
                    lat = s.lat,
                    lng = s.lng,
                    address_name = s.address_name,
                    address_vicinity = s.address_vicinity,
                    live_url = s.presenter_url

                }).OrderByDescending(o => o.id).ToList();
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "", true, items));
            }
        }

        /// <summary>
        /// شاشة عرض الدورات المسجل بها المعلم
        /// </summary>
        /// <param name="rows"></param>
        /// <returns></returns>
        [Route("MyCourses")]
        [ResponseType(typeof(List<CourseList>))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpGet]
        public IHttpActionResult MyCourses(string student_name = null, int? period = null, DateTime? reg_date = null, string teaching_mechanism = null, string material = null, int? rows = 10)
        {
            var u_id = User.Identity.GetUserId();
            using (var db = new MhanaDbEntities())
            {
                var times = db.Course_Times.Where(w => w.Course.teacher_id == u_id && w.sch_date.HasValue).Select(s => new CTime() { course_id = s.course_id, id = s.id, status = s.status, sch_date = s.sch_date }).OrderBy(o => o.sch_date).ToList();

                var mycourses = db.Courses.Where(w => w.teacher_id == u_id);
                if (!string.IsNullOrEmpty(student_name))
                    mycourses = mycourses.Where(w => w.Std_Course.FirstOrDefault() != null && w.Std_Course.FirstOrDefault().AspNetUser != null && !string.IsNullOrEmpty(w.Std_Course.FirstOrDefault().AspNetUser.fullname) && w.Std_Course.FirstOrDefault().AspNetUser.fullname.Contains(student_name));
                if (period.HasValue && period != 0)
                    mycourses = mycourses.Where(w => w.period.HasValue && w.period == period);
                if (reg_date.HasValue)
                    mycourses = mycourses.Where(w => w.cdate.HasValue && w.cdate.Value.Year == reg_date.Value.Year && w.cdate.Value.Month == reg_date.Value.Month);
                if (!string.IsNullOrEmpty(teaching_mechanism))
                    mycourses = mycourses.Where(w => !string.IsNullOrEmpty(w.teaching_mechanism) && w.teaching_mechanism == teaching_mechanism);
                if (!string.IsNullOrEmpty(material))
                    mycourses = mycourses.Where(w => !string.IsNullOrEmpty(w.material) && w.material == material);


                var items = mycourses.Select(s => new CourseList()
                {
                    id = s.Id,
                    title = s.title,
                    start_time = s.start_time,
                    status = s.status,
                    teacher_name = (s.AspNetUser != null) ? (string.IsNullOrEmpty(s.AspNetUser.fullname) ? s.AspNetUser.first_name + " " + s.AspNetUser.last_name : s.AspNetUser.fullname) : "",
                    photo = URL + "/resources/users/" + (s.Std_Course.FirstOrDefault() != null ? s.Std_Course.FirstOrDefault().AspNetUser.photo : "defalut.png"),
                    live_type = (s.allow_sound.HasValue && s.allow_video.HasValue && s.allow_sound.Value && s.allow_video.Value) ? "video" : "audio",
                    live_url = s.course_live_url,
                    student_name = s.Std_Course.FirstOrDefault() != null ? s.Std_Course.FirstOrDefault().AspNetUser.fullname : "---",
                    student_id = s.Std_Course.FirstOrDefault() != null ? s.Std_Course.FirstOrDefault().AspNetUser.Id : "---"//!string.IsNullOrEmpty(s.std_id_request) ? s.std_id_request : "---"


                }).OrderByDescending(o => o.id).ToList();
                if (items != null)
                {
                    foreach (var item in items)
                    {
                        var sd = times.Where(w => w.course_id == item.id).FirstOrDefault();
                        item.start_date = sd != null ? sd.sch_date : null;
                    }
                }

                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "", true, items));
            }
        }


        /// <summary>
        /// عرض العقد على المعلم 
        /// 0 jsut new request
        /// 1 accepted from teacher
        /// 2 send contract to teacher 
        /// 3 teacher accept contract
        /// -1 teacher reject contrat
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("GetContract")]
        [ResponseType(typeof(NewRequestList))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpGet]
        public IHttpActionResult GetContract(int? request_id)
        {
            using (var db = new MhanaDbEntities())
            {

                var contract = db.StdNewRequests.Where(w => w.Id == request_id).FirstOrDefault();
                if (contract != null)
                {
                    NewRequestListViewModel s = new NewRequestListViewModel()
                    {
                        id = contract.Id,
                        cdate = contract.cdate + "",
                        details = contract.details,
                        material = contract.material,
                        period = contract.period,
                        start_date = contract.start_date.HasValue ? contract.start_date.Value.Year + "-" + contract.start_date.Value.Month + "-" + contract.start_date.Value.Day : "",
                        start_time = contract.start_time.HasValue ? contract.start_time.Value.Hours + ":" + contract.start_time.Value.Minutes : "---",
                        status = contract.status,
                        teacher_id = contract.teacher_id,
                        student_name = (contract.AspNetUser != null) ? (string.IsNullOrEmpty(contract.AspNetUser.fullname) ? contract.AspNetUser.first_name + " " + contract.AspNetUser.last_name : contract.AspNetUser.fullname) : "",
                        student_id = contract.AspNetUser != null ? contract.AspNetUser.Id : "",
                        photo = URL + "/resources/users/" + (contract.AspNetUser != null ? contract.AspNetUser.photo : "default.png"),
                        lat = contract.lat,
                        lng = contract.lng,
                        address_name = contract.address_name,
                        address_vicinity = contract.address_vicinity,
                        teaching_mechanism = contract.teaching_mechanism,
                        live_url = contract.presenter_url
                    };
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, null, true, s));
                }
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));
            }
        }


        /// <summary>
        /// ارسال دعوة الى معلم معين 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("InviteTeacher")]
        [ResponseType(typeof(string))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpPost]
        public IHttpActionResult InviteTeacher(InviteStudent model)
        {
            using (var db = new MhanaDbEntities())
            {
                if (!model.course_id.HasValue || string.IsNullOrEmpty(model.email))
                {
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));

                }
                var mail = new emailInviteT()
                {
                    cdate = DateTime.Now + "",
                    from = User.Identity.Name,
                    to = model.email,
                    title = "دعوة الانضمام الى تطبيق درس خصوصي",
                    msg = "لقد تمت دعوتك للانضمام الى تطبيق درس خصوصي من قبل المعلم " + model.email + "يمكنك الآن الانضمام الى التطبيق وتسجيل بياناتك من خلال الروابط المرفقة الخاصة بتطبيق الأيفون والأندرويد"
                };

                new Mhana.Controllers.MailerController().InviteT(mail).Deliver();
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "تم ارسال طلبك بنجاح", true, null));
            }
        }

        /// <summary>
        /// طلب تمديد لقاء محدد 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("ExpandCourse")]
        [ResponseType(typeof(string))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpPost]
        public IHttpActionResult ExpandCourse(ExpandModel model)
        {
            using (var db = new MhanaDbEntities())
            {

                if (!model.course_id.HasValue || !model.period.HasValue)
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));
                var _course = db.Course_Times.Where(w => w.course_id == model.course_id && w.sch_date.Value.Year == DateTime.Now.Year && w.sch_date.Value.Month == DateTime.Now.Month && w.sch_date.Value.Day == DateTime.Now.Day).FirstOrDefault();
                if (_course == null)
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "ليس لديك لقاء فعال ليتم تمديده", false, null));
                if (_course != null && _course.Course.is_test == 1)
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هذا لقاء تجريبي ليس لديك الصلاحية لتمديد الوقت الخاص به", false, null));
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "تم ارسال طلبك بنجاح", true, null));
            }
        }

        /// <summary>
        /// شاشة عرض تفاصيل الكورس
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("CourseDetails")]
        [ResponseType(typeof(CourseDetails))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpGet]
        public IHttpActionResult CourseDetails(int? id)
        {
            using (var db = new MhanaDbEntities())
            {
                var item = db.Courses.Where(w => w.Id == id).FirstOrDefault();
                CourseDetails cd = new CourseDetails()
                {
                    id = item.Id,
                    cdate = item.cdate + "",
                    live_url = item.course_live_url,
                    photo = URL + "/resources/users/" + (item.AspNetUser != null && !string.IsNullOrEmpty(item.AspNetUser.photo) ? item.AspNetUser.photo : "default.png"),
                    remain_time = (DateTime.Now - item.start_date).Value.TotalMinutes + "",
                    status = item.status,
                    title = item.title,
                    teacher_name = (item.AspNetUser != null) ? (string.IsNullOrEmpty(item.AspNetUser.fullname) ? item.AspNetUser.first_name + " " + item.AspNetUser.last_name : item.AspNetUser.fullname) : "",
                    lat = item.lat,
                    lng = item.lng,
                    address_name = item.address_name,
                    address_vicinity = item.address_vicinity

                };
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "", true, cd));
            }
        }


        /// <summary>
        /// الموافقة/الرفض لطلب 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("AcceptRequest")]
        [ResponseType(typeof(string))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpPost]
        public IHttpActionResult AcceptRequest(AcceptanceRequset model)
        {
            var u_id = User.Identity.GetUserId();
            using (var db = new MhanaDbEntities())
            {

                if (!model.status.HasValue || !model.request_id.HasValue)
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));
                var requset = db.StdNewRequests.Where(w => w.Id == model.request_id).FirstOrDefault();
                if (requset != null && requset.teacher_id == u_id)
                {
                    requset.status = model.status;
                    db.Entry(requset).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Entry(requset).State = EntityState.Detached;
                    try
                    {
                        var fcm = db.AspNetUsers.Where(w => w.Id == requset.std_id).AsEnumerable().Select(s => s.fcm).ToList();
                        if (fcm.Count() > 0)
                        {
                            string msg = model.status == 1 ? "لقد تم الموافقة على طلبك من قبل المعلم يرجى قراءة العقد المرسل والموافقة عليه" : "لقد تم رفض طلبك من المعلم";
                            Functions.SendNotification(fcm, msg, "token", requset.Id, 1, requset.std_id);
                        }
                        var _teacher = db.AspNetUsers.FirstOrDefault(c => c.Id == u_id);
                        var _std = db.AspNetUsers.FirstOrDefault(c => c.Id == requset.std_id);
                        if (model.status == 1)
                        {
                            EmailContract mail = new EmailContract()
                            {
                                from = string.IsNullOrEmpty(_teacher.fullname) ? (_teacher.first_name + " " + _teacher.last_name) : _teacher.fullname,
                                material = requset.material,
                                details = requset.details,
                                period = requset.period == 1 ? "يومي" : (requset.period == 7 ? "اسبوعي" : "شهري"),
                                start_date = requset.start_date,
                                start_time = requset.start_time,
                                teaching_mechanism = requset.teaching_mechanism,
                                is_test = requset.is_test == 1 ? "يحتاج اتصال تجريبي " : "بدون اتصال تجريبي",
                                cost = requset.teaching_mechanism == "online" ? _teacher.online_cost : _teacher.site_cost,
                                title = "طلب درس خصوصي",
                                to = _std.Email,
                                msg = requset.teacher_notes,
                                cdate = string.Format("{0:MMM/dd/yyyy}", DateTime.Now),
                                status = 1

                            };
                            new Mhana.Controllers.MailerController().EmailRequest(mail).Deliver();
                        }

                        //   if (requset.teaching_mechanism == "online")
                        //   {

                        string atten_url = "";
                        string wiziqId = Functions.CreateWiziqTest(requset);
                        using (System.IO.StreamWriter file = new System.IO.StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/debug.txt"), true))
                            file.WriteLine("wiziqId : " + wiziqId);
                        if (!string.IsNullOrEmpty(wiziqId))
                        {
                            atten_url = Functions.AddStudentToWiziqTest(requset, wiziqId);
                            using (System.IO.StreamWriter file = new System.IO.StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/debug.txt"), true))
                                file.WriteLine("atten_url : " + atten_url);
                        }
                        else
                        {
                           // return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));
                        }
                        
                    }
                    catch (Exception ex)
                    {

                    }
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "تم ارسال طلبك بنجاح", true, null));
                }
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));

            }
        }


        /// <summary>
        /// تقييم معلم ل طالب   
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("Rate_Student")]
        [ResponseType(typeof(string))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpPost]
        public IHttpActionResult Rate_Student(RateModel model)
        {
            var u_id = User.Identity.GetUserId();
            using (var db = new MhanaDbEntities())
            {

                if (!model.rate.HasValue || string.IsNullOrEmpty(model.std_id))
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));
                var requset = db.MyRates.Where(w => w.std_id == model.std_id && w.teacher_id == u_id && w.rate_type == 1).FirstOrDefault();
                if (requset != null)
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "لقد قمت بتقييم هذا الطالب مسبقاً", false, null));
                MyRate mr = new MyRate()
                {
                    cdate = DateTime.Now,
                    comment = model.comment,
                    course_id = model.course_id,
                    rate = model.rate,
                    rate_type = 1,
                    std_id = model.std_id,
                    teacher_id = u_id

                };
                db.MyRates.Add(mr);
                db.SaveChanges();
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "شكراً لك تم ارسال تقييم بنجاح", true, null));
            }
        }


        /// <summary>
        /// عرض تقييمات الطلاب للمعلم       
        /// </summary>
        /// <param name="rows"></param>
        /// <returns></returns>
        [Route("TeacherRate")]
        [ResponseType(typeof(List<RateModel>))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpGet]
        public IHttpActionResult TeacherRate(int? rows = 10)
        {
            var u_id = User.Identity.GetUserId();
            using (var db = new MhanaDbEntities())
            {

                var items = db.MyRates.Where(w => w.teacher_id == u_id && w.rate_type == 2).Select(s => new RateModel()
                {
                    id = s.id,
                    cdate = DateTime.Now,
                    comment = s.comment,
                    course_title = s.Course.title,
                    rate = s.rate,
                    course_id = s.course_id,
                    std_id = s.std_id,
                    student_name = string.IsNullOrEmpty(s.AspNetUser.fullname) ? (s.AspNetUser.first_name + " " + s.AspNetUser.last_name) : s.AspNetUser.fullname,
                    teacher_name = string.IsNullOrEmpty(s.AspNetUser1.fullname) ? (s.AspNetUser1.first_name + " " + s.AspNetUser1.last_name) : s.AspNetUser1.fullname,
                    online = s.AspNetUser1.online.HasValue ? s.AspNetUser1.online : false

                }).OrderByDescending(o => o.id).ToList();
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "", true, items));
            }
        }


        /// <summary>
        /// عرض تقارير طالب محدد       
        /// </summary>
        /// <param name="rows"></param>
        /// <returns></returns>
        [Route("GetStudentReport")]
        [ResponseType(typeof(List<RateModel>))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpGet]
        public IHttpActionResult GetStudentReport(string std_id = null)
        {
            if (string.IsNullOrEmpty(std_id))
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "لا يوجد تقارير سابقة لعرضها", false, null));
            using (var db = new MhanaDbEntities())
            {
                var items = db.MyRates.Where(w => w.std_id == std_id && w.rate_type == 1 && !string.IsNullOrEmpty(w.report)).Select(s => new RateModel()
                {
                    id = s.id,
                    cdate = DateTime.Now,
                    course_title = s.Course != null ? s.Course.title : "---",
                    rate = s.rate,
                    course_id = s.course_id,
                    std_id = s.std_id,
                    student_name = s.AspNetUser != null ? string.IsNullOrEmpty(s.AspNetUser.fullname) ? (s.AspNetUser.first_name + " " + s.AspNetUser.last_name) : s.AspNetUser.fullname : "---",
                    teacher_name = s.AspNetUser1 != null ? string.IsNullOrEmpty(s.AspNetUser1.fullname) ? (s.AspNetUser1.first_name + " " + s.AspNetUser1.last_name) : s.AspNetUser1.fullname : "---",
                    photo = URL + "/resources/users/" + (s.AspNetUser != null && !string.IsNullOrEmpty(s.AspNetUser.photo) ? s.AspNetUser.photo : "default.png"),
                    comment = s.report

                }).OrderByDescending(o => o.id).ToList();
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "", true, items));
            }

        }

        /// <summary>
        /// الحصول على ملفات كورس محدد للطالب / المعلم       
        /// </summary>
        /// <param name="rows"></param>
        /// <returns></returns>
        [Route("CourseFiles")]
        [ResponseType(typeof(List<CourseFiles>))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpGet]
        public IHttpActionResult CourseFiles(int? course_id, int? rows = 10)
        {
            var u_id = User.Identity.GetUserId();
            using (var db = new MhanaDbEntities())
            {
                List<CourseFiles> _list = new List<CourseFiles>();
                CourseFiles cf = new CourseFiles() { cdate = string.Format("{0:MM/dd/yyyy}", DateTime.Now), extension = "png", id = 1, size = "2MB", title = "ملف مرفق 1", url = URL + "/resources/files/" + "1.png" };
                CourseFiles cf1 = new CourseFiles() { cdate = string.Format("{0:MM/dd/yyyy}", DateTime.Now), extension = "pdf", id = 1, size = "5MB", title = "ملف مرفق 2", url = URL + "/resources/files/" + "1.pdf" };
                CourseFiles cf2 = new CourseFiles() { cdate = string.Format("{0:MM/dd/yyyy}", DateTime.Now), extension = "jpg", id = 1, size = "2MB", title = "ملف مرفق 3", url = URL + "/resources/files/" + "1.jpg" };
                CourseFiles cf4 = new CourseFiles() { cdate = string.Format("{0:MM/dd/yyyy}", DateTime.Now), extension = "gif", id = 1, size = "1MB", title = "ملف مرفق 5", url = URL + "/resources/files/" + "1.gif" };
                _list.Add(cf);
                _list.Add(cf1);
                _list.Add(cf2);
                _list.Add(cf4);
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "", true, _list));
            }
        }

        /// <summary>
        /// عرض تقييمات طالب محدد        
        /// </summary>
        /// <param name="rows"></param>
        /// <returns></returns>
        [Route("ShowStudentRates")]
        [ResponseType(typeof(List<RateModel>))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpGet]
        public IHttpActionResult ShowStudentRates(string id, int? rows = 10)
        {
            if (string.IsNullOrEmpty(id))
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));
            var u_id = User.Identity.GetUserId();
            using (var db = new MhanaDbEntities())
            {

                var items = db.MyRates.Where(w => w.std_id == id && w.rate_type == 1).Select(s => new RateModel()
                {
                    id = s.id,
                    cdate = DateTime.Now,
                    comment = s.comment,
                    course_title = s.Course.title,
                    rate = s.rate,
                    course_id = s.course_id,
                    std_id = s.std_id,
                    student_name = s.AspNetUser != null ? string.IsNullOrEmpty(s.AspNetUser.fullname) ? (s.AspNetUser.first_name + " " + s.AspNetUser.last_name) : s.AspNetUser.fullname : "---",
                    teacher_name = s.AspNetUser1 != null ? string.IsNullOrEmpty(s.AspNetUser1.fullname) ? (s.AspNetUser1.first_name + " " + s.AspNetUser1.last_name) : s.AspNetUser1.fullname : "---",
                    photo = URL + "/resources/users/" + (s.AspNetUser != null && !string.IsNullOrEmpty(s.AspNetUser.photo) ? s.AspNetUser.photo : "default.png"),
                }).OrderByDescending(o => o.id).ToList();
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "", true, items));
            }
        }


        /// <summary>
        /// عرض بيانات الطالب للمعلم 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("GetStudentDetails")]
        [ResponseType(typeof(TeacherDetailsResponse))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpGet]
        public IHttpActionResult GetStudentDetails(string id, int? course_id, int? request_id)
        {
            using (var db = new MhanaDbEntities())
            {
                if (string.IsNullOrEmpty(id))
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));

                var item = db.AspNetUsers.Where(w => w.Id == id && w.status == 1).FirstOrDefault();
                if (item == null)
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هذا المستخدم غير فعال لدينا", false, null));
                string material = "";
                string period = "";
                string teaching_mechanism = "";
                if (request_id.HasValue && request_id != 0)
                {
                    var _course = db.StdNewRequests.Where(w => w.Id == request_id).FirstOrDefault();
                    material = _course.material;
                    period = _course.period + "";
                    teaching_mechanism = _course.teaching_mechanism;
                }
                if (course_id.HasValue && course_id != 0)
                {
                    var _course = db.Std_Course.Where(w => w.Id == course_id).FirstOrDefault();
                    material = _course.Course.material;
                    period = _course.Course.period + "";
                    teaching_mechanism = _course.Course.teaching_mechanism;
                }
                var edu_leve = db.education_level.Where(w => w.Id + "" == item.education_level_text).FirstOrDefault();
                TeacherDetailsResponse t = new TeacherDetailsResponse()
                {
                    id = item.Id,
                    fullname = (string.IsNullOrEmpty(item.fullname) ? item.first_name + " " + item.last_name : item.fullname),
                    gender = item.gender,
                    details = item.details,
                    email = item.Email,
                    teaching_mechanism = teaching_mechanism,
                    certificate = item.certificate,
                    online_cost = string.IsNullOrEmpty(item.online_cost) ? 0 : Convert.ToDouble(item.online_cost),
                    site_cost = string.IsNullOrEmpty(item.site_cost) ? 0 : Convert.ToDouble(item.site_cost),
                    specialization = item.specialization1 != null ? item.specialization1.name : "---",
                    branch_specialization = item.branch_specialization1 != null ? item.branch_specialization1.name : "---",
                    total_course = 1,
                    rating = 4,
                    cdate = string.Format("{0:MMM/dd/yyyy}", item.cdate),
                    photo = URL + "/resources/users/" + (string.IsNullOrEmpty(item.photo) ? "default.png" : item.photo),
                    country = item.country,
                    education_level = edu_leve != null ? edu_leve.name : "",
                    period = period,
                    material = material
                };

                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, null, true, t));
            }
        }

        /// <summary>
        /// عرض مدفوعات المعلم 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("GetMyPocket")]
        [ResponseType(typeof(List<ViewPocketModel>))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpGet]
        public IHttpActionResult GetMyPocket()
        {
            using (var db = new MhanaDbEntities())
            {

                List<ViewPocketModel> model = new List<ViewPocketModel>();
                model.Add(new ViewPocketModel()
                {
                    course_id = 2173,
                    course_name = "دورة الطالبة نادين ",
                    fee = 15,
                    single_amount = 20,
                    std_count = 2,
                    total_amount = 25,
                    id = 1,
                    status = "تم التحصيل"
                });
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, null, true, model));
            }
        }

        /// <summary>
        /// اضافة موعد لقاء 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("SetTime")]
        [ResponseType(typeof(string))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpPost]
        public IHttpActionResult SetTime(addTimeCourse model)
        {
            try
            {
                var user_id = User.Identity.GetUserId();
                using (var db = new MhanaDbEntities())
                {
                    var _course = db.Courses.FirstOrDefault(f => f.Id == model.id);
                    if(_course ==null)
                        return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هذه الدورة غير موجود أو غير تابعة لك", false, null));

                    DateTime _d = new DateTime(model.start_date.Value.Year, model.start_date.Value.Month, model.start_date.Value.Day, model.start_time.Value.Hours, model.start_time.Value.Minutes, 0);
                    Course_Times ct = new Course_Times()
                    {
                        course_id = model.id,
                        sch_date = _d,
                        sch_time = model.start_time,
                    };
                    db.Course_Times.Add(ct);
                    db.SaveChanges();
                    db.Entry(ct).State = EntityState.Detached;
                    db.Entry(_course).State = EntityState.Detached;
                    string atten_url = "";
                    string wiziqId = Functions.CreateWiziq(_course, ct);
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/debug.txt"), true))
                        file.WriteLine("wiziqId : " + wiziqId);
                    if (!string.IsNullOrEmpty(wiziqId))
                    {
                        atten_url = Functions.AddStudentToWiziq(_course, wiziqId);
                        using (System.IO.StreamWriter file = new System.IO.StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/debug.txt"), true))
                            file.WriteLine("atten_url : " + atten_url);

                        return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "تمت العملية بنجاح", true, null));
                    }
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات المدخلة", false, null));
                }
            }catch(Exception ex)
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/debug.txt"), true))
                    file.WriteLine("ex : " + ex.ToString());
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات المدخلة", false, null));

            }
        }
    }
}
