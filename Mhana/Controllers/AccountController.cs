﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Mhana.Models;
using Mhana.Providers;
using Mhana.Results;
using System.IO;
using System.Web.Hosting;
using System.Web.Http.Description;
using Mhana.Helper;
using System.Net;
using System.Linq;
using System.Data.Entity;
using System.Configuration;


namespace Mhana.Controllers
{
    [Authorize]
    [RoutePrefix("api/v1/Account")]
    public class AccountController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        string URL = ConfigurationManager.AppSettings["URL"];
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        // GET api/Account/UserInfo
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("UserInfo")]
        public UserInfoViewModel GetUserInfo()
        {
            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            return new UserInfoViewModel
            {
                Email = User.Identity.GetUserName(),
                HasRegistered = externalLogin == null,
                LoginProvider = externalLogin != null ? externalLogin.LoginProvider : null
            };
        }

        /// <summary>
        /// تسحيل الخروج   .
        /// </summary>
        [Route("Logout")]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }       
        /// <summary>
        /// تغيير كلمة المرور لمستخدم مسجل  .
        /// </summary>
        [ResponseType(typeof(string))]
        [ResponseCodes(HttpStatusCode.OK)]
        // POST api/Account/ChangePassword
        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في كلمة المرور يرجى التأكد منها", false, null));
            }

            IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword,model.NewPassword);

            if (!result.Succeeded)
            {
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في كلمة المرور يرجى التأكد منها", false, null));
            }

            return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "تم تغيير كلمة المرور بنجاح", true, null));
        }
        // POST api/Account/Register
        /// <summary>
        /// تسجيل مستخدم جديد   .
        /// </summary>
        [AllowAnonymous]
        [Route("Register")]
        [ResponseType(typeof(registerResponseModel))]
        public async Task<IHttpActionResult> Register(RegisterUserModel model)
        {
            try
            {
                if(string.IsNullOrEmpty(model.email) || string.IsNullOrEmpty(model.password) || string.IsNullOrEmpty(model.role))
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "يرجى ادخال بيانات صحيحة", false, null));
                if (string.IsNullOrEmpty(model.password) || model.password.Length < 6)
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "كلمة المرور 6 حروف على الأقل", false, null));
                if (model.password != model.confirm_password)
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "كلمة المرور غير متطابقة", false, null));
                var check_username = await UserManager.FindByNameAsync(model.email);
                if (check_username != null)
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "اسم المستخدم موجود مسبقاً يرجى ادخال اسم مستخدم جديد", false, null));
                var check_email = await UserManager.FindByEmailAsync(model.email);
                if (check_email != null)
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "البريد الالكتروني المستخدم موجود مسبقاً", false, null));
               
                var user = new ApplicationUser()
                {
                    UserName = model.email,
                    Email = model.email,
                    PhoneNumber = model.mobile,                   
                    status = model.role =="Student"?1:0,
                    last_login = DateTime.Now,
                    cdate = DateTime.Now,
                    allow_notify = false,   
                    is_complete = 0,
                    absher = 0,
                    fullname = model.fullname,
                    photo = "defaultm.jpg"
                 
                };
                IdentityResult result = await UserManager.CreateAsync(user, model.password);

                if (!result.Succeeded)
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "حدث خطأ في ادخال البيانات", false, result.Succeeded));

                await UserManager.AddToRoleAsync(user.Id, model.role);         
                string phoneToken = "";
                if (!string.IsNullOrEmpty(user.PhoneNumber))
                    phoneToken = UserManager.GenerateChangePhoneNumberToken(user.Id, user.PhoneNumber);
                SMS _sms = new SMS();
                string MESSAGE = "أهلاً بك في تطبيق المهنا.كود التفعيل هو  " + phoneToken;
                // string RECIEVER = "966" + user.PhoneNumber.Substring(1);
                // _sms.sendmessage(RECIEVER, MESSAGE);               
                string msg = "";
                if (model.role == "Student")
                    msg = "شكرأ لتسجيلك معنا في تطبيق درس خصوصي الآن يمكنك الاستفادة من التطبيق وتعزيز قدراتك العلمية من خلال مشاركتنا والاستفادة من خدماتنا ";
                else
                    msg = "شكرأ لتسجيلك معنا في تطبيق درس خصوصي  يرجى الدخول الى التطبيق لاكمال ملفك الشخصي حتى تتمكن الادارة من مراجعة ملفك الشخصي وتفعيل حسابك كمعلم  ";
                RegEmail mail = new RegEmail()
                {
                    firstname = model.email,
                    cdate = string.Format("{0:MM/dd/yyyy}", DateTime.Now),
                    message = msg,
                    type = model.role=="Student"?"طالب":"معلم",
                    to = model.email
                };
                new Mhana.Controllers.MailerController().RegEmail(mail).Deliver();
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, null, true,
                            new registerResponseModel()
                            {
                                expired = 14,
                                fullname =  user.fullname,
                                token = await GetToken(user),                               
                                email = user.Email,                               
                                allow_notify = user.allow_notify,                               
                                id = user.Id,                               
                                mobile = user.PhoneNumber,
                                role = model.role,
                                is_complete = false,
                                absher = user.absher.HasValue?user.absher:0,
                                online = true,
                                photo = URL + "/resources/users/" + user.photo
                            }));
            }
            catch (Exception ex)
            {
                return this.ResponseBadRequest(new ResponseViewModel(HttpStatusCode.OK, ex.ToString(), false, null));

            }
        }

        /// <summary>
        /// التحقق من كود التفعيل 
        /// هتلاقي كود التفعيل راجعلك مع نتائج عملية التسجيل في متغير اسمو phone_code
        /// </summary>       
        /// <returns></returns>       
        [Route("ConfirmCode")]
        [ResponseType(typeof(string))]
        public IHttpActionResult ConfirmCode(string code, string mobile = null)
        {
            string id = User.Identity.GetUserId();
            var _user = UserManager.FindById(id);
            var res = UserManager.VerifyChangePhoneNumberToken(id, code, string.IsNullOrEmpty(mobile) ? _user.PhoneNumber : mobile);
            if (res)
            {
                _user.status = 1;
                _user.PhoneNumberConfirmed = true;
                _user.PhoneNumber = string.IsNullOrEmpty(mobile) ? _user.PhoneNumber : mobile;
                UserManager.Update(_user);
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "تم تأكيد  رقم الجوال", true, null));
            }
            return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "كود التفعيل المستخدم خاطئ", false, null));
        }
        /// <summary>
        /// ارسال كود التفعيل       
        /// </summary>       
        /// <returns></returns>       
        [Route("SendCode")]
        [ResponseType(typeof(string))]
        public IHttpActionResult SendCode(string mobile = null)
        {
            string id = User.Identity.GetUserId();
            var _user = UserManager.FindById(id);
            var phoneToken = UserManager.GenerateChangePhoneNumberToken(_user.Id, string.IsNullOrEmpty(mobile) ? _user.PhoneNumber : mobile);
            string myMobile = string.IsNullOrEmpty(mobile) ? _user.PhoneNumber : mobile;
            SMS _sms = new SMS();
            string MESSAGE = "أهلاً بك في تطبيق المهنا.كود التفعيل هو  " + phoneToken;
            string RECIEVER = "966" + myMobile.Substring(1);
            _sms.sendmessage(RECIEVER, MESSAGE);
            // "يرجى ادخال كود التفعيل المرسل على جوالك "
            return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "يرجى ادخال كود التفعيل المرسل على جوالك ", true, null));

        }
        // POST api/Account/Login
        /// <summary>
        /// تسجيل الدخول الموحد  مع ملاجظة أن النتائج نفس النتائج الراجعة من التسجيل حسب نوع المستخدم  .
        /// </summary>
        [AllowAnonymous]
        [Route("Login")]
        // [ResponseType(typeof(registerResponseRecipient))]
        // [ResponseType(typeof(registerResponseAdvertiser))]
        public async Task<IHttpActionResult> Login(LoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "اسم المستخدم أو كلمة المرور غير صحيحة", false, null));
            }
            var checkAccount = await UserManager.FindByNameAsync(model.username);
            if (checkAccount == null)
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هذا الحساب غير مسجل لدينا ", false, null));

            var user = await UserManager.FindAsync(checkAccount.UserName, model.password);
            if (user != null)
            {
                if (user.status == -2 || user.status == -1)
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "تم ايقاف حسابك لمزيد من التفاصيل يرجى التواصل مع الادارة ", false, null));               
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, null, true,
                           new registerResponseModel()
                           {
                               expired = 14,
                               fullname = user.fullname,
                               token = await GetToken(user),
                               email = user.Email,
                               allow_notify = user.allow_notify,
                               id = user.Id,
                               mobile = user.PhoneNumber,
                               role = user.Roles.FirstOrDefault()!=null?( user.Roles.FirstOrDefault().RoleId=="2"?"Teacher":"Student"):"",
                               is_complete = user.is_complete==1?true:false,
                               absher = user.absher.HasValue ? user.absher : 0,                               
                               online = true,
                               photo = URL + "/resources/users/" + user.photo
                           }));
            }
            else
            {

                // config.SetActualResponseType(typeof(Core.Models.Policy),
                // "Policy", "Get");
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "اسم المستخدم أو كلمة المرور غير صحيحة", false, null));
            }


        }



        // POST api/Account/EditTeacher
        /// <summary>
        /// تعديل بيانات معلم  .
        /// </summary>  
        [Route("EditTeacher")]
        [ResponseType(typeof(Teacher))]
        [HttpPost]
        public async Task<IHttpActionResult> EditTeacher(Teacher model)
        {
            try
            {
                List<TeacherTimes> _teacherTimes = new List<TeacherTimes>();             
                keyValue _spe = new keyValue();
                keyValue _branch = new keyValue();

                string user_id = User.Identity.GetUserId();
                if (!string.IsNullOrEmpty(model.photo))
                {
                    var ph = Functions.UploadB64File(model.photo, "~/resources/users");
                    model.photo = ph;
                }
                if (!string.IsNullOrEmpty(model.certificate_photo))
                {

                   var ph2 =   Functions.UploadB64File(model.certificate_photo, "~/resources/files");
                    model.certificate_photo = ph2;
                }
                var user = await UserManager.FindByIdAsync(user_id);
                if (user != null)
                {
                   
                   

                    user.first_name = string.IsNullOrEmpty(model.first_name) ? user.first_name : model.first_name;
                    user.last_name = string.IsNullOrEmpty(model.last_name) ? user.last_name : model.last_name;
                    user.fullname = user.first_name + " " + user.last_name;
                    user.gender = string.IsNullOrEmpty(model.gender) ? user.gender : model.gender;                    
                    user.PhoneNumber = string.IsNullOrEmpty(model.mobile) ? user.PhoneNumber : model.mobile;
                    user.city  = string.IsNullOrEmpty(model.city) ? user.city : model.city;
                    user.region  = string.IsNullOrEmpty(model.region) ? user.region : model.region;

                    if (string.IsNullOrEmpty(model.photo) && string.IsNullOrEmpty(user.photo))
                        model.photo = (user.gender == "ذكر" ? "defaultm.jpg" : "defaultf.jpg");

                    user.photo = string.IsNullOrEmpty(model.photo) ? user.photo : model.photo;
                    user.country = string.IsNullOrEmpty(model.country) ? user.country : model.country;
                    user.certificate = string.IsNullOrEmpty(model.certificate) ? user.certificate : model.certificate; 
                    user.dob = !model.dob.HasValue ? user.dob : model.dob;
                    user.education_level_text = (model.education_level != null && model.education_level.Count() > 0) ? string.Join(",", model.education_level) : user.education_level_text;
                    user.details = string.IsNullOrEmpty(model.details) ? user.details : model.details;
                    user.university = string.IsNullOrEmpty(model.university) ? user.university : model.university;
                    user.teaching_mechanism = string.IsNullOrEmpty(model.teaching_mechanism) ? user.teaching_mechanism : model.teaching_mechanism;
                    user.specialization = (!model.specialization.HasValue || model.specialization==0) ? user.specialization : model.specialization;
                    user.branch_specialization = (!model.branch_specialization.HasValue || model.branch_specialization == 0) ? user.branch_specialization : model.branch_specialization;
                    user.is_complete = 1;
                    user.online_cost = model.online_cost.HasValue ? model.online_cost + "" : "0";
                    user.site_cost = model.site_cost.HasValue ? model.site_cost + "" : "0";
                    user.absher_no = string.IsNullOrEmpty(model.nid) ? user.absher_no : model.nid;
                    user.certificate_photo = model.certificate_photo;
                    var Updated = UserManager.Update(user);
                    List<Teacher_Times> TT = new List<Teacher_Times>();
                    List<string> el = new List<string>();                   
                    if (Updated.Succeeded)
                    {
                        if (model.teacher_times.Count() > 0) {
                            
                            using (var db = new MhanaDbEntities())
                            {
                                var removed = db.Teacher_Times.Where(w => w.user_id == user_id).ToList();
                                foreach (var item in removed)
                                {
                                    db.Teacher_Times.Remove(item);
                                }
                                foreach(var item in model.teacher_times)
                                {
                                    var _ct = new Teacher_Times() { day_number = item.day_number, from_time = item.from, to_time = item.to, user_id = user_id };
                                    db.Teacher_Times.Add(_ct);
                                    TT.Add(_ct);
                                }
                                db.SaveChanges();
                            }
                        }
                        using (var db = new MhanaDbEntities())
                        {
                            _teacherTimes = db.Teacher_Times.Where(w => w.user_id == user_id).Select(s => new TeacherTimes { day_number = s.day_number, from = s.from_time, to = s.to_time }).ToList();
                            _spe = db.specializations.Where(w => w.Id == user.specialization).Select(s => new keyValue() { id = s.Id, value = s.name }).FirstOrDefault();
                            _branch = db.branch_specialization.Where(w => w.Id == user.branch_specialization).Select(s => new keyValue() { id = s.Id, value = s.name }).FirstOrDefault();
                        }
                        TeacherRegisterResponseModel xx = new TeacherRegisterResponseModel()
                        {
                            branch_specialization = user.branch_specialization,
                            certificate = user.certificate,
                            specialization = user.specialization,
                            mobile = user.PhoneNumber,
                            first_name = user.first_name,
                            last_name = user.last_name,
                            id = user.Id,
                            certificate_photo = URL + "/resources/files/" + user.certificate_photo,
                            city = user.city,
                            region = user.region,
                            country = user.country,
                            details = user.details,
                            dob = string.Format("{0:MM/dd/yyyy}", user.dob),
                            email = user.Email,
                            expired = 14,
                            fcm = user.fcm,
                            fullname = user.fullname,
                            gender = user.gender,
                            photo = URL + "/resources/users/" + user.photo,
                            online_cost = string.IsNullOrEmpty(user.online_cost)? 0:Convert.ToDouble(user.online_cost),
                            site_cost = string.IsNullOrEmpty(user.site_cost) ? 0 : Convert.ToDouble(user.site_cost),
                            status = user.status,
                            teaching_mechanism = user.teaching_mechanism,
                            university = user.university,
                            token = await GetToken(user),
                            teacher_times = _teacherTimes,
                            education_level = !string.IsNullOrEmpty(user.education_level_text) ?user.education_level_text.Split(',').ToList(): el,
                            is_complete = user.is_complete == 1 ? true : false,
                            absher = user.absher.HasValue?user.absher: 0 ,
                            branch_specialization_obj = _branch,
                            specialization_obj = _spe,
                            nid = user.absher_no
                             
                        };
                        return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "تمت العملية بنجاح", true, xx));

                    }
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));
                }
                else
                {
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));
                }
            }
            catch (Exception ex)
            {
                using (StreamWriter writetext = new StreamWriter(HttpContext.Current.Server.MapPath("~/Debug.txt"), true))
                {
                    writetext.WriteLine(model.specialization);
                }
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, ex.ToString(), false, null));
            }
        }


        // POST api/Account/GetTeacherInfo
        /// <summary>
        /// تعديل بيانات معلم  .
        /// </summary>  
        [Route("GetTeacherInfo")]
        [ResponseType(typeof(Teacher))]
        [HttpGet]
        public async Task<IHttpActionResult> GetTeacherInfo()
        {
            try
            {               
                string user_id = User.Identity.GetUserId();                              
                var user = await UserManager.FindByIdAsync(user_id);
                List<TeacherTimes> _teacherTimes = new List<TeacherTimes>();
                List<string> el = new List<string>();
                keyValue _spe = new keyValue();
                keyValue _branch = new keyValue();
                if (user != null)
                {                   
                       using (var db = new MhanaDbEntities())
                       {
                        _teacherTimes = db.Teacher_Times.Where(w => w.user_id == user_id).Select(s=>new TeacherTimes { day_number = s.day_number,from=s.from_time,to=s.to_time }).ToList();
                        _spe = db.specializations.Where(w => w.Id == user.specialization).Select(s => new keyValue() { id = s.Id, value = s.name }).FirstOrDefault();
                        _branch = db.branch_specialization.Where(w => w.Id == user.branch_specialization).Select(s => new keyValue() { id = s.Id, value = s.name }).FirstOrDefault();
                    }
                        TeacherRegisterResponseModel xx = new TeacherRegisterResponseModel()
                        {
                            branch_specialization = user.branch_specialization,// user.branch_specialization,
                            certificate = user.certificate,
                            specialization = user.specialization,
                            mobile = user.PhoneNumber,
                            first_name = user.first_name,
                            last_name = user.last_name,
                            id = user.Id,
                            certificate_photo = URL + "/resources/files/" + user.certificate_photo,
                            city = user.city,
                            region = user.region,
                            country = user.country,
                            details = user.details,
                            dob = string.Format("{0:MM/dd/yyyy}", user.dob),
                            email = user.Email,
                            expired = 14,
                            fcm = user.fcm,
                            fullname = user.fullname,
                            gender = user.gender,
                            photo = URL + "/resources/users/" + user.photo,
                            online_cost = string.IsNullOrEmpty(user.online_cost) ? 0 : Convert.ToDouble(user.online_cost),
                            site_cost = string.IsNullOrEmpty(user.site_cost) ? 0 : Convert.ToDouble(user.site_cost),
                            status = user.status,
                            teaching_mechanism = user.teaching_mechanism,
                            university = user.university,
                            teacher_times = _teacherTimes,
                            token = await GetToken(user),
                            education_level = !string.IsNullOrEmpty(user.education_level_text) ? user.education_level_text.Split(',').ToList() : el,
                            is_complete = user.is_complete == 1 ? true : false,
                            branch_specialization_obj = _branch,
                            specialization_obj = _spe,
                            nid = user.absher_no
                };
                        return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK,null, true, xx));

                    }                 
                else
                {
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));
                }
            }
            catch (Exception ex)
            {
                using (StreamWriter writetext = new StreamWriter(HttpContext.Current.Server.MapPath("~/Debug.txt"), true))
                {
                    writetext.WriteLine(ex.ToString());
                }
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, ex.ToString(), false, null));
            }
        }



        // POST api/Account/EditStudent
        /// <summary>
        /// تعديل بيانات طالب  .
        /// </summary>  
        [Route("EditStudent")]
        [ResponseType(typeof(Student))]
        [HttpPost]
        public async Task<IHttpActionResult> EditStudent(Student model)
        {
            try
            {

                string user_id = User.Identity.GetUserId();            
                if (!string.IsNullOrEmpty(model.photo))
                {
                    var ph = Functions.UploadB64File(model.photo, "~/resources/users");
                    model.photo = ph;
                }
                var user = await UserManager.FindByIdAsync(user_id);
                if (user != null)
                {
                    
                    user.fullname = string.IsNullOrEmpty(model.fullname) ? user.fullname : model.fullname;
                    user.first_name = string.IsNullOrEmpty(model.first_name) ? user.first_name : model.first_name;
                    user.last_name = string.IsNullOrEmpty(model.last_name) ? user.last_name : model.last_name;
                    user.gender = string.IsNullOrEmpty(model.gender) ? user.gender : model.gender;
                    user.PhoneNumber = string.IsNullOrEmpty(model.mobile) ? user.PhoneNumber : model.mobile;
                    user.city = string.IsNullOrEmpty(model.city) ? user.city : model.city;
                    user.country = string.IsNullOrEmpty(model.country) ? user.country : model.country;
                    user.region = string.IsNullOrEmpty(model.region) ? user.region : model.region;
                    if (string.IsNullOrEmpty(model.photo) && string.IsNullOrEmpty(user.photo))
                        model.photo = (user.gender == "ذكر" ? "defaultm.jpg" : "defaultf.jpg");
                    user.photo = string.IsNullOrEmpty(model.photo) ? user.photo : model.photo;
                    // user.photo = string.IsNullOrEmpty(model.photo) ? user.photo : model.photo; 
                    user.dob = !model.dob.HasValue ? user.dob : model.dob;
                    user.education_level_text = model.education_level.Count() > 0 ? string.Join(",", model.education_level) : user.education_level_text;                   
                    user.is_complete = 1;
                    var Updated = UserManager.Update(user);                   
                    List<string> el = new List<string>();
                    if (Updated.Succeeded)
                    {                       
                        StudentRegisterResponseModel xx = new StudentRegisterResponseModel()
                        {
                            
                            mobile = user.PhoneNumber,
                            first_name = user.first_name,
                            last_name = user.last_name,
                            id = user.Id,                            
                            city = user.city,
                            region = user.region,
                            country = user.country,
                            dob = string.Format("{0:MM/dd/yyyy}", user.dob),
                            email = user.Email,
                            expired = 14,
                            fcm = user.fcm,
                            fullname = user.fullname,
                            gender = user.gender,
                            photo = URL + "/resources/users/" + user.photo,                           
                            status = user.status,                          
                            token = await GetToken(user),                           
                            education_level = !string.IsNullOrEmpty(user.education_level_text) ? user.education_level_text.Split(',').ToList() : el,
                            is_complete = user.is_complete == 1 ? true : false
                        };
                        return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "تمت العملية بنجاح", true, xx));

                    }
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));
                }
                else
                {
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));
                }
            }
            catch (Exception ex)
            {
                
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, ex.ToString(), false, null));
            }
        }


        // POST api/Account/GetStudentInfo
        /// <summary>
        /// تعديل بيانات طالب  .
        /// </summary>  
        [Route("GetStudentInfo")]
        [ResponseType(typeof(Student))]
        [HttpGet]
        public async Task<IHttpActionResult> GetStudentInfo()
        {
            try
            {
                string user_id = User.Identity.GetUserId();
                var user = await UserManager.FindByIdAsync(user_id);                    
                List<string> el = new List<string>();
                if (user != null)
                {                   
                   
                    StudentRegisterResponseModel xx = new StudentRegisterResponseModel()
                    {                      
                        mobile = user.PhoneNumber,
                        first_name = user.first_name,
                        last_name = user.last_name,
                        id = user.Id,                        
                        city = user.city,
                        region = user.region,
                        country = user.country,
                        dob = string.Format("{0:MM/dd/yyyy}", user.dob),
                        email = user.Email,
                        expired = 14,
                        fcm = user.fcm,
                        fullname = string.IsNullOrEmpty(user.fullname)?user.first_name + " "+ user.last_name:user.fullname,
                        gender = user.gender,
                        photo = URL + "/resources/users/" + user.photo,                        
                        status = user.status,                        
                        token = await GetToken(user),
                        education_level =  !string.IsNullOrEmpty(user.education_level_text) ? user.education_level_text.Split(',').ToList() : el,
                        is_complete = user.is_complete == 1 ? true : false
                    };
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, null, true, xx));

                }
                else
                {
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));
                }
            }
            catch (Exception ex)
            {
               
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, ex.ToString(), false, null));
            }
        }



        // POST api/Account/ForgotPassword
        /// <summary>
        /// نسيت كلمة المرور   .
        /// </summary>   
        [HttpPost]
        [AllowAnonymous]
        [Route("ForgotPassword")]
        public async Task<IHttpActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
            {
                var user = await UserManager.FindByEmailAsync(model.Email);

                if (user == null)
                {
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هذا المستخدم غير مسجل لدينا", false, null));
                }
                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Link("Default", new { Controller = "Home", Action = "ResetPassword", id = user.Id, code = code });
                ChangeUserModel mail = new ChangeUserModel()
                {
                    name = string.IsNullOrEmpty(user.fullname)? user.first_name+" "+user.last_name: user.fullname,
                    url = callbackUrl,
                    to = model.Email
                };
                new Mhana.Controllers.MailerController().foregetPassword(mail).Deliver();
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "تم ارسال رسالة الى بريدك الالكتروني يرجى مراجعة الرسالة", true, null));
            }
            return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ يرجى التأكد من البيانات ", false, null));
            }
            catch (Exception ex)
            {
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, ex.ToString(), false, null));
            }

        }
        // POST api/Account/RegisterExternal
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("RegisterExternal")]
        public async Task<IHttpActionResult> RegisterExternal(RegisterExternalBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var info = await Authentication.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return InternalServerError();
            }

            var user = new ApplicationUser() { UserName = model.Email, Email = model.Email };

            IdentityResult result = await UserManager.CreateAsync(user);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            result = await UserManager.AddLoginAsync(user.Id, info.Login);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }
            return Ok();
        }


        /// <summary>
        ///   اضافة IBAN.
        /// </summary>
        [Route("SetIban")]
        [ResponseType(typeof(string))]
        [HttpPost]
        public IHttpActionResult SetIban(IBan model)
        {
            string _user = User.Identity.GetUserId();
            using (var db = new MhanaDbEntities())
            {

                var _bank = new Banks_Account()
                {
                    user_id = _user,
                    bank_account = model.iban_account,
                    bank_name = model.iban_bank,
                    bank_account_name = model.iban_name
                };
                db.Banks_Account.Add(_bank);
                db.SaveChanges();
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "تم بنجاح اضافة الحساب  الخاص بك", true, null));
            }

        }

        /// <summary>
        ///   تعديل IBAN.
        /// </summary>
        [Route("EditIban")]
        [ResponseType(typeof(string))]
        [HttpPost]
        public IHttpActionResult EditIban(IBanEdit model)
        {
            string _user = User.Identity.GetUserId();
            using (var db = new MhanaDbEntities())
            {
                var _bank = db.Banks_Account.Where(w => w.Id == model.id).FirstOrDefault();
                if (_bank != null && _bank.user_id == _user)
                {
                    _bank.bank_account = model.iban_account;
                    _bank.bank_name = model.iban_bank;
                    _bank.bank_account_name = model.iban_name;
                    db.Entry(_bank).State = EntityState.Modified;
                    db.SaveChanges();
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "تم بنجاح تعديل الحساب  الخاص بك", true, null));
                }
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));
            }

        }
        /// <summary>
        ///   اضافة FCM.
        /// </summary>
        [Route("AddFcm")]
        [ResponseType(typeof(string))]
        [HttpPost]
        public IHttpActionResult AddFcm(FCMToken model)
        {
            string _user = User.Identity.GetUserId();
            using (var db = new MhanaDbEntities())
            {

                var user = UserManager.FindById(_user);
                user.fcm = model.fcm;
                UserManager.Update(user);
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "تم بنجاح", true, null));
            }

        }

        /// <summary>
        ///   عرض IBANs.
        /// </summary>
        [Route("GetIbans")]
        [ResponseType(typeof(List<IBan>))]
        [HttpGet]
        public IHttpActionResult GetIbans()
        {
            string _user = User.Identity.GetUserId();
            using (var db = new MhanaDbEntities())
            {
                var ibans = db.Banks_Account.Where(w => w.user_id == _user).Select(s => new IBanEdit()
                {
                    iban_name = s.bank_account_name,
                    iban_bank = s.bank_name,
                    iban_account = s.bank_account,
                    id = s.Id

                }).ToList();

                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "", true, ibans));
            }

        }

        /// <summary>
        ///   عرض الاشعارات حسب اليوزر.
        /// </summary>
        [Route("GetNotifications")]
        [ResponseType(typeof(List<NotificationModel>))]
        [HttpGet]
        public IHttpActionResult GetNotifications(DateTime? last_date,int?rows)
        {
            string _user = User.Identity.GetUserId();
            using (var db = new MhanaDbEntities())
            {
                var noti = db.Notifications.Where(w=>w.user_id== _user && w.type!=0).Select(s => new NotificationModel()
                {
                     id = s.id,
                     cdate =  s.cdate,
                     title = s.details, 
                     details = s.details,
                     is_read = s.is_read,
                     type =s.type,
                     photo = "",
                     rel_id = s.rel_id
                }).OrderByDescending(o=>o.id).ToList();

                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "", true, noti));
            }

        }

        [Route("GetRequestDetails")]
        [ResponseType(typeof(NewRequestListViewModel))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpGet]
        public IHttpActionResult GetRequestDetails(string request_id)
        {
            using (var db = new MhanaDbEntities())
            {
               
                var contract = db.StdNewRequests.Where(w => w.Id+"" == request_id).FirstOrDefault();
                string photo = "";
                string live_url = "";
                if (User.IsInRole("Teacher"))
                {
                    photo = URL + "/resources/users/" + (contract.AspNetUser != null ? contract.AspNetUser.photo : "default.png");
                    live_url = contract.presenter_url;
                }
                else
                {
                    photo = URL + "/resources/users/" + (contract.AspNetUser1 != null ? contract.AspNetUser1.photo : "default.png");
                    live_url = contract.std_url;
                }

              
                if (contract != null)
                {
                    NewRequestListViewModel s = new NewRequestListViewModel()
                    {
                        id = contract.Id,
                        cdate = contract.cdate+"",
                        details = contract.details,
                        material = contract.material,
                        period = contract.period,                        
                        start_date = contract.start_date.HasValue ? contract.start_date.Value.Year + "-" + contract.start_date.Value.Month + "-" + contract.start_date.Value.Day : "",
                        start_time = contract.start_time.HasValue ? contract.start_time.Value.Hours + ":" + contract.start_time.Value.Minutes : "---",
                        status = contract.status,
                        teacher_id = contract.teacher_id,
                        student_name = contract.AspNetUser != null ? string.IsNullOrEmpty(contract.AspNetUser.fullname)? contract.AspNetUser.first_name +" "+ contract.AspNetUser.last_name : contract.AspNetUser.fullname : "---",
                        photo = photo,
                        teacher_name = contract.AspNetUser1 != null ? string.IsNullOrEmpty(contract.AspNetUser1.fullname) ? contract.AspNetUser1.first_name + " " + contract.AspNetUser1.last_name : contract.AspNetUser1.fullname : "---",
                        teaching_mechanism = contract.teaching_mechanism,
                        live_url = live_url, 
                        lat = contract.lat,
                        lng = contract.lng

                    };
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, null, true, s));
                }
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));
            }
        }
        /////// <summary>
        ///////   طلب سحب المبلغ المحصل.
        /////// </summary>
        //[Route("Withdraw")]
        //[ResponseType(typeof(string))]
        //[HttpPost]
        //public IHttpActionResult Withdraw(Withdraw model)
        //{
        //    string _user = User.Identity.GetUserId();
        //    var user = UserManager.FindById(_user);


        //    if (user.taks_income.HasValue || user.taks_income <= 0)
        //        return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "حسابك الحالي أقل من الحد المطلوب", false, null));
        //    using (var db = new AtnyDbEntities())
        //    {

        //        return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "تم بنجاح ارسال طلبك الى الادارة للنظر فيه سيتم الرد عليك بأقرب فرصة ", true, null));
        //    }

        //}



        /// <summary>
        ///   تعطيل وتفعيل الاشعارات.
        /// </summary>
        [Route("AllowNotify")]
        [ResponseType(typeof(string))]
        [HttpPost]
        public IHttpActionResult AllowNotify(NotifyModel model)
        {

            using (var db = new MhanaDbEntities())
            {
                string _user = User.Identity.GetUserId();
                var user = UserManager.FindById(_user);
                user.fcm = model.status == 0 ? "" : model.fcm;
                user.allow_notify = model.status == 1?true:false;
                UserManager.Update(user);
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "تمت العملية بنجاح", true, null));
            }

        }


        ///// <summary>
        /////   عرض الاشعارات.
        ///// </summary>
        //[Route("GetNotify")]
        //[ResponseType(typeof(List<NotifyListModel>))]
        //[HttpGet]
        //public IHttpActionResult GetNotify()
        //{
        //    string _user = User.Identity.GetUserId();
        //    using (var db = new AtnyDbEntities())
        //    {
        //        var items = db.User_Tr.Where(w => w.user_id == _user).Select(s => new NotifyListModel()
        //        {
        //            cdate = s.cdate,
        //            details = s.details,
        //            survey_id = s.survey_id,
        //            type = s.tr

        //        }).ToList();
        //        return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "تمت العملية بنجاح", true, items));
        //    }

        //}


        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        #region Helpers
        private bool IsBase64Encoded(String str)
        {
            try
            {
                // If no exception is caught, then it is possibly a base64 encoded string
                byte[] data = Convert.FromBase64String(str);
                // The part that checks if the string was properly padded to the
                // correct length was borrowed from d@anish's solution
                return (str.Replace(" ", "").Length % 4 == 0);
            }
            catch
            {
                // If exception is caught, then it is not a base64 encoded string
                return false;
            }
        }
        private string FixBase64ForImage(string Image)
        {
            System.Text.StringBuilder sbText = new System.Text.StringBuilder(Image, Image.Length);
            sbText.Replace("\r\n", String.Empty); sbText.Replace(" ", String.Empty);
            return sbText.ToString();
        }
        private async Task<string> GetToken(ApplicationUser user)
        {
            ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(UserManager,
                   OAuthDefaults.AuthenticationType);

            ClaimsIdentity cookiesIdentity = await user.GenerateUserIdentityAsync(UserManager,
            CookieAuthenticationDefaults.AuthenticationType);

            AuthenticationProperties properties = ApplicationOAuthProvider.CreateProperties(user.UserName);

            AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);

            ticket.Properties.ExpiresUtc = DateTime.Now.AddDays(1000);
            Authentication.SignIn(cookiesIdentity);
            ////
            //هادا التوكن  Startup.OAuthOptions.AccessTokenFormat.Protect(ticket)
            ///
            return Startup.OAuthOptions.AccessTokenFormat.Protect(ticket);



        }
        //private string FixBase64ForImage(string Image)
        //{
        //    System.Text.StringBuilder sbText = new System.Text.StringBuilder(Image, Image.Length);
        //    sbText.Replace("\r\n", String.Empty); sbText.Replace(" ", String.Empty);
        //    return sbText.ToString();
        //}
        private static void SaveFile(byte[] content, string path)
        {
            string filePath = GetFileFullPath(path);
            if (!Directory.Exists(Path.GetDirectoryName(filePath)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(filePath));
            }

            //Save file
            using (FileStream str = File.Create(filePath))
            {
                str.Write(content, 0, content.Length);
            }
        }
        private static string GetFileFullPath(string path)
        {
            string relName = path.StartsWith("~") ? path : path.StartsWith("/") ? string.Concat("~", path) : path;

            string filePath = relName.StartsWith("~") ? HostingEnvironment.MapPath(relName) : relName;

            return filePath;
        }
        //// POST api/Account/Logout
        //[Route("Logout")]
        //public IHttpActionResult Logout()
        //{
        //    Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
        //    return Ok();
        //}

        //// GET api/Account/ManageInfo?returnUrl=%2F&generateState=true
        //[Route("ManageInfo")]
        //public async Task<ManageInfoViewModel> GetManageInfo(string returnUrl, bool generateState = false)
        //{
        //    IdentityUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

        //    if (user == null)
        //    {
        //        return null;
        //    }

        //    List<UserLoginInfoViewModel> logins = new List<UserLoginInfoViewModel>();

        //    foreach (IdentityUserLogin linkedAccount in user.Logins)
        //    {
        //        logins.Add(new UserLoginInfoViewModel
        //        {
        //            LoginProvider = linkedAccount.LoginProvider,
        //            ProviderKey = linkedAccount.ProviderKey
        //        });
        //    }

        //    if (user.PasswordHash != null)
        //    {
        //        logins.Add(new UserLoginInfoViewModel
        //        {
        //            LoginProvider = LocalLoginProvider,
        //            ProviderKey = user.UserName,
        //        });
        //    }

        //    return new ManageInfoViewModel
        //    {
        //        LocalLoginProvider = LocalLoginProvider,
        //        Email = user.UserName,
        //        Logins = logins,
        //        ExternalLoginProviders = GetExternalLogins(returnUrl, generateState)
        //    };
        //}

        //// POST api/Account/ChangePassword
        //[Route("ChangePassword")]
        //public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword,
        //        model.NewPassword);

        //    if (!result.Succeeded)
        //    {
        //        return GetErrorResult(result);
        //    }

        //    return Ok();
        //}

        //// POST api/Account/SetPassword
        //[Route("SetPassword")]
        //public async Task<IHttpActionResult> SetPassword(SetPasswordBindingModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);

        //    if (!result.Succeeded)
        //    {
        //        return GetErrorResult(result);
        //    }

        //    return Ok();
        //}

        //// POST api/Account/AddExternalLogin
        //[Route("AddExternalLogin")]
        //public async Task<IHttpActionResult> AddExternalLogin(AddExternalLoginBindingModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

        //    AuthenticationTicket ticket = AccessTokenFormat.Unprotect(model.ExternalAccessToken);

        //    if (ticket == null || ticket.Identity == null || (ticket.Properties != null
        //        && ticket.Properties.ExpiresUtc.HasValue
        //        && ticket.Properties.ExpiresUtc.Value < DateTimeOffset.UtcNow))
        //    {
        //        return BadRequest("External login failure.");
        //    }

        //    ExternalLoginData externalData = ExternalLoginData.FromIdentity(ticket.Identity);

        //    if (externalData == null)
        //    {
        //        return BadRequest("The external login is already associated with an account.");
        //    }

        //    IdentityResult result = await UserManager.AddLoginAsync(User.Identity.GetUserId(),
        //        new UserLoginInfo(externalData.LoginProvider, externalData.ProviderKey));

        //    if (!result.Succeeded)
        //    {
        //        return GetErrorResult(result);
        //    }

        //    return Ok();
        //}

        //// POST api/Account/RemoveLogin
        //[Route("RemoveLogin")]
        //public async Task<IHttpActionResult> RemoveLogin(RemoveLoginBindingModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    IdentityResult result;

        //    if (model.LoginProvider == LocalLoginProvider)
        //    {
        //        result = await UserManager.RemovePasswordAsync(User.Identity.GetUserId());
        //    }
        //    else
        //    {
        //        result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(),
        //            new UserLoginInfo(model.LoginProvider, model.ProviderKey));
        //    }

        //    if (!result.Succeeded)
        //    {
        //        return GetErrorResult(result);
        //    }

        //    return Ok();
        //}

        //// GET api/Account/ExternalLogin
        //[OverrideAuthentication]
        //[HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        //[AllowAnonymous]
        //[Route("ExternalLogin", Name = "ExternalLogin")]
        //public async Task<IHttpActionResult> GetExternalLogin(string provider, string error = null)
        //{
        //    if (error != null)
        //    {
        //        return Redirect(Url.Content("~/") + "#error=" + Uri.EscapeDataString(error));
        //    }

        //    if (!User.Identity.IsAuthenticated)
        //    {
        //        return new ChallengeResult(provider, this);
        //    }

        //    ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

        //    if (externalLogin == null)
        //    {
        //        return InternalServerError();
        //    }

        //    if (externalLogin.LoginProvider != provider)
        //    {
        //        Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
        //        return new ChallengeResult(provider, this);
        //    }

        //    ApplicationUser user = await UserManager.FindAsync(new UserLoginInfo(externalLogin.LoginProvider,
        //        externalLogin.ProviderKey));

        //    bool hasRegistered = user != null;

        //    if (hasRegistered)
        //    {
        //        Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

        //         ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(UserManager,
        //            OAuthDefaults.AuthenticationType);
        //        ClaimsIdentity cookieIdentity = await user.GenerateUserIdentityAsync(UserManager,
        //            CookieAuthenticationDefaults.AuthenticationType);

        //        AuthenticationProperties properties = ApplicationOAuthProvider.CreateProperties(user.UserName);
        //        Authentication.SignIn(properties, oAuthIdentity, cookieIdentity);
        //    }
        //    else
        //    {
        //        IEnumerable<Claim> claims = externalLogin.GetClaims();
        //        ClaimsIdentity identity = new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType);
        //        Authentication.SignIn(identity);
        //    }

        //    return Ok();
        //}

        //// GET api/Account/ExternalLogins?returnUrl=%2F&generateState=true
        //[AllowAnonymous]
        //[Route("ExternalLogins")]
        //public IEnumerable<ExternalLoginViewModel> GetExternalLogins(string returnUrl, bool generateState = false)
        //{
        //    IEnumerable<AuthenticationDescription> descriptions = Authentication.GetExternalAuthenticationTypes();
        //    List<ExternalLoginViewModel> logins = new List<ExternalLoginViewModel>();

        //    string state;

        //    if (generateState)
        //    {
        //        const int strengthInBits = 256;
        //        state = RandomOAuthStateGenerator.Generate(strengthInBits);
        //    }
        //    else
        //    {
        //        state = null;
        //    }

        //    foreach (AuthenticationDescription description in descriptions)
        //    {
        //        ExternalLoginViewModel login = new ExternalLoginViewModel
        //        {
        //            Name = description.Caption,
        //            Url = Url.Route("ExternalLogin", new
        //            {
        //                provider = description.AuthenticationType,
        //                response_type = "token",
        //                client_id = Startup.PublicClientId,
        //                redirect_uri = new Uri(Request.RequestUri, returnUrl).AbsoluteUri,
        //                state = state
        //            }),
        //            State = state
        //        };
        //        logins.Add(login);
        //    }

        //    return logins;
        //}

        //// POST api/Account/Register
        //[AllowAnonymous]
        //[Route("Register")]
        //public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var user = new ApplicationUser() { UserName = model.Email, Email = model.Email };

        //    IdentityResult result = await UserManager.CreateAsync(user, model.Password);

        //    if (!result.Succeeded)
        //    {
        //        return GetErrorResult(result);
        //    }

        //    return Ok();
        //}

        //// POST api/Account/RegisterExternal
        //[OverrideAuthentication]
        //[HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        //[Route("RegisterExternal")]
        //public async Task<IHttpActionResult> RegisterExternal(RegisterExternalBindingModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var info = await Authentication.GetExternalLoginInfoAsync();
        //    if (info == null)
        //    {
        //        return InternalServerError();
        //    }

        //    var user = new ApplicationUser() { UserName = model.Email, Email = model.Email };

        //    IdentityResult result = await UserManager.CreateAsync(user);
        //    if (!result.Succeeded)
        //    {
        //        return GetErrorResult(result);
        //    }

        //    result = await UserManager.AddLoginAsync(user.Id, info.Login);
        //    if (!result.Succeeded)
        //    {
        //        return GetErrorResult(result); 
        //    }
        //    return Ok();
        //}
        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }

        #endregion
    }
}
