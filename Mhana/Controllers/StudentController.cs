﻿using Mhana.Helper;
using Mhana.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.IO;
using System.Text;
using System.Web;
using System.Threading.Tasks;

namespace Mhana.Controllers
{
    [Authorize]
    [RoutePrefix("api/v1/Student")]
    public class StudentController : ApiController
    {
        DateTime newD = DateTime.Now.AddHours(3);
        string URL = ConfigurationManager.AppSettings["URL"];
        /// <summary>
        /// شاشة عرض كافة المعلمين لدى الطالب
        /// </summary>
        /// <param name="name"></param>
        /// <param name="teaching_mechanism"></param>
        /// <param name="city"></param>
        /// <param name="gender"></param>
        /// <param name="specialization"></param>
        /// <param name="rate"></param>
        /// <param name="rows">start from 0 </param>
        /// <returns></returns>
        [Route("GetTeachers")]
        [ResponseType(typeof(List<TeacherList>))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpGet]
        public IHttpActionResult GetTeachers(string name=null,string teaching_mechanism = null, string city = null, string gender = null, int? specialization = null, int?rate = null, int?rows=0)
        {
            using (var db = new MhanaDbEntities())
            {
                rows = rows - 10 >= 0 ? rows -10 : rows;
                var filter = db.AspNetUsers.Where(w => w.AspNetRoles.FirstOrDefault() != null && w.AspNetRoles.FirstOrDefault().Name == "Teacher" && w.status==1 && w.is_complete==1 && w.absher==1);
                if (!string.IsNullOrEmpty(name))
                    filter = filter.Where(w => w.fullname.Contains(name));
                if (!string.IsNullOrEmpty(teaching_mechanism))
                    filter = filter.Where(w => w.teaching_mechanism.Contains(teaching_mechanism));
                if (!string.IsNullOrEmpty(gender))
                    filter = filter.Where(w => w.gender.Contains(gender));
                if (!string.IsNullOrEmpty(city))
                    filter = filter.Where(w => w.city.Contains(city));
                if(specialization.HasValue && specialization!=0)
                    filter = filter.Where(w => w.specialization== specialization);
                //if (rate.HasValue && rate != 0)
                //    filter = filter.Where(w => w. == specialization);
                var items = filter.Select(s => new TeacherList()
                {

                    id = s.Id,
                    fullname = string.IsNullOrEmpty(s.fullname) ? (s.first_name + " " + s.last_name) : s.fullname,
                    photo =URL +"/resources/users/"+ (string.IsNullOrEmpty(s.photo)?"default.png": s.photo),
                    rate = s.MyRates.Where(v=>v.rate_type==2).Count()>0? s.MyRates.Where(v => v.rate_type == 2).Sum(b=>b.rate) /s.MyRates.Where(v => v.rate_type == 2).Count():0,
                    feature = false,
                    specialization = s.specialization1 != null ? s.specialization1.name : "---",
                    branch_specialization = s.branch_specialization1 != null ? s.branch_specialization1.name : "---",
                    absher = s.absher.HasValue ? s.absher : 0,
                    online = s.online.HasValue?s.online:false

                }).OrderBy(o=>o.fullname).ToList();
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, null, true,items));                          
            }
        }
        /// <summary>
        /// عرض بيانات المعلم 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("GetTeacherDetails")]
        [ResponseType(typeof(TeacherDetailsResponse))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpGet]
        public IHttpActionResult GetTeacherDetails(string id)
        {
            using (var db = new MhanaDbEntities())
            {
                if(string.IsNullOrEmpty(id))
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK,"هنالك خطأ في البيانات", false, null));

                var item = db.AspNetUsers.Where(w => w.Id == id && w.status == 1).FirstOrDefault();
                if(item==null)
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هذا المستخدم غير فعال لدينا", false, null));

                TeacherDetailsResponse t = new TeacherDetailsResponse()
                {
                    id = item.Id,
                    fullname = string.IsNullOrEmpty(item.fullname) ? (item.first_name + " " + item.last_name) : item.fullname,
                    gender = item.gender,
                    details = item.details,
                    email = item.Email,
                    teaching_mechanism = item.teaching_mechanism,
                    certificate = item.certificate,
                    online_cost = Convert.ToDouble(item.online_cost),
                    site_cost = Convert.ToDouble(item.site_cost),
                    specialization = item.specialization1 != null ? item.specialization1.name : "---",
                    branch_specialization = item.branch_specialization1 != null ? item.branch_specialization1.name : "---",
                    total_course = 1,
                    rating = item.MyRates.Where(v => v.rate_type == 2).Count() > 0 ? item.MyRates.Where(v => v.rate_type == 2).Sum(b => b.rate) / item.MyRates.Where(v => v.rate_type == 2).Count() : 0,
                    cdate = string.Format("{0:MMM/dd/yyyy}", item.cdate),
                    photo = URL + "/resources/users/" + (string.IsNullOrEmpty(item.photo) ? "default.png" : item.photo),
                    absher = item.absher.HasValue ? item.absher : 0,
                    online = item.online.HasValue ? item.online : false

                };
               
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, null, true, t));
            }
        }
        /// <summary>
        /// شاشة ارسال طلب للمعلم 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("SendTeacherRequest")]
        [ResponseType(typeof(string))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpPost]
        public IHttpActionResult SendTeacherRequest(NewRequest model)
        {
            using (var db = new MhanaDbEntities())
            {
                if (!model.period.HasValue || string.IsNullOrEmpty(model.material))
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));
                string user_id = User.Identity.GetUserId();
                StdNewRequest _request = new StdNewRequest()
                {
                    cdate = DateTime.Now,
                    details = model.details,
                    material = model.material,
                    period = model.period,
                    start_date = model.start_date,
                    start_time = model.start_time,
                    status = 0,
                    std_id = user_id,
                    teacher_id = model.teacher_id,
                    teaching_mechanism = model.teaching_mechanism,
                    lat = model.lat,
                    lng = model.lng,
                    address_name = model.address_name,
                    address_vicinity = model.address_vicinity,
                    is_test = model.is_test
                     
                };
                var request =  db.StdNewRequests.Add(_request);
                db.SaveChanges();
                try
                {
                    if (request != null)
                    {
                        var fcm = db.AspNetUsers.Where(w => w.Id == model.teacher_id).AsEnumerable().Select(s => s.fcm).ToList();
                        if (fcm.Count() > 0)
                        {
                            Functions.SendNotification(fcm, "يوجد لديك طلب لدرس جديد يرجى مراجعة التطبيق لذلك", "token", request.Id, 1, model.teacher_id);
                        }
                        var _std = db.AspNetUsers.FirstOrDefault(c => c.Id == user_id);
                        var _teacher = db.AspNetUsers.FirstOrDefault(c => c.Id == model.teacher_id);
                        EmailContract mail = new EmailContract()
                        {
                            from = string.IsNullOrEmpty(_std.fullname)?(_std.first_name+" "+_std.last_name):_std.fullname,
                            material = model.material,
                            details = model.details,                            
                            period = model.period == 1?"يومي" : (model.period == 7 ? "اسبوعي" : "شهري"),
                            start_date = model.start_date,
                            start_time = model.start_time,
                            teaching_mechanism = model.teaching_mechanism,
                            is_test = model.is_test==1? "يحتاج اتصال تجريبي ":"بدون اتصال تجريبي",
                            title = "طلب درس خصوصي",
                            to = _teacher.Email,
                            cdate = string.Format("{0:MMM/dd/yyyy}", DateTime.Now),
                            status = 0

                        };
                        new Mhana.Controllers.MailerController().EmailRequest(mail).Deliver();
                    }
                }
                catch (Exception ex)
                {
                   
                }
                
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "تم ارسال طلبك بنجاح يرجى انتظار الرد في أقرب فرصة", true, null));
            }
        }
        /// <summary>
        /// شاشة عرض طلباتي لدى الطالب
        /// </summary>
        /// <param name="rows"></param>
        /// <returns></returns>
        [Route("MyRequests")]
        [ResponseType(typeof(List<NewRequestList>))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpGet]
        public IHttpActionResult MyRequests(int?rows=10)
        {
            using (var db = new MhanaDbEntities())
            {                
                string user_id = User.Identity.GetUserId();
                var items = db.StdNewRequests.Where(w => w.std_id == user_id).Select(s => new NewRequestListViewModel()
                {
                    id=s.Id,
                    cdate = DateTime.Now + "",
                    details = s.details,
                    material = s.material,
                    period = s.period,
                    start_date = s.start_date.HasValue?s.start_date.Value.Year + "-"+ s.start_date.Value.Month + "-"+ s.start_date.Value.Day:"",
                    start_time = s.start_time.HasValue ? s.start_time.Value.Hours + ":" + s.start_time.Value.Minutes : "---",
                    status = s.status,
                    teacher_id = s.teacher_id,
                    teacher_name = (s.AspNetUser1 != null) ? s.AspNetUser1.fullname : "---",
                    photo = URL + "/resources/users/" + (s.AspNetUser1 != null && !string.IsNullOrEmpty(s.AspNetUser1.photo) ? s.AspNetUser1.photo : "default.png"),
                    teaching_mechanism = s.teaching_mechanism,
                    student_name = s.AspNetUser != null ? s.AspNetUser.fullname:"---",
                    live_type = "video",
                    lng = s.lng,
                    lat = s.lat,
                    address_name = s.address_name,
                    address_vicinity = s.address_vicinity,
                    live_url = s.std_url
                }).OrderByDescending(o => o.id).ToList();                              
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "", true, items));
            }
        }
        /// <summary>
        /// شاشة عرض الدورات المسجل بها الطالب
        /// </summary>
        /// <param name="rows"></param>
        /// <returns></returns>
        [Route("MyCourses")]
        [ResponseType(typeof(List<CourseList>))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpGet]
        public IHttpActionResult MyCourses(string teacher_name=null,int? period = null, DateTime? reg_date = null, string teaching_mechanism = null,string material=null ,int? rows = 10)
        {
            using (var db = new MhanaDbEntities())
            {
                var times = db.Course_Times.Where(w => w.sch_date.HasValue ).Select(s => new CTime() { course_id = s.course_id, id = s.id, status = s.status, sch_date = s.sch_date }).OrderBy(o => o.sch_date).ToList();
                string user_id = User.Identity.GetUserId();
                var mycourses = db.Std_Course.Where(w => w.std_id == user_id);
                if (!string.IsNullOrEmpty(teacher_name))
                    mycourses = mycourses.Where(w => w.Course.AspNetUser != null && w.Course.AspNetUser.fullname != null && w.Course.AspNetUser.fullname.Contains(teacher_name));
                if(period.HasValue && period !=0)
                    mycourses = mycourses.Where(w => w.Course.period.HasValue &&w.Course.period==period);
                if (reg_date.HasValue)
                    mycourses = mycourses.Where(w => w.cdate.HasValue && w.cdate.Value.Year == reg_date.Value.Year && w.cdate.Value.Month == reg_date.Value.Month );

                if (!string.IsNullOrEmpty(teaching_mechanism))
                    mycourses = mycourses.Where(w =>!string.IsNullOrEmpty(w.Course.teaching_mechanism) && w.Course.teaching_mechanism==teaching_mechanism);

                if (!string.IsNullOrEmpty(material))
                    mycourses = mycourses.Where(w => !string.IsNullOrEmpty(w.Course.material) && w.Course.material == material);

                var items = mycourses.Select(s => new CourseList()
                {
                    id=s.course_id,
                    title = s.Course.title,                    
                    start_time = s.Course.start_time,
                    status = 0,                   
                    teacher_name = (s.Course.AspNetUser != null) ? s.Course.AspNetUser.fullname : "---",
                    photo = URL + "/resources/users/" + (s.Course.AspNetUser != null && !string.IsNullOrEmpty(s.Course.AspNetUser.photo) ? s.Course.AspNetUser.photo : "default.png"),
                    live_url = s.lesson_link,
                    teacher_id = (s.Course.AspNetUser != null) ? s.Course.AspNetUser.Id : "---",
                }).OrderByDescending(o => o.id).ToList();
                foreach (var item in items)
                {
                    var sd = times.Where(w => w.course_id == item.id).FirstOrDefault();
                    item.start_date = sd != null ? sd.sch_date : null;
                }
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "", true, items));
            }
        }
        /// <summary>
        /// شاشة عرض تفاصيل الكورس
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("CourseDetails")]
        [ResponseType(typeof(CourseDetails))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpGet]
        public IHttpActionResult CourseDetails(int? id)
        {
            string user_id = User.Identity.GetUserId();
            using (var db = new MhanaDbEntities())
            {
                var item = db.Courses.Where(w => w.Id == id).FirstOrDefault();
                var std_course = item.Std_Course.Where(w => w.std_id == user_id).FirstOrDefault();
                CourseDetails cd = new CourseDetails()
                {
                    id = item.Id,
                    cdate = item.cdate + "",
                    live_url = std_course!=null? std_course.lesson_link:"",
                    photo = URL + "/resources/users/" + (item.AspNetUser != null ? item.AspNetUser.photo : "default.png"),
                    remain_time = (DateTime.Now - item.start_date).Value.TotalMinutes + "",
                    status = item.status,
                    title = item.title,
                    teacher_name = item.AspNetUser.fullname,
                    teacher_id = (item.AspNetUser != null) ? item.AspNetUser.Id : "---",
                    lat = item.lat,
                    lng = item.lng,
                    address_name = item.address_name,
                    address_vicinity = item.address_vicinity
                    
                };
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "", true, cd));
            }
        }
        /// <summary>
        /// ارسال دعوة الى طالب معين 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("InviteStudent")]
        [ResponseType(typeof(string))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpPost]
        public async Task<IHttpActionResult> InviteStudent(InviteStudent model)
        {
            try
            {
                using (var context = new ApplicationDbContext())
                {
                    var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                    var checkUser = UserManager.FindByName(model.email);
                    string user_id = User.Identity.GetUserId();
                    var owner_user = UserManager.FindByName(user_id);
                    if (checkUser == null)
                    {
                        var user = new ApplicationUser
                        {
                            cdate = DateTime.Now,
                            Email = model.email,
                            fullname = "",
                            status = 0,
                            UserName = model.email,
                            is_complete = 0,
                             
                        };

                        string pass = System.Web.Security.Membership.GeneratePassword(8, 3);

                        var x = await UserManager.CreateAsync(user, pass);

                        UserManager.AddToRole(user.Id, "Student");

                        emailInviteS mail = new emailInviteS()
                        {
                            from = owner_user.fullname,
                            msg = "لقد تم دعوتك من قبل الطالب " + owner_user.fullname + "للمشاركة في درس في تطبيق درس خصوصي نتمن القيام بتحميل التطبيق الخاص بدرس خصوصي من الروابط التالية  ",
                            title = "دعوة طالب للمشاركة في درس خصوصي",
                            password = pass,
                            username = model.email,
                            to = model.email,
                            cdate = string.Format("{0:MMM/dd/yyyy}", DateTime.Now)
                        };
                        new Mhana.Controllers.MailerController().InviteS(mail).Deliver();
                    }

                }


                using (var db = new MhanaDbEntities())
                {
                    var u_id = User.Identity.GetUserId();

                    var _user = db.AspNetUsers.Find(u_id);

                    var __course = db.Course_Times.Where(w => w.course_id == model.course_id).OrderByDescending(o => o.id).FirstOrDefault();
                    if (!model.course_id.HasValue || string.IsNullOrEmpty(model.email))
                        return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));
                    var invited_user = db.AspNetUsers.Where(w => w.UserName == model.email).FirstOrDefault();

                    if (invited_user != null)
                    {
                        var check_course_user = db.Std_Course.Where(w => w.std_id == invited_user.Id && w.course_id == model.course_id).FirstOrDefault();
                        if (check_course_user != null)
                            return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هذا الحساب مسجل مسبقاً في هذا الدرس", false, null));

                        Std_Course sc = new Std_Course()
                        {
                            cdate = DateTime.Now,
                            course_id = model.course_id,
                            status = 1,
                            std_id = invited_user.Id,
                        };

                                         

                        Invite e = new Invite()
                        {
                            cdate = DateTime.Now,
                            send_by = u_id,
                            status = 0,
                            invite_type = 0 //0 for student without reg 1 for student with reg
                        };
                        db.Invites.Add(e);
                        db.SaveChanges();


                        emailInviteS mail = new emailInviteS()
                        {
                            from = _user != null ? _user.fullname : "---",
                            msg = "لقد تم دعوتك من قبل الطالب " + (_user != null ? _user.fullname : "---") + "للمشاركة في درس في تطبيق درس خصوصي نتمنى القيام بتحميل التطبيق الخاص بدرس خصوصي من الروابط التالية  ",
                            title = "دعوة طالب للمشاركة في درس خصوصي",
                            password = "",
                            username = "",
                            to = model.email,
                            cdate = string.Format("{0:MMM/dd/yyyy}", DateTime.Now)
                        };
                        new Mhana.Controllers.MailerController().InviteS(mail).Deliver();
                        if (!string.IsNullOrEmpty(__course.wiziq_id))
                        {
                            string xml_attendance = "";
                            xml_attendance += "<attendee><attendee_id>" + invited_user.Id + "</attendee_id><screen_name>" + model.email + "</screen_name><language_culture_name>ar-sa</language_culture_name></attendee>";
                            var std_response = WiziQ.WizIQClass.AddAttendees(__course.wiziq_id, xml_attendance);
                            string atten_url = "";
                            if (!string.IsNullOrEmpty(std_response) && std_response.Substring(0, 20).Contains("ok"))
                            {
                                rsp resultObject = Functions.DeserializeXML<rsp>(std_response);
                                foreach (var item in resultObject.add_attendees.attendee_list)
                                {
                                    // var std_time = sc;
                                    if (sc != null)
                                    {
                                        atten_url = item.attendee_url;
                                        sc.lesson_link = item.attendee_url;
                                    }

                                }
                            }
                        }
                        db.Std_Course.Add(sc);
                        db.SaveChanges();
                    }                   
                   
                    var responseData = myrequest(50, model.course_id);
                    PaymentModelResult pmodel = new PaymentModelResult();
                   

                        if (responseData != null)
                        {
                            //using (StreamWriter writetext = new StreamWriter(HttpContext.Current.Server.MapPath("~/write.txt"), true))
                            //{
                            //    writetext.WriteLine("----------------------------------- Payment responseData " + DateTime.Now + " -----------------");
                            //    foreach (var group in responseData)
                            //    {
                            //        writetext.WriteLine("Key: {0} Value: {1}", group.Key, group.Value);
                            //    }
                            //    foreach (var group in responseData["result"])
                            //    {
                            //        writetext.WriteLine("Key: {0} Value: {1}", group.Key, group.Value);
                            //    }
                            //    writetext.WriteLine("responseData : " + responseData);
                            //    writetext.WriteLine("----------------------------------- Payment responseData" + DateTime.Now + " -----------------");
                            //}

                            foreach (var group in responseData)
                            {
                                if (group.Key == "id")
                                {
                                    pmodel.checkout_id = group.Value;

                                }
                                if (group.Key == "ndc")
                                {
                                    pmodel.ndc = group.Value;
                                    //_request.txt_id = group.Value;
                                    //db.Entry(_request).State = EntityState.Modified;
                                    //db.SaveChanges();
                                }

                                if (group.Key == "result")
                                {
                                    foreach (var group2 in responseData["result"])
                                    {
                                        if (group2.Key == "description")
                                        {
                                            pmodel.description = group2.Value;
                                            if (group2.Value == "successfully created checkout")
                                            {
                                                pmodel.status = 1;
                                                //  db.SaveChanges();
                                            }
                                            else
                                            {
                                                pmodel.status = -1;
                                            }

                                        }
                                        if (group.Key == "code")
                                        {
                                            pmodel.code = group2.Value;
                                        }


                                    }
                                }

                            }

                    }
                            return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "تم ارسال طلبك بنجاح", true, new InviteResponse { checkout_id = pmodel.checkout_id }));
                }

            }catch(Exception EX)
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/debug.txt"), true))
                    file.WriteLine("/n" + EX.ToString());
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));
            }
        }
        public Dictionary<string, dynamic> myrequest(float? amount, int? request_id, int? gateway = 0)
        {

            using (StreamWriter writetext = new StreamWriter(HttpContext.Current.Server.MapPath("~/write.txt"), true))
            {
                writetext.WriteLine("----------------------------------- Model2 " + DateTime.Now + " -----------------");
                writetext.WriteLine("Amount: {0}", String.Format("{0:0.00}", amount));
            }
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            Dictionary<string, dynamic> responseData;
            string data = ConfigurationManager.AppSettings["HyperPayEntityId"]+// "entityId=8ac7a4ca75744bbb017574dc588703ef" +
                "&amount=" + String.Format("{0:0.00}", amount) +
                "&currency=SAR" +
                "&paymentType=DB" +
                "&notificationUrl="+ ConfigurationManager.AppSettings["HyperPayNotify"] + "/" + request_id;

            string url = ConfigurationManager.AppSettings["HyperPayUrl"];// "https://test.oppwa.com/v1/checkouts";
            byte[] buffer = Encoding.ASCII.GetBytes(data);
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.Method = "POST";
            request.Headers["Authorization"] = ConfigurationManager.AppSettings["HyperPayAuthorization"];// "Bearer OGFjN2E0Y2E3NTc0NGJiYjAxNzU3NGRiZDM3NDAzZWF8QmtISDNNR3h0cw==";
            request.ContentType = "application/x-www-form-urlencoded";
            Stream PostData = request.GetRequestStream();
            PostData.Write(buffer, 0, buffer.Length);
            PostData.Close();
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                var s = new System.Web.Script.Serialization.JavaScriptSerializer();
                responseData = s.Deserialize<Dictionary<string, dynamic>>(reader.ReadToEnd());
                reader.Close();
                dataStream.Close();
            }
            return responseData;
        }
        /// <summary>
        /// عرض العقد على الطالب 
        /// 0 jsut new request
        /// 1 accepted from teacher
        /// 2 send contract to teacher 
        /// 3 teacher accept contract
        /// -1 teacher reject contrat
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("GetContract")]
        [ResponseType(typeof(NewRequestList))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpGet]
        public IHttpActionResult GetContract(int? request_id)
        {
            using (var db = new MhanaDbEntities())
            {

                var contract = db.StdNewRequests.Where(w => w.Id == request_id).FirstOrDefault();
                var term = Functions.getOption("contract_term");
                string Role = User.IsInRole("Student") ? "Student" : "Teacher";
                if(contract!=null)
                {
                    var ipans = db.Banks_Account.Where(w => w.user_id == contract.teacher_id).Select(x => new IBan()
                    {
                        iban_account = x.bank_account,
                        iban_bank = x.bank_name,
                        iban_name = x.bank_account_name
                    }).ToList();

                    //string presenter_url = "";
                    //string std_url = "";
                    if (string.IsNullOrEmpty(contract.presenter_url) || string.IsNullOrEmpty(contract.std_url))
                    {
                        var wiziq_id = db.WiziqIDs.Where(w => w.status == 0).FirstOrDefault();
                        if (wiziq_id != null)
                        {

                            CourseWiziQ Cw = new CourseWiziQ()
                            {
                                attendee_limit = 2 + "",
                                create_recording = "true",
                                duration = "10",
                                start_time = contract.start_date.Value.Month + "/" + contract.start_date.Value.Day + "/" + contract.start_date.Value.Year + " " + contract.start_date.Value.Hour + ":" + contract.start_date.Value.Minute,
                                title = contract.std_id + contract.Id,
                                //presenter_email = "mohanna111@gmail.com",
                                presenter_id = wiziq_id.wiziq_id,
                                presenter_name = wiziq_id.email,
                                language_culture_name = "ar-sa",
                                presenter_default_controls = "audio, video",
                                time_zone = "Asia/Riyadh"
                            };
                            string c_response = WiziQ.WizIQClass.Create(Cw);
                            using (System.IO.StreamWriter file = new System.IO.StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/debug.txt"), true))
                                file.WriteLine("/n" + c_response + Cw.start_time);
                            if (!string.IsNullOrEmpty(c_response) && c_response.Substring(0, 20).Contains("ok"))
                            {
                                rsp resultObject = Functions.DeserializeXML<rsp>(c_response);
                                contract.presenter_url = resultObject.create.class_details.presenter_list.presenter.presenter_url;
                                contract.wiziq_id = resultObject.create.class_details.class_id;
                                //  presenter_url = contract.presenter_url;
                                // _ct.status = 1;
                                // elemnt.course_live_url = resultObject.create.class_details.presenter_list.presenter.presenter_url;
                                db.Entry(contract).State = EntityState.Modified;
                                // db.Entry(elemnt).State = EntityState.Modified;
                                db.SaveChanges();

                                //// add student ////

                                string xml_attendance = "<attendee><attendee_id>" + contract.std_id + "</attendee_id><screen_name>اتصال تجريبي</screen_name><language_culture_name>ar-sa</language_culture_name></attendee>";
                                var std_response = WiziQ.WizIQClass.AddAttendees(contract.wiziq_id, xml_attendance);
                                using (System.IO.StreamWriter file = new System.IO.StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/debug.txt"), true))
                                    file.WriteLine("/n" + std_response);
                                // string atten_url = "";

                                if (!string.IsNullOrEmpty(std_response) && std_response.Substring(0, 20).Contains("ok"))
                                {
                                    rsp resultObject2 = Functions.DeserializeXML<rsp>(std_response);
                                    foreach (var item in resultObject2.add_attendees.attendee_list)
                                    {
                                        contract.std_url = item.attendee_url;
                                        //   std_url = contract.std_url;
                                        db.Entry(contract).State = EntityState.Modified;
                                    }
                                    db.SaveChanges();
                                }


                            }
                        }
                    }
                    NewRequestListViewModel s = new NewRequestListViewModel()
                    {
                        id = contract.Id,
                        cdate = contract.cdate + "",
                        details = contract.details,
                        material = contract.material,
                        period = contract.period,
                        start_date = contract.start_date.HasValue ? contract.start_date.Value.Year + "-" + contract.start_date.Value.Month + "-" + contract.start_date.Value.Day : "",
                        start_time = contract.start_time.HasValue ? contract.start_time.Value.Hours + ":" + contract.start_time.Value.Minutes : "---",
                        status = contract.status,
                        teacher_id = contract.teacher_id,
                        teacher_name = contract.AspNetUser1 != null ? contract.AspNetUser1.fullname : "---",
                        photo = URL + "/resources/users/" + (contract.AspNetUser1 != null && !string.IsNullOrEmpty(contract.AspNetUser1.photo) ? contract.AspNetUser1.photo : "default.png"),
                        teacher_bank_accounts = ipans,
                        terms = term,
                        site_cost = contract.AspNetUser1 != null && !string.IsNullOrEmpty(contract.AspNetUser1.site_cost) ? Convert.ToDouble(contract.AspNetUser1.site_cost) : 0,
                        online_cost = contract.AspNetUser1 != null && !string.IsNullOrEmpty(contract.AspNetUser1.site_cost) ? Convert.ToDouble(contract.AspNetUser1.online_cost) : 0,
                        lessons = contract.period,
                        teaching_mechanism = contract.teaching_mechanism,
                        lat = contract.lat,
                        lng = contract.lng,
                        live_url = contract.std_url

                    };
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, null, true, s));
                }
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));
            }
        }



        /// <summary>
        /// ارسال اشعار برابط الدرس التجريبي        
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("GetTestNotify")]
        [ResponseType(typeof(string))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpGet]
        public IHttpActionResult GetTestNotify(int? request_id)
        {
            using (var db = new MhanaDbEntities())
            {

                var contract = db.StdNewRequests.Where(w => w.Id == request_id).FirstOrDefault();
                if(contract !=null)
                {
                    var teacher = db.AspNetUsers.FirstOrDefault(f => f.Id == contract.teacher_id);
                    var student = db.AspNetUsers.FirstOrDefault(f => f.Id == contract.std_id);
                    List<string> fcm = new List<string>();
                    if (!string.IsNullOrEmpty(teacher.fcm))
                    {
                        fcm.Add(teacher.fcm);
                        string msg = "لقد قام الطالب  " + student.fullname + " بارسال طلب درس تجريبي مدته 10 دقائق يرجى مراجعة التطبيق ";
                        Functions.SendNotification(fcm, msg, "token", contract.Id, 1, teacher.Id);
                    }
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "تم ارسال طلبك بنجاح يرجى انتظار رد المعلم", true, null));
                }                                   
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));
            }
        }


        /// <summary>
        /// الموافقة/الرفض لطلب 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("PayFroRequest")]
        [ResponseType(typeof(string))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpPost]
        public IHttpActionResult PayFroRequest(AcceptanceRequset model)
        {
            try { 
            var u_id = User.Identity.GetUserId();
                using (var db = new MhanaDbEntities())
                {

                    if (!model.status.HasValue || !model.request_id.HasValue)
                        return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));
                    var requset = db.StdNewRequests.Where(w => w.Id == model.request_id).FirstOrDefault();

                    if (requset != null && requset.std_id == u_id)
                    {
                        requset.status = 2;
                        db.Entry(requset).State = EntityState.Modified;
                        db.SaveChanges();
                        //  db.Entry(requset).State = EntityState.Detached;
                        try
                        {
                            var teacher = db.AspNetUsers.Where(w => w.Id == requset.teacher_id).FirstOrDefault();
                            var student = db.AspNetUsers.Where(w => w.Id == requset.std_id).FirstOrDefault();
                            List<Course_Times> ct = new List<Course_Times>();
                            var _date = DateTime.Now.AddMinutes(10);
                            Course_Times _ct = new Course_Times()
                            {
                                sch_date = _date,
                                sch_time = _date.TimeOfDay,
                                teacher_id = requset.teacher_id,
                                status = 1,
                            };
                            ct.Add(_ct);
                            Course c = new Course()
                            {
                                title = string.IsNullOrEmpty(student.fullname) ? (student.first_name + " " + student.last_name) : student.fullname,
                                allow_sound = true,
                                allow_video = true,
                                cdate = DateTime.Now,
                                cost = requset.teaching_mechanism == "onsite" ? Convert.ToDouble(teacher.site_cost) : Convert.ToDouble(teacher.online_cost),
                                details = requset.details,
                                lectures_count = 1,
                                period = requset.period,
                                start_date = requset.start_date,
                                start_time = requset.start_date.HasValue ? requset.start_date.Value.TimeOfDay : DateTime.Now.TimeOfDay,
                                teacher_id = requset.teacher_id,
                                status = 1,
                                teaching_mechanism = requset.teaching_mechanism,
                                total_std = 1,
                                Course_Times = ct,
                                request_id = model.request_id,
                                is_test = 0,
                                material = requset.material,
                                 lat = requset.lat,
                                  lng = requset.lng
                            };
                            var elemnt = db.Courses.Add(c);
                            db.SaveChanges();
                            

                            Std_Course sc = new Std_Course()
                            {
                                cdate = DateTime.Now,
                                course_id = elemnt.Id,
                                status = 1,
                                std_id = requset.std_id
                            };
                            db.Std_Course.Add(sc);
                            db.SaveChanges();
                            db.Entry(c).State = EntityState.Detached;
                            db.Entry(_ct).State = EntityState.Detached;
                            List<string> fcm = new List<string>();
                            if (!string.IsNullOrEmpty(teacher.fcm))
                            {
                                fcm.Add(teacher.fcm);
                                string msg = "لقد قام الطالب " + student.fullname + " بتأكيد موافقته على العقد وتأكيد الدفع لبدء الدروس";
                                Functions.SendNotification(fcm, msg, "token", requset.Id, 1, teacher.Id);
                            }


                            /////////////// WIZIQ //////////////
                             string atten_url = "";
                            if (c.teaching_mechanism == "online")
                            {
                              
                                string wiziqId = Functions.CreateWiziq(c, _ct);
                                using (System.IO.StreamWriter file = new System.IO.StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/debug.txt"), true))
                                    file.WriteLine("wiziqId : " + wiziqId);
                                if (!string.IsNullOrEmpty(wiziqId))
                                {
                                    atten_url = Functions.AddStudentToWiziq(c, wiziqId);
                                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/debug.txt"), true))
                                        file.WriteLine("atten_url : " + atten_url);
                                }
                                else
                                {
                                   // return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));
                                }
                            }

                            CourseDetails cd = new CourseDetails()
                            {
                                id = elemnt.Id,
                                cdate = elemnt.cdate + "",
                                live_url = atten_url,
                                photo = URL + "/resources/users/" + (teacher != null ? teacher.photo : "default.png"),
                                remain_time = "10",
                                status = elemnt.status,
                                title = string.IsNullOrEmpty(student.fullname) ? (student.first_name + " " + student.last_name) : student.fullname,
                                teacher_name = teacher.fullname,
                                teacher_id = teacher.Id,
                                lat = elemnt.lat,
                                lng = elemnt.lng,
                                address_name = elemnt.address_name,
                                address_vicinity = elemnt.address_vicinity

                            };
                            return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "تم ارسال طلبك بنجاح", true, cd));

                        }
                        catch (Exception ex)
                        {

                        }
                        return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "تم ارسال طلبك بنجاح", true, null));
                    }
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));
                }
            }
            catch (Exception ex)
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/debug.txt"), true))
                    file.WriteLine("/n" + ex.Message);
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));
            }

        }

        /// <summary>
        /// تقييم طالب ل معلم   
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("Rate_Teacher")]
        [ResponseType(typeof(string))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpPost]
        public IHttpActionResult Rate_Teacher(RateModel model)
        {
            var u_id = User.Identity.GetUserId();
            using (var db = new MhanaDbEntities())
            {

                if (!model.rate.HasValue || string.IsNullOrEmpty(model.teacher_id))
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));
                var requset = db.MyRates.Where(w => w.std_id == model.std_id && w.teacher_id == u_id && w.rate_type ==2).FirstOrDefault();
                if (requset != null)
                    return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "لقد قمت بتقييم هذا المعلم مسبقاً", false, null));

                MyRate mr = new MyRate()
                {
                    cdate = DateTime.Now,
                    comment = model.comment,
                    course_id = model.course_id,
                    rate = model.rate,
                    rate_type = 2,
                    std_id = model.std_id,
                    teacher_id = u_id

                };
                db.MyRates.Add(mr);
                db.SaveChanges();
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "شكراً لك تم ارسال تقييم بنجاح", true, null));

            }
        }


        /// <summary>
        /// عرض تقييمات المعلم للطالب       
        /// </summary>
        /// <param name="rows"></param>
        /// <returns></returns>
        [Route("StudentRate")]
        [ResponseType(typeof(List<RateModel>))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpGet]
        public IHttpActionResult StudentRate(int? rows = 10)
        {
            var u_id = User.Identity.GetUserId();
            using (var db = new MhanaDbEntities())
            {

                var items = db.MyRates.Where(w => w.std_id == u_id && w.rate_type == 1).Select(s => new RateModel()
                {
                    id = s.id,
                    cdate = DateTime.Now,
                    comment = s.comment,
                    course_title = s.Course.title,
                    rate = s.rate,
                    course_id = s.course_id,
                    std_id = s.std_id,
                    student_name = string.IsNullOrEmpty(s.AspNetUser.fullname) ? (s.AspNetUser.first_name + " " + s.AspNetUser.last_name) : s.AspNetUser.fullname,
                    
                    teacher_name = string.IsNullOrEmpty(s.AspNetUser1.fullname) ? (s.AspNetUser1.first_name + " " + s.AspNetUser1.last_name) : s.AspNetUser1.fullname,
                    photo = URL + "/resources/users/" + (s.AspNetUser != null && !string.IsNullOrEmpty(s.AspNetUser.photo) ? s.AspNetUser.photo : "default.png"),

                }).OrderByDescending(o => o.id).ToList();
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "", true, items));
            }
        }


        /// <summary>
        /// عرض تقييمات معلم محدد        
        /// </summary>
        /// <param name="rows"></param>
        /// <returns></returns>
        [Route("ShowTeacherRates")]
        [ResponseType(typeof(List<RateModel>))]
        [ResponseCodes(HttpStatusCode.OK)]
        [HttpGet]
        public IHttpActionResult ShowTeacherRates(string id, int? rows = 10)
        {
            if (string.IsNullOrEmpty(id))
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "هنالك خطأ في البيانات", false, null));
            var u_id = User.Identity.GetUserId();
            using (var db = new MhanaDbEntities())
            {

                var items = db.MyRates.Where(w => w.teacher_id == id && w.rate_type == 2).Select(s => new RateModel()
                {
                    id = s.id,
                    cdate = DateTime.Now,
                    comment = s.comment,
                    course_title = s.Course.title,
                    rate = s.rate,
                    course_id = s.course_id,
                    std_id = s.std_id,
                    student_name = string.IsNullOrEmpty(s.AspNetUser.fullname) ? (s.AspNetUser.first_name + " " + s.AspNetUser.last_name) : s.AspNetUser.fullname,
                    teacher_name = string.IsNullOrEmpty(s.AspNetUser1.fullname) ? (s.AspNetUser1.first_name + " " + s.AspNetUser1.last_name) : s.AspNetUser1.fullname,
                    online = s.AspNetUser.online.HasValue ? s.AspNetUser.online : false

                }).OrderByDescending(o => o.id).ToList();
                return this.ResponseOK(new ResponseViewModel(HttpStatusCode.OK, "", true, items));
            }
        }
    }
}
