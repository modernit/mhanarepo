﻿using System.IO;
using System.Web.Mvc;
namespace Mhana.Helper
{
    public static class ControllerExtensions
    {
        public static ImageResult Image(this Controller controller, Stream imageStream, string contentType)
        {
            return new ImageResult(imageStream, contentType);
        }

        public static ImageResult Image(this Controller controller, byte[] imageBytes, string contentType)
        {
            return new ImageResult(new MemoryStream(imageBytes), contentType);
        }

        public static VideoResult Video(this Controller controller, byte[] imageBytes, string contentType)
        {
            return new VideoResult(new MemoryStream(imageBytes), contentType);
        }
    }
}