﻿using System;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace Mhana.Helper
{
    public static class ImageHelper
    {
        //public static string[] Extensions = ConfigurationManager.AppSettings["ImageFormats"].Split(',');

        public static byte[] GetCroppedImage(byte[] originalBytes, Size size, int x, int y, ImageFormat format, string saveTo = null)
        {
            using (var streamOriginal = new MemoryStream(originalBytes))
            using (var imgOriginal = Image.FromStream(streamOriginal))
            {
                //get original width and height of the incoming image
                var originalWidth = imgOriginal.Width; // 2560
                var originalHeight = imgOriginal.Height; // 1440

                //get the percentage difference in size of the dimension that will change the least
                var percWidth = (size.Width / (float)originalWidth); // 0.2
                var percHeight = (size.Height / (float)originalHeight); // 0.2
                var percentage = Math.Max(percHeight, percWidth); // 0.2

                //get the ideal width and height for the resize (to the next whole number)
                var width = (int)Math.Max(originalWidth * percentage, size.Width); // 512,600
                var height = (int)Math.Max(originalHeight * percentage, size.Height); // 288,377

                //actually resize it
                using (var resizedBmp = new Bitmap(width, height))
                {
                    using (var graphics = Graphics.FromImage(resizedBmp))
                    {
                     //   graphics.Clear(Color.White);
                        graphics.InterpolationMode = InterpolationMode.Default;
                        graphics.DrawImage(imgOriginal, 0, 0, width, height);
                    }

                    //work out the coordinates of the top left pixel for cropping
                    x = Math.Max(x, (width - size.Width) / 2); // 0
                    y = Math.Max(y, (height - size.Height) / 2); // 0

                    //create the cropping rectangle
                    var rectangle = new Rectangle(x, y, size.Width, size.Height); // 0, 0, 600, 377

                    //crop
                    using (var croppedBmp = resizedBmp.Clone(rectangle, resizedBmp.PixelFormat))
                    using (var ms = new MemoryStream())
                    {
                        //get the codec needed
                        var imgCodec = ImageCodecInfo.GetImageEncoders().First(c => c.FormatID == format.Guid);

                        //make a paramater to adjust quality
                        var codecParams = new EncoderParameters(1);

                        //reduce to quality of 80 (from range of 0 (max compression) to 100 (no compression))
                        codecParams.Param[0] = new EncoderParameter(Encoder.Quality, 100L);

                        //save to the memorystream - convert it to an array and send it back as a byte[]
                        croppedBmp.Save(ms, imgCodec, codecParams);
                        if (!string.IsNullOrEmpty(saveTo))
                            croppedBmp.Save(saveTo);
                        byte[] array = ms.ToArray();
                        croppedBmp.Dispose();
                        streamOriginal.Dispose();
                        imgOriginal.Dispose();
                        ms.Dispose();
                        return array;
                    }
                }
            }
        }
        public static byte[] FixedSize(byte[] originalBytes, Size size, int x, int y, ImageFormat format, string saveTo = null)
        {
            var streamOriginal = new MemoryStream(originalBytes);
            var imgOriginal = Image.FromStream(streamOriginal);


            int sourceWidth = imgOriginal.Width;
            int sourceHeight = imgOriginal.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float) size.Width / (float) sourceWidth);
            nPercentH = ((float) size.Height / (float) sourceHeight);
            if (nPercentH < nPercentW || nPercentH > nPercentW)
            {
                nPercent = nPercentH;
                destX = System.Convert.ToInt16((size.Width -
                                                (sourceWidth * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = System.Convert.ToInt16((size.Height -
                                                (sourceHeight * nPercent)) / 2);
            }

            int destWidth = (int) (sourceWidth * nPercent);
            int destHeight = (int) (sourceHeight * nPercent);

            Bitmap bmPhoto = new Bitmap(size.Width, size.Height,
                PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(imgOriginal.HorizontalResolution,
                imgOriginal.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.Clear(Color.White);
            grPhoto.InterpolationMode =
                InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgOriginal,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            var imgCodec = ImageCodecInfo.GetImageEncoders().First(c => c.FormatID == format.Guid);
            var codecParams = new EncoderParameters(1);

            //reduce to quality of 80 (from range of 0 (max compression) to 100 (no compression))
            codecParams.Param[0] = new EncoderParameter(Encoder.Quality, 100L);

            var ms = new MemoryStream();
            //save to the memorystream - convert it to an array and send it back as a byte[]
            bmPhoto.Save(ms, imgCodec, codecParams);

            if (!string.IsNullOrEmpty(saveTo))
                bmPhoto.Save(saveTo);
            byte[] array = ms.ToArray();
            bmPhoto.Dispose();
           streamOriginal.Dispose();
            imgOriginal.Dispose();
            ms.Dispose();
            return array;

        }
        //public static Image ScaleImage(Image image, int maxWidth, int maxHeight)
        //{
        //    var ratioX = (double)maxWidth / image.Width;
        //    var ratioY = (double)maxHeight / image.Height;
        //    var ratio = Math.Min(ratioX, ratioY);

        //    var newWidth = (int)(image.Width * ratio);
        //    var newHeight = (int)(image.Height * ratio);

        //    var newImage = new Bitmap(newWidth, newHeight);

        //    using (var graphics = Graphics.FromImage(newImage))
        //        graphics.DrawImage(image, 0, 0, newWidth, newHeight);

        //    return newImage;
        //}
        /*public static byte[] CropImage(byte[] originalBytes, int x, int y,int w, int h, ImageFormat format, string saveTo = null)
        {
            using (var streamOriginal = new MemoryStream(originalBytes))
            using (var imgOriginal = new Bitmap(streamOriginal))
            {
                //create the cropping rectangle
                var rectangle = new Rectangle(x, y, w, h); // 0, 0, 600, 377

                //crop
                using (var croppedBmp = imgOriginal.Clone(rectangle, imgOriginal.PixelFormat))
                using (var ms = new MemoryStream())
                {
                    //get the codec needed
                    var imgCodec = ImageCodecInfo.GetImageEncoders().First(c => c.FormatID == format.Guid);

                    //make a paramater to adjust quality
                    var codecParams = new EncoderParameters(1);

                    //reduce to quality of 80 (from range of 0 (max compression) to 100 (no compression))
                    codecParams.Param[0] = new EncoderParameter(Encoder.Quality, 50L);

                    //save to the memorystream - convert it to an array and send it back as a byte[]
                    croppedBmp.Save(ms, imgCodec, codecParams);
                    if (!string.IsNullOrEmpty(saveTo))
                        croppedBmp.Save(saveTo);

                    croppedBmp.Dispose();
                    streamOriginal.Dispose();
                    imgOriginal.Dispose();
                    return ms.ToArray();
                }
            }
        }*/
        public static byte[] CropImage(byte[] originalBytes, int x, int y, int w, int h, ImageFormat format, string saveTo = null)
        {
            using (var streamOriginal = new MemoryStream(originalBytes))
            using (var imgOriginal = new Bitmap(streamOriginal))
            {
                //create the cropping rectangle
                var rectangle = new Rectangle(x, y, w, h); // 0, 0, 600, 377

                //crop
                using (var croppedBmp = Crop(imgOriginal, rectangle))
                using (var ms = new MemoryStream())
                {
                    //get the codec needed
                    var imgCodec = ImageCodecInfo.GetImageEncoders().First(c => c.FormatID == format.Guid);

                    //make a paramater to adjust quality
                    var codecParams = new EncoderParameters(1);

                    //reduce to quality of 80 (from range of 0 (max compression) to 100 (no compression))
                    codecParams.Param[0] = new EncoderParameter(Encoder.Quality, 100L);

                    //save to the memorystream - convert it to an array and send it back as a byte[]
                    croppedBmp.Save(ms, imgCodec, codecParams);
                    if (!string.IsNullOrEmpty(saveTo))
                        croppedBmp.Save(saveTo);

                    croppedBmp.Dispose();
                    streamOriginal.Dispose();
                    imgOriginal.Dispose();
                    return ms.ToArray();
                }
            }
        }
        public static Bitmap Crop(Bitmap img, Rectangle cropArea)
        {
            using (Graphics gph = Graphics.FromImage(img))
            {
                gph.DrawImage(img, cropArea, cropArea, GraphicsUnit.Pixel);
            }
            return img;
        }

        //public static bool IsImage(HttpPostedFileBase file)
        //{
        //    if (file.ContentType.Contains("image")) return true;
        //    return Extensions.Any(item => file.FileName.ToLower().EndsWith(item, StringComparison.OrdinalIgnoreCase));
        //}

        public static string SaveTempFile(HttpPostedFileBase file, string path = "~/Temp")
        {
            string minWidth = ConfigurationManager.AppSettings["MinImageWidth"];
            string tempFolder = HttpContext.Current.Server.MapPath(path);
            string fileName = Guid.NewGuid() + ".jpeg";
            string savePath = Path.Combine(tempFolder, fileName);

            if (!Directory.Exists(tempFolder)) Directory.CreateDirectory(tempFolder);

            MemoryStream target = new MemoryStream();
            file.InputStream.CopyTo(target);
            byte[] fileData = target.ToArray();

            Image img = Image.FromStream(file.InputStream);
            int width = int.Parse(minWidth.Replace("px", ""));
            int height = (int)(img.Height * width / (decimal)img.Width);

            GetCroppedImage(fileData, new Size(width, height), 0, 0, ImageFormat.Jpeg, savePath);
            return fileName;
        }

        public static string SaveServiceFile(byte[] file, string path = "~/Temp")
        {
            string minWidth = ConfigurationManager.AppSettings["MinImageWidth"];
            string tempFolder = HttpContext.Current.Server.MapPath(path);
            string fileName = Guid.NewGuid() + ".jpeg";
            string savePath = Path.Combine(tempFolder, fileName);

            if (!Directory.Exists(tempFolder)) Directory.CreateDirectory(tempFolder);

           MemoryStream target = new MemoryStream(file);
           // file.InputStream.CopyTo(target);
           // byte[] fileData = file.ToArray();
            Image img = Image.FromStream(target);
            int width = int.Parse(minWidth.Replace("px", ""));
            int height = (int)(img.Height * width / (decimal)img.Width);

            GetCroppedImage(file, new Size(width, height), 0, 0, ImageFormat.Jpeg, savePath);
            return fileName;
        }

        public static void CropAndSaveImage(string filePath, string savePath, int x, int y, int w, int h)
        {
            byte[] fileData = File.ReadAllBytes(filePath);

            if (File.Exists(savePath)) File.Delete(savePath);
            if (File.Exists(filePath)) File.Delete(filePath);

            CropImage(fileData, x, y, w, h, ImageFormat.Jpeg, savePath);
        }

        //public static Tuple<bool, string> ValidateImage(HttpPostedFileBase file, bool restrict = true)
        //{
        //    if (file == null) return Tuple.Create(false, "لا يوجد صورة");
        //    if (!IsImage(file)) return Tuple.Create(false, "أنواع الصور المسموحة هي [" + string.Join(",", Extensions) + "]");
        //    if (file.ContentLength <= 0) return Tuple.Create(false, "حجم الملف المرفوع = صفر");
        //    if (restrict)
        //    {
        //        string maxImageSize = ConfigurationManager.AppSettings["MaxImageSize"];
        //        int size = int.Parse(maxImageSize.Replace("mb", "")) * 1048576;

        //        if (file.ContentLength > size)
        //            return Tuple.Create(false, "حجم الملف المسموح بتحميله يجب أن يكون أقل من أو يساوي" + maxImageSize);

        //        Image img = Image.FromStream(file.InputStream);
        //        string minWidth = ConfigurationManager.AppSettings["MinImageWidth"];
        //        string minHeight = ConfigurationManager.AppSettings["MinImageHeight"];
        //        int w = int.Parse(minWidth.Replace("px", ""));
        //        int h = int.Parse(minHeight.Replace("px", ""));
        //        if (img.Width < w || img.Height < h)
        //            return Tuple.Create(false, "أبعاد الصورة يجب أن تكون على الأقل" + minWidth + " في " + minHeight);
        //        img.Dispose();
        //        file.InputStream.Position = 0;
        //    }
        //    return Tuple.Create(true, "");
        //}

        public static string CropUserPhoto(string file, int x, int y, int w, int h)
        {
            HttpServerUtility server = HttpContext.Current.Server;
            string tempPath = server.MapPath("~/temp/" + file);
            byte[] fileData = File.ReadAllBytes(tempPath);
            string fileName = Guid.NewGuid() + ".jpeg";
            string savePath = server.MapPath("~/uploads/users/" + fileName);

            if (File.Exists(savePath)) File.Delete(savePath);
            if (File.Exists(tempPath)) File.Delete(tempPath);

            CropImage(fileData, x, y, w, h, ImageFormat.Jpeg, savePath);

            return fileName;
        }

        /*internal static void RemoveUserImage(User user)
        {
            var path = HttpContext.Current.Server.MapPath("~/uploads/users/" + user.Profile.Photo);
            if (File.Exists(path)) File.Delete(path);
        }*/
    }
}