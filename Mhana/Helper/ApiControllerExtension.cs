﻿using Mhana.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Http;

namespace Mhana.Helper
{
    public static class ApiControllerExtension
    {
        public static ResponseResults ResponseNotFound(this ApiController controller, ResponseViewModel result)
        {
            return new ResponseResults(HttpStatusCode.NotFound, result);
        }
        public static ResponseResults ResponseOK(this ApiController controller, ResponseViewModel result)
        {
            return new ResponseResults(HttpStatusCode.OK, result);
        }
        public static ResponseResults ResponseBadRequest(this ApiController controller, ResponseViewModel result)
        {
            return new ResponseResults(HttpStatusCode.BadRequest, result);
        }
        public static ResponseResults ResponseError(this ApiController controller, ResponseViewModel result)
        {
            return new ResponseResults(HttpStatusCode.InternalServerError, result);
        }

        
    }
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public  class ResponseCodesAttribute : Attribute
    {
        public ResponseCodesAttribute(params HttpStatusCode[] statusCodes)
        {
            ResponseCodes = statusCodes;
        }

        public IEnumerable<HttpStatusCode> ResponseCodes { get; private set; }
    }

    
}