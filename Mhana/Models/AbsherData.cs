//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Mhana.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class AbsherData
    {
        public string englishName { get; set; }
        public string arabicFatherName { get; set; }
        public string englishFatherName { get; set; }
        public string sub { get; set; }
        public string gender { get; set; }
        public string iss { get; set; }
        public string cardIssueDateGregorian { get; set; }
        public string englishGrandFatherName { get; set; }
        public string userid { get; set; }
        public string idVersionNo { get; set; }
        public string arabicNationality { get; set; }
        public string arabicName { get; set; }
        public string arabicFirstName { get; set; }
        public string nationalityCode { get; set; }
        public string iqamaExpiryDateHijri { get; set; }
        public string lang { get; set; }
        public string exp { get; set; }
        public string iat { get; set; }
        public string iqamaExpiryDateGregorian { get; set; }
        public string idExpiryDateGregorian { get; set; }
        public string jti { get; set; }
        public string issueLocationAr { get; set; }
        public string dobHijri { get; set; }
        public string cardIssueDateHijri { get; set; }
        public string englishFirstName { get; set; }
        public string issueLocationEn { get; set; }
        public string arabicGrandFatherName { get; set; }
        public string aud { get; set; }
        public string nbf { get; set; }
        public string nationality { get; set; }
        public string dob { get; set; }
        public string englishFamilyName { get; set; }
        public string idExpiryDateHijri { get; set; }
        public string assurance_level { get; set; }
        public string arabicFamilyName { get; set; }
        public Nullable<System.DateTime> cdate { get; set; }
        public string mhana_user_id { get; set; }
        public int id { get; set; }
    }
}
