﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net;

namespace Mhana.Models
{
    // Models returned by AccountController actions.

    public class ExternalLoginViewModel
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public string State { get; set; }
    }
    public class addTimeCourse
    {
        public int? id { get; set; }
        public DateTime? start_date { get; set; }
        public TimeSpan? start_time { get; set; }
    }
    public class InviteResponse
    {
        public string checkout_id { set; get; }
    }
    public class PaymentModelReq
    {
        public int? request_id { get; set; }
        public float? amount { get; set; } = 20;
        public int? gateway { get; set; } = 0;
        public string customer_email { get; set; }
        public string billing_street1 { get; set; }
        public string billing_city { get; set; }
        public string billing_state { get; set; }
        public string billing_postcode { get; set; }
        public string customer_fname { get; set; }
        public string customer_surname { get; set; }
    }
    public class PaymentModelResult
    {
        public string description { get; set; }
        public string checkout_id { get; set; }
        public string code { get; set; }
        public string ndc { get; set; }
        public int? status { get; set; }
    }
    public class AbsherDataResponse
    {
          public string    englishName { get; set; }
          public string    arabicFatherName { get; set; }
          public string    englishFatherName { get; set; }
          public string    sub { get; set; }
          public string    gender { get; set; }
          public string    iss { get; set; }
          public string    cardIssueDateGregorian { get; set; }
          public string    englishGrandFatherName { get; set; }
          public string    userid { get; set; }
          public string    idVersionNo { get; set; }
          public string    arabicNationality { get; set; }
          public string    arabicName { get; set; }
          public string    arabicFirstName { get; set; }
          public string    nationalityCode { get; set; }
          public string    iqamaExpiryDateHijri { get; set; }
          public string    lang { get; set; }
          public double?    exp { get; set; }
          public double?    iat { get; set; }
          public string    iqamaExpiryDateGregorian { get; set; }
          public string    idExpiryDateGregorian { get; set; }
          public string    jti { get; set; }
          public string    issueLocationAr { get; set; }
          public string    dobHijri { get; set; }
          public string    cardIssueDateHijri { get; set; }
          public string    englishFirstName { get; set; }
          public string    issueLocationEn { get; set; }
          public string    arabicGrandFatherName { get; set; }
          public string    aud { get; set; }
          public string    nbf { get; set; }
          public string    nationality { get; set; }
          public string    dob { get; set; }
          public string    englishFamilyName { get; set; }
          public string    idExpiryDateHijri { get; set; }
          public string    assurance_level { get; set; }
          public string    arabicFamilyName { get; set; }
    }
    public class emailInviteT
    {
        public string to { get; set; }
        public string title { get; set; }
        public string from { get; set; }
        public string msg { get; set; }
        public string cdate { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }
    public class emailInviteS
    {
        public string to { get; set; }
        public string title { get; set; }
        public string from { get; set; }
        public string msg { get; set; }
        public string cdate { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }
    public class EmailContract
    {
        public string to { get; set; }
        public string title { get; set; }
        public string from { get; set; }
        public string msg { get; set; }
        public string cdate { get; set; }
        public string period { get; set; }
        public DateTime? start_date { get; set; }
        public TimeSpan? start_time { get; set; }        
        public string teaching_mechanism { get; set; }
        public string material { get; set; }
        public string details { get; set; }       
        public string is_test { get; set; }                
        public string cost { get; set; }
        public int? status { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }

    }
    public class ChangeUserModel
    {
        public string to { get; set; }
        public string name { get; set; }
        public string url { get; set; }
    }
    public class adminModel
    {
        public string id { get; set; }
        public string fullname { get; set; }
        public string email { get; set; }
        public string photo { get; set; }
        public string phoneNumber { get; set; }
        public string absher_no { get; set; }
    }
    public class ManageInfoViewModel
    {
        public string LocalLoginProvider { get; set; }

        public string Email { get; set; }

        public IEnumerable<UserLoginInfoViewModel> Logins { get; set; }

        public IEnumerable<ExternalLoginViewModel> ExternalLoginProviders { get; set; }
    }

    public class UserInfoViewModel
    {
        public string Email { get; set; }

        public bool HasRegistered { get; set; }

        public string LoginProvider { get; set; }
    }

    public class UserLoginInfoViewModel
    {
        public string LoginProvider { get; set; }

        public string ProviderKey { get; set; }
    }
    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }
    public class ResetPasswordModel
    {
        [Required]
        public string CurrentPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
    ////// External Models /////////
    public class ForgotPasswordViewModel
    {
        public string Email { get; set; }
    }
    public class CityModel
    {

        public string code { get; set; }
        public List<string> cities { get; set; }
    }
    public class term_policy
    {
        public string term { get; set; }
        //public string term_en { get; set; }
        public string about { get; set; }
        /*public string about_en { get; set; }*/
        //public string advertiser_instructions { get; set; }
        //public string receive_instructions { get; set; }
        //public int? point_to_cash { get; set; }
        //public double? view_price { get; set; }

    }
    public class ConForm
    {
        public string name { get; set; }
        public string message { get; set; }
        public string email { get; set; }
    }
    public class CounrtyModel
    {
        public string name { get; set; }
        public string code { get; set; }
        public string flag { get; set; }
    }
    public class CityListModel
    {
        public string region_name { get; set; }
        public string city_name { get; set; }
        public string district_name { get; set; }
    }
    public class ResponseViewModel
    {
        public ResponseViewModel(HttpStatusCode _responseCode, string _message, bool? _status, object _results)
        {
            this.responseCode = _responseCode;
            this.message = _message;
            this.status = _status;
            this.results = _results;
        }
        public HttpStatusCode responseCode { get; set; }
        public string message { get; set; }
        public bool? status { get; set; }
        public object results { get; set; }
    }
    public class loginUser
    {
        public string fullname { get; set; }
        public string email { get; set; }
        public string id { get; set; }
        public string role { get; set; }
        public string photo { get; set; }
    }
    public class RegisterUserModel
    {
        public string email { get; set; }
        public string mobile { get; set; }
        /// <summary>
        /// Student  -- Teacher
        /// </summary>
        public string role { get; set; }
        public string fullname { get; set; }
        public string password { get; set; }
        public string confirm_password { get; set; }
    }
    public class registerResponseModel
    {
        public string token { get; set; }
        public int expired { get; set; }
        public string id { get; set; }
        public string fullname { get; set; }
        public string email { get; set; }
        public string mobile { get; set; }
        public bool? allow_notify { get; set; }
        public string role { get; set; }
        public bool? is_complete { get; set; }
        public int? absher { get; set; }
        public bool? online { get; set; }
        public string photo { get; set; }
        //public string absher_no { get; set; }
    }
    public class LoginBindingModel
    {
        public string username { get; set; }
        public string password { get; set; }
    }
    public class FCMToken
    {
        public string fcm { get; set; }
        public string device { get; set; }
    }
    public class IBan
    {

        public string iban_bank { get; set; }
        public string iban_account { get; set; }
        public string iban_name { get; set; }
    }
    public class IBanEdit
    {
        public int? id { get; set; }
        public string iban_bank { get; set; }
        public string iban_account { get; set; }
        public string iban_name { get; set; }
    }
    public class NotificationModel
    {
        public int id { get; set; }
        public string title { get; set; }
        public string details { get; set; }
        public string photo { get; set; }
        public string rel_id { get; set; }
        /// <summary>
        /// 1- ارسال اشعار بطلب جديد لمعلم 
        /// 2- اموافقة المعلم على طلب الطالب 
        /// 3- رفض المعلم طلب الطالب 
        /// 4- تنبيه ببدأ الكورس         
        ///
        /// </summary>
        public Nullable<int> type { get; set; }
        public Nullable<System.DateTime> cdate { get; set; }
        public Nullable<int> is_read { get; set; }
    }
    public class IBanList
    {
        public List<IBanEdit> iban_list { get; set; }
    }
    public class NotifyModel
    {
        public int? status { get; set; }
        public string fcm { get; set; }
    }
    public class ContactModel
    {
        public string message { get; set; }
        public string subject { get; set; }
        public int? course_id { get; set; }
        public int? rate { get; set; }

    }
    public class keyValue
    {
        public int? id { get; set; }

        public string value { get; set; }
    }

    public class keyValueEdu
    {
        public int? id { get; set; }
        public string value { get; set; }
        public List<keyValue> sub_level { get; set; }
    }

    public class keyValue2
    {
        public string id { get; set; }

        public string value { get; set; }
    }
    public class GeneralUser
    {
        public string id { get; set; }
        public string fullname { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string gender { get; set; }
        public string city { get; set; }
        public string region { get; set; }
        public DateTime? dob { get; set; }
        public string country { get; set; }
        public int? status { get; set; }
        public string photo { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public bool? is_complete { get; set; }
        public string nid { get; set; }


    }
    public class GeneralUserResponseModel
    {
        public string id { get; set; }
        public string fullname { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string gender { get; set; }
        public string city { get; set; }
        public string region { get; set; }
        public string dob { get; set; }
        public string country { get; set; }
        public int? status { get; set; }
        public string photo { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public bool? is_complete { get; set; }
        public string nid { get; set; }


    }
    public class Teacher : GeneralUser
    {
        public string details { get; set; }
        public string certificate { get; set; }
        public string certificate_photo { get; set; }
        public int? specialization { get; set; }
        public int? branch_specialization { get; set; }

        public keyValue specialization_obj { get; set; }
        public keyValue branch_specialization_obj { get; set; }
        /// <summary>
        /// (onsite)
        /// (online)
        /// (both)
        /// </summary>
        public string teaching_mechanism { get; set; }
        public string university { get; set; }
        public List<string> education_level { get; set; }
        public double? online_cost { get; set; }
        public double? site_cost { get; set; }
        public List<TeacherTimes> teacher_times { get; set; }
        public string nid { get; set; }

    }
    public class TeacherResponseModel : GeneralUserResponseModel
    {
        public string details { get; set; }
        public string certificate { get; set; }
        public string certificate_photo { get; set; }
        public int? specialization { get; set; }
        public int? branch_specialization { get; set; }

        public keyValue specialization_obj { get; set; }
        public keyValue branch_specialization_obj { get; set; }
        /// <summary>
        /// (onsite)
        /// (online)
        /// (both)
        /// </summary>
        public string teaching_mechanism { get; set; }
        public string university { get; set; }
        public List<string> education_level { get; set; }
        public double? online_cost { get; set; }
        public double? site_cost { get; set; }
        public List<TeacherTimes> teacher_times { get; set; }
        public string nid { get; set; }

    }
    public class TeacherRegisterResponseModel : TeacherResponseModel
    {
        public int? expired { get; set; }
        public string token { get; set; }
        public string fcm { get; set; }
        public int? absher { get; set; }
        
    }
    public class ViewPocketModel
    {
        public int? id { get; set; }
        public int? course_id { get; set; }
        public string course_name { get; set; }
        public int? std_count { get; set; }
        public double? total_amount { get; set; }
        public double? single_amount { get; set; }
        public double? fee { get; set; }
        public string status { get; set; }
    }
    public class TeacherDetailsResponse
    {
        public string id { get; set; }
        public string fullname { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string gender { get; set; }
        public string details { get; set; }
        public string certificate { get; set; }
        public double? online_cost { get; set; }
        public double? site_cost { get; set; }
        public string specialization { get; set; }
        public string branch_specialization { get; set; }
        public int? total_course { get; set; }
        public float? rating { get; set; }
        public string email { get; set; }
        public string teaching_mechanism { get; set; }
        public string cdate { get; set; }
        public string photo { get; set; }
        public string country { get; set; }
        public string education_level { get; set; }
        public int? absher { get; set; }
        public bool? online { get; set; }
        public string period { get; set; }
        public string material { get; set; }
    }
    public class Student : GeneralUser
    {
        public List<string> education_level { get; set; }
    }

    public class StudentResponseModel : GeneralUserResponseModel
    {
        public List<string> education_level { get; set; }
    }
    public class StudentRegisterResponseModel : StudentResponseModel
    {
        public int? expired { get; set; }
        public string token { get; set; }
        public string fcm { get; set; }
    }
    public class TeacherTimes
    {
        public int? day_number { get; set; }
        public string from { get; set; }
        public string to { get; set; }
    }
    public class TeacherList
    {
        public string id { get; set; }
        public string fullname { get; set; }
        public string specialization { get; set; }
        public string branch_specialization { get; set; }
        public string photo { get; set; }
        public int? rate { get; set; }
        public bool? feature { get; set; }
        public int? absher { get; set; }
        public bool? online { get; set; }
    }
    public class TeacherListAdmin
    {
        public string id { get; set; }
        public string fullname { get; set; }
        public string email { get; set; }
        public string mobile { get; set; }
        public string specialization { get; set; }
        public string branch_specialization { get; set; }
        public string photo { get; set; }
        public int? rate { get; set; }
        public int? absher { get; set; }
        public DateTime? cdate { get; set; }
        public int? status { get; set; }
        public List<int> course_ids { get; set; }
    }
    public class NewRequest
    {
        public int? period { get; set; }
        public DateTime? start_date { get; set; }
        public TimeSpan? start_time { get; set; }
        /// <summary>
        /// (onsite)
        /// (online)
        /// (both)
        /// </summary>
        public string teaching_mechanism { get; set; }
        public string material { get; set; }
        public string details { get; set; }
        public string teacher_id { get; set; }
        public string student_id { get; set; }
        public int? is_test { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public string address_name { get; set; }
        public string address_vicinity { get; set; }
        public string live_url { get; set; }

    }

    public class NewRequestViewModel
    {
        public int? period { get; set; }
        public string start_date { get; set; }
        public string start_time { get; set; }
        /// <summary>
        /// (onsite)
        /// (online)
        /// (both)
        /// </summary>
        public string teaching_mechanism { get; set; }
        public string material { get; set; }
        public string details { get; set; }
        public string teacher_id { get; set; }
        public string student_id { get; set; }
        public int? is_test { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public string address_name { get; set; }
        public string address_vicinity { get; set; }
        public string live_url { get; set; }

    }

    public class NewRequestListViewModel : NewRequestViewModel
    {
        public int? id { get; set; }
        public string cdate { get; set; }
        /// <summary>
        /// (0 طلب جديد ) , 
        /// (1 تم الموافقة على الطلب ) , 
        /// (2 تم تأكيد الطلب والدفع),
        /// (3 تم تأكيد الدفع),
        /// (-1 تم رفض الطلب )
        /// </summary>
        public int? status { get; set; }
        public string teacher_name { get; set; }
        public string photo { get; set; }
        public string student_name { get; set; }
        /// <summary>
        /// (video), 
        /// (audio)
        /// </summary>
        public string live_type { get; set; }
        public List<IBan> teacher_bank_accounts { get; set; }
        public string terms { get; set; }
        public int? lessons { get; set; }
        public double online_cost { get; set; }
        public double site_cost { get; set; }
        //public string lat { get; set; }
        //public string lng { get; set; }
        public double? amount { get; set; } = 50;

    }
    public class NewRequestList : NewRequest
    {
        public int? id { get; set; }
        public string cdate { get; set; }
        /// <summary>
        /// (0 طلب جديد ) , 
        /// (1 تم الموافقة على الطلب ) , 
        /// (2 تم تأكيد الطلب والدفع),
        /// (3 تم تأكيد الدفع),
        /// (-1 تم رفض الطلب )
        /// </summary>
        public int? status { get; set; }
        public string teacher_name { get; set; }
        public string photo { get; set; }
        public string student_name { get; set; }
        /// <summary>
        /// (video), 
        /// (audio)
        /// </summary>
        public string live_type { get; set; }
        public List<IBan> teacher_bank_accounts { get; set; }
        public string terms { get; set; }
        public int? lessons { get; set; }
        public double online_cost { get; set; }
        public double site_cost { get; set; }  
        //public string lat { get; set; }
        //public string lng { get; set; }


    }
    public class CourseList
    {
        public string title { get; set; }
        public int? id { get; set; }
        public string cdate { get; set; }
        public int? status { get; set; }
        public string teacher_name { get; set; }
        public string photo { get; set; }
        public DateTime? start_date { get; set; }
        public TimeSpan? start_time { get; set; }
        public string live_type { get; set; }
        public string live_url { get; set; }
        public string student_name { get; set; }
        public string teacher_id { get; set; }
        public string student_id { get; set; }
        public int? std_count { get; set; }
    }
    public class CourseDetails
    {
        public string title { get; set; }
        public int? id { get; set; }
        public string cdate { get; set; }
        public int? status { get; set; }
        public string teacher_name { get; set; }
        public string photo { get; set; }
        public string remain_time { get; set; }
        public string live_url { get; set; }
        public string teacher_id { get; set; }
        public string lng { get; set; }
        public string lat { get; set; }
        public string address_name { get; set; }
        public string address_vicinity { get; set; }
    }
    public class InviteStudent
    {
        public string email { get; set; }
        public int? i_will_pay { get; set; }
        public int? course_id { get; set; }
    }

    public class ExpandModel
    {
        public int? course_id { get; set; }
        /// <summary>
        /// in min
        /// </summary>
        public int? period { get; set; }
    }
    public class AcceptanceRequset
    {
        public int? request_id { get; set; }
        /// <summary>
        /// (0 reject),(1 accept),(2 pay)
        /// </summary>
        public int? status { get; set; }
        public string reason { get; set; }
    }
    public class HomeRequsts
    {
        public int? id { get; set; }
        public string student_name { get; set; }
        public string student_id { get; set; }
        public string photo { get; set; }
        public string cdate { get; set; }
        public string teaching_mechanism { get; set; }
    }
    public class HomeCourses
    {
        public int? id { get; set; }
        public string title { get; set; }
        public string photo { get; set; }
        public string teaching_mechanism { get; set; }
        public string start_time { get; set; }
        public string education_level { get; set; }
        public string live_type { get; set; }
        public string student_name { get; set; }
        public string student_id { get; set; }
        public string live_url { get; set; }
        public string start_date { get; set; }
    }
    public class HomeModel
    {
        public List<NewRequestListViewModel> waiting_requsts { get; set; }
        public List<HomeCourses> upcoming_courses { get; set; }
    }
    public class CalenderModel
    {
        public int? id { get; set; }
        public string title { get; set; }
        public string photo { get; set; }
        public string live_type { get; set; }
        public string time { get; set; }
        public string education_level { get; set; }
        public DateTime? start_date { get; set; }
        public string student_name { get; set; }
        public string student_id { get; set; }
    }
    public class TeacherWiziQ
    {
        public string name { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string phone_number { get; set; }
        public string mobile_number { get; set; }
        public string time_zone { get; set; }
        public string about_the_teacher { get; set; }
        public string can_schedule_class { get; set; }
        public string is_active { get; set; }
    }
    public class CourseWiziQ
    {
        public string start_time { get; set; }
        public string presenter_email { get; set; }
        public string presenter_id { get; set; }
        public string presenter_name { get; set; }
        public string title { get; set; }
        public string time_zone { get; set; }
        public string attendee_limit { get; set; }
        public string duration { get; set; }
        public string presenter_default_controls { get; set; }
        public string attendee_default_controls { get; set; }
        public string create_recording { get; set; }
        public string return_url { get; set; }
        public string language_culture_name { get; set; }
    }
    public class CreateCourse
    {
        public int? id { get; set; }
        public string title { get; set; }
        public string details { get; set; }
        public string teacher_id { get; set; }
        public string teaching_mechanism { get; set; }
        public int? total_std { get; set; }
        public int? period { get; set; }
        public int? lectures_count { get; set; }
        public DateTime? start_date { get; set; }
        public TimeSpan? start_time { get; set; }
        public bool? allow_sound { get; set; }
        public bool? allow_video { get; set; }
        public bool? both { get; set; }
        public string photo { get; set; }
        public float? cost { get; set; }
        public List<DateTime> schedual { get; set; }
        public string live_type { get; set; }
        public int? is_test { get; set; }
        public int? request_id { get; set; }
        public string  material { get; set; }
    }
    public class EditCourse
    {
        public int? id { get; set; }
        public int? status { get; set; }
        public string details { get; set; }

    }
    public static class Enums
    {
        public enum Status
        {
            Reject = -1,
            Visible = 1,
            Wait = 0,
            Delete = -2,
            Finish = 3

        };

        public enum Action
        {
            Share = 2,
            Read = 1,
            Visit = 3
        };

        public enum MStatus
        {
            remove = 0,
            check = 1
        };
    }
    public class CourseListAdmin
    {
        public string title { get; set; }
        public int? id { get; set; }
        public int? total_std { get; set; }
        public int? lectures_count { get; set; }
        public int? rate { get; set; }
        public int? period { get; set; }
        public string cdate { get; set; }
        public int? status { get; set; }
        public string teacher_name { get; set; }
        public string photo { get; set; }
        public DateTime? start_date { get; set; }
        public TimeSpan? start_time { get; set; }
        public string live_type { get; set; }
        public double? cost { get; set; }
    }

    public class todayLesson
    {
        public int? id { get; set; }
        public string course_name { get; set; }
        public int? course_id { get; set; }
        public DateTime? sch_date { get; set; }
        public TimeSpan? sch_time { get; set; }
        public string lesson_number { get; set; }
        public int? total_std { get; set; }
        public int? status { get; set; }
        public string link { get; set; }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class rsp
    {

        private string methodField;
        private rspAdd_teacher add_teacherField;
        private string statusField;
        private string call_idField;
        private rspCreate createField;
        private rspCancel cancelField;
        private rspGet_teacher_details get_teacher_detailsField;
        private rspAdd_attendees add_attendeesField;
        /// <remarks/>
        public string method
        {
            get
            {
                return this.methodField;
            }
            set
            {
                this.methodField = value;
            }
        }

        /// <remarks/>
        public rspAdd_teacher add_teacher
        {
            get
            {
                return this.add_teacherField;
            }
            set
            {
                this.add_teacherField = value;
            }
        }
        public rspCreate create
        {
            get
            {
                return this.createField;
            }
            set
            {
                this.createField = value;
            }
        }
        public rspGet_teacher_details get_teacher_details
        {
            get
            {
                return this.get_teacher_detailsField;
            }
            set
            {
                this.get_teacher_detailsField = value;
            }
        }
        public rspCancel cancel
        {
            get
            {
                return this.cancelField;
            }
            set
            {
                this.cancelField = value;
            }
        }
        public rspAdd_attendees add_attendees
        {
            get
            {
                return this.add_attendeesField;
            }
            set
            {
                this.add_attendeesField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string call_id
        {
            get
            {
                return this.call_idField;
            }
            set
            {
                this.call_idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rspAdd_teacher
    {

        private string teacher_idField;

        private string teacher_emailField;

        private bool statusField;

        /// <remarks/>
        public string teacher_id
        {
            get
            {
                return this.teacher_idField;
            }
            set
            {
                this.teacher_idField = value;
            }
        }

        /// <remarks/>
        public string teacher_email
        {
            get
            {
                return this.teacher_emailField;
            }
            set
            {
                this.teacher_emailField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }
    }



    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rspCreate
    {

        private rspCreateClass_details class_detailsField;

        private bool statusField;

        /// <remarks/>
        public rspCreateClass_details class_details
        {
            get
            {
                return this.class_detailsField;
            }
            set
            {
                this.class_detailsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rspCreateClass_details
    {

        private string class_idField;

        private string recording_urlField;

        private rspCreateClass_detailsPresenter_list presenter_listField;

        /// <remarks/>
        public string class_id
        {
            get
            {
                return this.class_idField;
            }
            set
            {
                this.class_idField = value;
            }
        }

        /// <remarks/>
        public string recording_url
        {
            get
            {
                return this.recording_urlField;
            }
            set
            {
                this.recording_urlField = value;
            }
        }

        /// <remarks/>
        public rspCreateClass_detailsPresenter_list presenter_list
        {
            get
            {
                return this.presenter_listField;
            }
            set
            {
                this.presenter_listField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rspCreateClass_detailsPresenter_list
    {

        private rspCreateClass_detailsPresenter_listPresenter presenterField;

        /// <remarks/>
        public rspCreateClass_detailsPresenter_listPresenter presenter
        {
            get
            {
                return this.presenterField;
            }
            set
            {
                this.presenterField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rspCreateClass_detailsPresenter_listPresenter
    {

        private string presenter_emailField;

        private string presenter_urlField;

        /// <remarks/>
        public string presenter_email
        {
            get
            {
                return this.presenter_emailField;
            }
            set
            {
                this.presenter_emailField = value;
            }
        }

        /// <remarks/>
        public string presenter_url
        {
            get
            {
                return this.presenter_urlField;
            }
            set
            {
                this.presenter_urlField = value;
            }
        }
    }


    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rspGet_teacher_details
    {

        private rspGet_teacher_detailsTeacher_details_list teacher_details_listField;

        private bool statusField;

        /// <remarks/>
        public rspGet_teacher_detailsTeacher_details_list teacher_details_list
        {
            get
            {
                return this.teacher_details_listField;
            }
            set
            {
                this.teacher_details_listField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rspGet_teacher_detailsTeacher_details_list
    {

        private rspGet_teacher_detailsTeacher_details_listTeacher_details teacher_detailsField;

        /// <remarks/>
        public rspGet_teacher_detailsTeacher_details_listTeacher_details teacher_details
        {
            get
            {
                return this.teacher_detailsField;
            }
            set
            {
                this.teacher_detailsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rspGet_teacher_detailsTeacher_details_listTeacher_details
    {

        private string teacher_idField;

        private string nameField;

        private string emailField;

        private string passwordField;

        private string phone_numberField;

        private string mobile_numberField;

        private string about_the_teacherField;

        private string imageField;

        private string time_zoneField;

        private string can_schedule_classField;

        private string is_activeField;

        /// <remarks/>
        public string teacher_id
        {
            get
            {
                return this.teacher_idField;
            }
            set
            {
                this.teacher_idField = value;
            }
        }

        /// <remarks/>
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }

        /// <remarks/>
        public string password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        /// <remarks/>
        public string phone_number
        {
            get
            {
                return this.phone_numberField;
            }
            set
            {
                this.phone_numberField = value;
            }
        }

        /// <remarks/>
        public string mobile_number
        {
            get
            {
                return this.mobile_numberField;
            }
            set
            {
                this.mobile_numberField = value;
            }
        }

        /// <remarks/>
        public string about_the_teacher
        {
            get
            {
                return this.about_the_teacherField;
            }
            set
            {
                this.about_the_teacherField = value;
            }
        }

        /// <remarks/>
        public string image
        {
            get
            {
                return this.imageField;
            }
            set
            {
                this.imageField = value;
            }
        }

        /// <remarks/>
        public string time_zone
        {
            get
            {
                return this.time_zoneField;
            }
            set
            {
                this.time_zoneField = value;
            }
        }

        /// <remarks/>
        public string can_schedule_class
        {
            get
            {
                return this.can_schedule_classField;
            }
            set
            {
                this.can_schedule_classField = value;
            }
        }

        /// <remarks/>
        public string is_active
        {
            get
            {
                return this.is_activeField;
            }
            set
            {
                this.is_activeField = value;
            }
        }
    }



    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rspCancel
    {

        private bool statusField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }
    }

    public class ContactModelCompany
    {
        public string message { get; set; }
        public string email { get; set; }
        public string title { get; set; }
        public string fullname { get; set; }
    }

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    //[System.SerializableAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    //public partial class attendee_list
    //{

    //    private attendee_listAttendee[] attendeeField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlElementAttribute("attendee")]
    //    public attendee_listAttendee[] attendee
    //    {
    //        get
    //        {
    //            return this.attendeeField;
    //        }
    //        set
    //        {
    //            this.attendeeField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.SerializableAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class attendee_listAttendee
    //{

    //    private string attendee_idField;

    //    private string screen_nameField;

    //    private string language_culture_nameField;

    //    /// <remarks/>
    //    public string attendee_id
    //    {
    //        get
    //        {
    //            return this.attendee_idField;
    //        }
    //        set
    //        {
    //            this.attendee_idField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string screen_name
    //    {
    //        get
    //        {
    //            return this.screen_nameField;
    //        }
    //        set
    //        {
    //            this.screen_nameField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string language_culture_name
    //    {
    //        get
    //        {
    //            return this.language_culture_nameField;
    //        }
    //        set
    //        {
    //            this.language_culture_nameField = value;
    //        }
    //    }
    //}

    public class atten_info
    {
        public string user_id { get; set; }
        public string name { get; set; }
        public string lang { get; set; }
    }


    ///// <remarks/>
    //[System.SerializableAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class rspAdd_attendees
    //{

    //    private string class_idField;

    //    private rspAdd_attendeesAttendee_list attendee_listField;

    //    private bool statusField;

    //    /// <remarks/>
    //    public string class_id
    //    {
    //        get
    //        {
    //            return this.class_idField;
    //        }
    //        set
    //        {
    //            this.class_idField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public rspAdd_attendeesAttendee_list attendee_list
    //    {
    //        get
    //        {
    //            return this.attendee_listField;
    //        }
    //        set
    //        {
    //            this.attendee_listField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public bool status
    //    {
    //        get
    //        {
    //            return this.statusField;
    //        }
    //        set
    //        {
    //            this.statusField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.SerializableAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class rspAdd_attendeesAttendee_list
    //{

    //    private rspAdd_attendeesAttendee_listAttendee attendeeField;

    //    /// <remarks/>
    //    public rspAdd_attendeesAttendee_listAttendee attendee
    //    {
    //        get
    //        {
    //            return this.attendeeField;
    //        }
    //        set
    //        {
    //            this.attendeeField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.SerializableAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class rspAdd_attendeesAttendee_listAttendee
    //{

    //    private string attendee_idField;

    //    private string attendee_urlField;

    //    /// <remarks/>
    //    public string attendee_id
    //    {
    //        get
    //        {
    //            return this.attendee_idField;
    //        }
    //        set
    //        {
    //            this.attendee_idField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string attendee_url
    //    {
    //        get
    //        {
    //            return this.attendee_urlField;
    //        }
    //        set
    //        {
    //            this.attendee_urlField = value;
    //        }
    //    }
    //}




    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rspAdd_attendees
    {

        private string class_idField;

        private rspAdd_attendeesAttendee[] attendee_listField;

        private bool statusField;

        /// <remarks/>
        public string class_id
        {
            get
            {
                return this.class_idField;
            }
            set
            {
                this.class_idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("attendee", IsNullable = false)]
        public rspAdd_attendeesAttendee[] attendee_list
        {
            get
            {
                return this.attendee_listField;
            }
            set
            {
                this.attendee_listField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rspAdd_attendeesAttendee
    {

        private string attendee_idField;

        private string attendee_urlField;

        /// <remarks/>
        public string attendee_id
        {
            get
            {
                return this.attendee_idField;
            }
            set
            {
                this.attendee_idField = value;
            }
        }

        /// <remarks/>
        public string attendee_url
        {
            get
            {
                return this.attendee_urlField;
            }
            set
            {
                this.attendee_urlField = value;
            }
        }
    }

    public class CTime
    {
        public int? id { get; set; }
        public Nullable<System.DateTime> sch_date { get; set; }
        public Nullable<int> course_id { get; set; }
        public Nullable<int> status { get; set; }
    }
    public class CourseFiles
    {
        public int? id { get; set; }
        public string title { get; set; }
        public string extension { get; set; }
        public string cdate { get; set; }
        public string size { get; set; }
        public string url { get; set; }
    }
    public class RateModel
    {
       
        public string  std_id { get; set; }
        public int? course_id { get; set; }       
        public int? rate { get; set; }        
        public string comment { get; set; }
        public string teacher_id { get; set; }
        public bool? online { get; set; }
       
        /// <summary>
        /// Just OutPut
        /// </summary>
        public int? id { get; set; }
        /// <summary>
        /// Just OutPut
        /// </summary>
        public string course_title { get; set; }
        /// <summary>
        /// Just OutPut
        /// </summary>
        public string teacher_name { get; set; }
        /// <summary>
        /// Just OutPut
        /// </summary>
        public string student_name { get; set; }
        /// <summary>
        /// Just OutPut
        /// </summary>
        public Nullable<System.DateTime> cdate { get; set; }
        /// <summary>
        /// Just OutPut
        /// </summary>
        public string photo { get; set; }
    }
    public class RegEmail
    {
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string type { get; set; }
        public string message { get; set; }
        public string cdate { get; set; }
        public string to { get; set; }
    }

}
