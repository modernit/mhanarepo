﻿//== Class definition

var DatatableHtmlTableDemo = function() {
	//== Private functions

    var Survey_Status = function () {

        var datatable = $('#Survey_Status').mDatatable({
            data: {
                saveState: { cookie: false },
            },
            search: {
                input: $('#generalSearch'),
            },
            columns: [
                {
                    field: 'userId',
                    type: 'number',
                },
                {
                    field: 'OrderDate',
                    type: 'date',
                    format: 'YYYY-MM-DD',
                },
                {
                    field: 'Type',
                    title: 'Type',
                    // callback function support for column rendering
                    template: function (row) {
                        var status = {
                            0: { 'title': 'مؤجل', 'state': 'warning' },
                            1: { 'title': 'مقبول', 'state': 'success' },
                            2: { 'title': 'تم الاجابة', 'state': 'success' },
                            10: { 'title': 'مرفوض', 'state': 'danger' },  
                            3: { 'title': 'مقبول', 'state': 'success' },   
                            4: { 'title': 'مقبول', 'state': 'success' },   
                        };
                        return '<span class="m-badge m-badge--' + status[row.Type].state + ' m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-' +
                            status[row.Type].state + '">' +
                            status[row.Type].title + '</span>';
                    },
                },
            ],
        });

        //$('#m_form_status').on('change', function() {
        //    datatable.search($(this).val().toLowerCase(), 'Type');
        //});
        $('#m_form_email').on('blur', function () {
            datatable.search($(this).val().toLowerCase(), 'Email');
        });
        $('#m_form_mobile').on('blur', function () {
            datatable.search($(this).val().toLowerCase(), 'Mobile');
        });
        $('#m_form_name').on('blur', function () {
            datatable.search($(this).val().toLowerCase(), 'Owner');
        });
        $('#m_form_type').on('change', function () {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        //$('#m_form_status, #m_form_type').selectpicker();

    };
	// demo initializer
    var demo = function () {

        var datatable = $('.m-datatable').mDatatable({
            data: {
                saveState: { cookie: false },
            },
            search: {
                input: $('#generalSearch'),
            },
            columns: [
				{
				    field: 'userId',
				    type: 'number',
				},
				{
				    field: 'OrderDate',
				    type: 'date',
				    format: 'YYYY-MM-DD',
                },
                {
                    field: 'OrderDate2',
                    type: 'date',
                    format: 'YYYY-MM-DD',
                },
                {
				    field: 'Status',
				    title: 'Status',
				    // callback function support for column rendering
				    template: function (row) {
				        var status = {
				            1: { 'title': 'Pending', 'class': 'm-badge--brand' },
				            2: { 'title': 'Delivered', 'class': ' m-badge--metal' },
				            3: { 'title': 'Canceled', 'class': ' m-badge--primary' },
				            4: { 'title': 'Success', 'class': ' m-badge--success' },
				            5: { 'title': 'Info', 'class': ' m-badge--info' },
				            6: { 'title': 'Danger', 'class': ' m-badge--danger' },
				            7: { 'title': 'Warning', 'class': ' m-badge--warning' },
				        };
				        return '<span class="m-badge ' + status[row.Status].class + ' m-badge--wide">' + status[row.Status].title + '</span>';
				    },
				}, {
				    field: 'Type',
				    title: 'Type',
				    // callback function support for column rendering
				    template: function (row) {
				        var status = {
				            1: { 'title': 'فعال', 'state': 'info' },
				            10: { 'title': 'موقوف', 'state': 'danger' },
                            0: { 'title': 'بانتظار التفعيل', 'state': 'warning' },
                            2: { 'title': 'موقوف', 'state': 'danger' },
                            3: { 'title': 'مؤجل', 'state': 'warning' },
                            4: { 'title': 'انتهى', 'state': 'success' },
				        };
				        return '<span class="m-badge m-badge--' + status[row.Type].state + ' m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-' +
							status[row.Type].state + '">' +
							status[row.Type].title + '</span>';
				    },
				},
            ],
        });
    }
	    // demo initializer
		var order = function() {

		    var datatable = $('.m-datatable').mDatatable({
		        data: {
		            saveState: {cookie: false},
		        },
		        search: {
		            input: $('#generalSearch'),
		        },
		        columns: [
                    {
                        field: 'userId',
                        type: 'number',
                    },
                    {
                        field: 'OrderDate',
                        type: 'date',
                        format: 'YYYY-MM-DD',
                    },
                    //{
                    //    field: 'Status',
                    //    title: 'Status',
                    //    // callback function support for column rendering
                    //    template: function(row) {
                    //        var status = {
                    //            1: {'title': 'Pending', 'class': 'm-badge--brand'},
                    //            2: {'title': 'Delivered', 'class': ' m-badge--metal'},
                    //            3: {'title': 'Canceled', 'class': ' m-badge--primary'},
                    //            4: {'title': 'Success', 'class': ' m-badge--success'},
                    //            5: {'title': 'Info', 'class': ' m-badge--info'},
                    //            6: {'title': 'Danger', 'class': ' m-badge--danger'},
                    //            7: {'title': 'Warning', 'class': ' m-badge--warning'},
                    //        };
                    //        return '<span class="m-badge ' + status[row.Status].class + ' m-badge--wide">' + status[row.Status].title + '</span>';
                    //    },
                    //},
                    {
                        field: 'Type',
                        title: 'Type',
                        // callback function support for column rendering
                        template: function(row) {
                            var status = {
                                1: {'title': 'جاري العمل', 'state': 'brand'},
                                2: {'title': 'تم انجازه', 'state': 'success' },
                                0: {'title': 'بانتظار الموافقة', 'state': 'warning' },
                                '-1': {'title': 'تم الغاءه', 'state': 'danger' },
                            };
                            return '<span class="m-badge m-badge--' + status[row.Type].state + ' m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-' +
                                status[row.Type].state + '">' +
                                status[row.Type].title + '</span>';
                        },
                    },
		        ],
		    });

		//$('#m_form_status').on('change', function() {
		//    datatable.search($(this).val().toLowerCase(), 'Type');
		//});
		$('#m_form_email').on('blur', function () {
		    datatable.search($(this).val().toLowerCase(), 'Email');
		});
		$('#m_form_mobile').on('blur', function () {
		    datatable.search($(this).val().toLowerCase(), 'Mobile');
		});
		$('#m_form_name').on('blur', function () {
		    datatable.search($(this).val().toLowerCase(), 'Owner');
		});
		$('#m_form_type').on('change', function() {
			datatable.search($(this).val().toLowerCase(), 'Type');
		});

		//$('#m_form_status, #m_form_type').selectpicker();

    };


   


	return {
		//== Public functions
		init: function() {
			// init dmeo
            Survey_Status();
		    demo();
            order();
           
		},
	};
}();

jQuery(document).ready(function() {
	DatatableHtmlTableDemo.init();
});